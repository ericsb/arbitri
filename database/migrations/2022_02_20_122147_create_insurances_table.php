<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInsurancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insurances', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->text('conclude_insurance_status')->nullable();
            $table->text('conclude_insurance_comment')->nullable();
            $table->text('conclude_private_status')->nullable();
            $table->text('conclude_private_comment')->nullable();
            $table->text('specific_requirements')->nullable();
            $table->text('additional_comments')->nullable();
            $table->unsignedBigInteger('file_id_1')->nullable();
            $table->unsignedBigInteger('file_id_2')->nullable();
            $table->unsignedBigInteger('file_id_3')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insurances');
    }
}
