<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDomiciliationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('domiciliations', function (Blueprint $table) {
            $table->id();
            $table->text('third_party_status')->nullable();
            $table->text('third_party_requirements')->nullable();
            $table->text('looking_renting_status')->nullable();
            $table->text('budget_per_month')->nullable();
            $table->text('purchasing_budget')->nullable();
            $table->text('need_residence_status')->nullable();
            $table->text('country')->nullable();
            $table->text('canton')->nullable();
            $table->text('neighborhood')->nullable();
            $table->text('infrastructure')->nullable();
            $table->text('multiple_office_status')->nullable();
            $table->text('preferred_style')->nullable();
            $table->text('m2_count')->nullable();
            $table->text('people_count')->nullable();
            $table->text('infrastructure_near_by')->nullable();
            $table->text('infrastructure_in')->nullable();
            $table->longText('specific_requirements')->nullable();
            $table->text('comments')->nullable();
            $table->text('additional_file_id_1')->nullable();
            $table->text('additional_file_id_2')->nullable();
            $table->text('additional_file_id_3')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('domiciliations');
    }
}
