<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIdentitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('identities', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->text('name')->nullable();
            $table->text('surname')->nullable();
            $table->text('birth_date')->nullable();
            $table->text('address_of_domicile')->nullable();
            $table->text('phone_number')->nullable();
            $table->text('email')->nullable();
            $table->text('profession')->nullable();
            $table->text('company_name')->nullable();
            $table->text('role_in_the_company')->nullable();
            $table->text('linkedin')->nullable();
            $table->text('previous_year_annual_salary_in_chf')->nullable();
            $table->text('nationality')->nullable();
            $table->text('other_nationalities')->nullable();
            $table->text('city_of_origin')->nullable();
            $table->text('municipality_of_origin')->nullable();
            $table->text('id_card')->nullable();
            $table->unsignedBigInteger('id_recto_file_id')->nullable();
            $table->unsignedBigInteger('id_verso_file_id')->nullable();
            $table->unsignedBigInteger('passport_file_id')->nullable();
            $table->unsignedBigInteger('utility_bill_file_id')->nullable();
            $table->unsignedBigInteger('criminal_record_extract_file_id')->nullable();
            $table->text('company_person_represent')->nullable();
            $table->text('company_legal_form')->nullable();
            $table->text('company_address')->nullable();
            $table->text('company_id_number')->nullable();
            $table->text('company_commercial_register_extract')->nullable();
            $table->unsignedBigInteger('company_proof_of_existence_file_id')->nullable();
            $table->unsignedBigInteger('company_signed_procuration_file_id')->nullable();
            $table->unsignedBigInteger('company_other_proof_of_existence_file_id')->nullable();
            $table->unsignedBigInteger('company_other_signed_procuration_file_id')->nullable();
            $table->text('additional_comments')->nullable();
            $table->unsignedBigInteger('additional_1_file_id')->nullable();
            $table->unsignedBigInteger('additional_2_file_id')->nullable();
            $table->unsignedBigInteger('additional_3_file_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('identities');
    }
}
