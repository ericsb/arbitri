<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmploymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->text('hire_employees_status')->nullable();
            $table->text('hire_director_status')->nullable();
            $table->text('immigration_law_status')->nullable();
            $table->text('find_personal_status')->nullable();
            $table->text('introduction_blockchain_status')->nullable();
            $table->text('recover_private_key_status')->nullable();
            $table->text('comments')->nullable();
            $table->unsignedBigInteger('file_id_1')->nullable();
            $table->unsignedBigInteger('file_id_2')->nullable();
            $table->unsignedBigInteger('file_id_3')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employments');
    }
}
