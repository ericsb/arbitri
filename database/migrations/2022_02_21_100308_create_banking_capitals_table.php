<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBankingCapitalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banking_capitals', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('identity_id')->nullable();
            $table->text('deposit')->nullable();
            $table->text('source_of_wealth')->nullable();
            $table->text('wealth_from_crypto_status')->nullable();
            $table->unsignedBigInteger('transaction_file_id_1')->nullable();
            $table->unsignedBigInteger('transaction_file_id_2')->nullable();
            $table->unsignedBigInteger('transaction_file_id_3')->nullable();
            $table->text('employ_count')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banking_capitals');
    }
}
