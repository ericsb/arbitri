<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFoundationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('foundations', function (Blueprint $table) {
            $table->id();
            $table->text('user_id')->nullable();
            $table->text('form_type')->nullable();
            $table->text('form_name')->nullable();
            $table->text('company_name')->nullable();
            $table->text('company_alternative_name')->nullable();
            $table->text('address_of_seat')->nullable();
            $table->text('political_municipality')->nullable();
            $table->text('company_office_type')->nullable();
            $table->text('election_file_id')->nullable();
            $table->text('goals')->nullable();
            $table->text('goals_involve')->nullable();
            $table->text('further_document_file_id_1')->nullable();
            $table->text('further_document_file_id_2')->nullable();
            $table->text('further_document_file_id_3')->nullable();
            $table->text('capital')->nullable();
            $table->text('capital_liberated')->nullable();
            $table->text('liberated_in_chf')->nullable();
            $table->text('type_of_liberation')->nullable();
            $table->text('bank_location_in_escrow')->nullable();
            $table->text('bank_name_in_escrow')->nullable();
            $table->text('attestation_certificate_file_id')->nullable();
            $table->text('liberated_in_cash')->nullable();
            $table->text('nature_of_contribution')->nullable();
            $table->text('will_nature_of_contribution')->nullable();
            $table->text('foundation_report')->nullable();
            $table->text('foundation_contract')->nullable();
            $table->text('auditor_providing')->nullable();
            $table->text('identity_of_swiss_auditor')->nullable();
            $table->text('website_of_auditor')->nullable();
            $table->text('email_of_contact')->nullable();
            $table->text('detailed_inventory_file_id')->nullable();
            $table->text('balance_sheet_file_id')->nullable();
            $table->text('number_of_shares')->nullable();
            $table->text('nominal_value')->nullable();
            $table->text('nature_of_the_share')->nullable();
            $table->text('communication')->nullable();
            $table->text('issuance_of_all')->nullable();
            $table->text('issuance_of_share')->nullable();
            $table->text('detail_regarding')->nullable();
            $table->text('plan_to_acquire_another_company')->nullable();
            $table->text('another_company_foundation_contract_file_id')->nullable();
            $table->text('another_company_takeover_contract_file_id')->nullable();
            $table->text('another_company_auditor_providing_file_id')->nullable();
            $table->text('another_company_identity_of_swiss_auditor')->nullable();
            $table->text('another_company_website_of_auditor')->nullable();
            $table->text('another_company_email_of_contact')->nullable();
            $table->text('audit_applicable')->nullable();
            $table->text('who_will_auditor')->nullable();
            $table->text('mandate_file_id')->nullable();
            $table->text('excerpt_file_id')->nullable();
            $table->text('financial_year')->nullable();
            $table->text('financial_year_comment')->nullable();
            $table->text('closing_of_the_first_financial_year')->nullable();
            $table->text('closing_of_the_first_financial_year_comment')->nullable();
            $table->text('minimum_quorum')->nullable();
            $table->text('minimum_quorum_comment')->nullable();
            $table->text('majority_required')->nullable();
            $table->text('fiduciary_contract')->nullable();
            $table->text('shareholder_agreement')->nullable();
            $table->text('commercial_register')->nullable();
            $table->text('commercial_register_person')->nullable();
            $table->text('tax_exemption')->nullable();
            $table->text('tax_ruling')->nullable();
            $table->text('part_of_group')->nullable();
            $table->text('people_count')->nullable();
            $table->text('additional_comments')->nullable();
            $table->text('additional_file_id_1')->nullable();
            $table->text('additional_file_id_2')->nullable();
            $table->text('additional_file_id_3')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('foundations');
    }
}
