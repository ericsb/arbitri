<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuditorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auditors', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->text('economic_activities')->nullable();
            $table->unsignedBigInteger('business_plan_file_id')->nullable();
            $table->unsignedBigInteger('internal_chart_file_id')->nullable();
            $table->unsignedBigInteger('overall_chart_file_id')->nullable();
            $table->text('avg_volume_transaction')->nullable();
            $table->text('book_entries')->nullable();
            $table->text('countries_or_regions')->nullable();
            $table->text('fiat_currencies')->nullable();
            $table->text('crypto_currencies')->nullable();
            $table->text('employ_count')->nullable();
            $table->text('regulated_activity_status')->nullable();
            $table->text('certification_status')->nullable();
            $table->text('ordinary_status')->nullable();
            $table->text('comments')->nullable();
            $table->unsignedBigInteger('additional_file_id_1')->nullable();
            $table->unsignedBigInteger('additional_file_id_2')->nullable();
            $table->unsignedBigInteger('additional_file_id_3')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auditors');
    }
}
