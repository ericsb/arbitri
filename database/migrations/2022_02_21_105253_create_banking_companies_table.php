<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBankingCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banking_companies', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('identity_id')->nullable();
            $table->text('fiat_currencies')->nullable();
            $table->text('crypto_currencies')->nullable();
            $table->text('yearly_transaction_volume')->nullable();
            $table->text('avg_amount_in_custody')->nullable();
            $table->text('source_of_wealth')->nullable();
            $table->text('wealth_from_crypto_status')->nullable();
            $table->unsignedBigInteger('transaction_file_id_1')->nullable();
            $table->unsignedBigInteger('transaction_file_id_2')->nullable();
            $table->unsignedBigInteger('transaction_file_id_3')->nullable();
            $table->unsignedBigInteger('excerpt_file_id')->nullable();
            $table->unsignedBigInteger('memo_file_id_1')->nullable();
            $table->unsignedBigInteger('memo_file_id_2')->nullable();
            $table->unsignedBigInteger('annual_file_id')->nullable();
            $table->unsignedBigInteger('preliminary_file_id')->nullable();
            $table->unsignedBigInteger('current_organization_file_id')->nullable();
            $table->unsignedBigInteger('current_organization_file_id_1')->nullable();
            $table->unsignedBigInteger('current_organization_file_id_2')->nullable();
            $table->unsignedBigInteger('shareholders_file_id')->nullable();
            $table->text('additional_comment')->nullable();
            $table->unsignedBigInteger('additional_file_id_1')->nullable();
            $table->unsignedBigInteger('additional_file_id_2')->nullable();
            $table->unsignedBigInteger('additional_file_id_3')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banking_companies');
    }
}
