<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<script src="{{ asset('assets/js/scripts.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
<script>
    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });

    $('.flag').on('click', function() {
        let _token   = $('meta[name="csrf-token"]').attr('content');
        let flagImg = $(this);
        $.post("/toggle-flag",
            {
                name: this.name,
                _token: _token
            },
            function(data, status){
                if(status === 'success')
                {
                    if(flagImg.attr("src") === "https://form.arbitri.ch/assets/white.png")
                    {
                        console.log("white change to black");
                        flagImg.attr("src","https://form.arbitri.ch/assets/black.png");

                    }
                    else if(flagImg.attr("src") === "https://form.arbitri.ch/assets/black.png")
                    {
                        console.log("black change to white");
                        flagImg.attr("src","https://form.arbitri.ch/assets/white.png");
                    }

                    if(flagImg.attr("src") === "https://form.arbitri.ch/assets/search-on.png")
                    {
                        console.log("white change to black");
                        flagImg.attr("src","https://form.arbitri.ch/assets/search-off.png");

                    }
                    else if(flagImg.attr("src") === "https://form.arbitri.ch/assets/search-off.png")
                    {
                        console.log("black change to white");
                        flagImg.attr("src","https://form.arbitri.ch/assets/search-on.png");
                    }

                    if(flagImg.attr("src") === "https://form.arbitri.ch/assets/gear-on.png")
                    {
                        console.log("white change to black");
                        flagImg.attr("src","https://form.arbitri.ch/assets/gear-off.png");

                    }
                    else if(flagImg.attr("src") === "https://form.arbitri.ch/assets/gear-off.png")
                    {
                        console.log("black change to white");
                        flagImg.attr("src","https://form.arbitri.ch/assets/gear-on.png");
                    }

                    if(flagImg.attr("src") === "https://form.arbitri.ch/assets/x-on.png")
                    {
                        console.log("white change to black");
                        flagImg.attr("src","https://form.arbitri.ch/assets/x-off.png");

                    }
                    else if(flagImg.attr("src") === "https://form.arbitri.ch/assets/x-off.png")
                    {
                        console.log("black change to white");
                        flagImg.attr("src","https://form.arbitri.ch/assets/x-on.png");
                    }

                    if(flagImg.attr("src") === "https://form.arbitri.ch/assets/check-on.png")
                    {
                        console.log("white change to black");
                        flagImg.attr("src","https://form.arbitri.ch/assets/check-off.png");

                    }
                    else if(flagImg.attr("src") === "https://form.arbitri.ch/assets/check-off.png")
                    {
                        console.log("black change to white");
                        flagImg.attr("src","https://form.arbitri.ch/assets/check-on.png");
                    }

                }
            });
    });
</script>

