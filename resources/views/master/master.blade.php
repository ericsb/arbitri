<!DOCTYPE html>
<html lang="en">
<head>
    @include('master.head')
</head>
<body class="sb-nav-fixed">
@include('components.topnav')
<div id="layoutSidenav">
    <div id="layoutSidenav_nav">
        <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
            <div class="sb-sidenav-menu">
                <div class="nav">
                    <div class="sb-sidenav-menu-heading">Forms</div>
                    <a class="nav-link @if(\Illuminate\Support\Facades\Route::is('identities.add-name.index')) {{ 'active' }} @endif" href="{{ route('identities.add-name.index') }}">
                        <div class="sb-nav-link-icon"><i class="fas fa-pen-alt"></i></div>
                        Identities
                    </a>
                    <a class="nav-link @if(\Illuminate\Support\Facades\Route::is('foundation.edit')) {{ 'active' }} @endif" href="{{ route('foundation.edit') }}">
                        <div class="sb-nav-link-icon"><i class="fas fa-pen-alt"></i></div>
                        Company / Association / Foundation
                    </a>
                    <a class="nav-link @if(\Illuminate\Support\Facades\Route::is('domiciliation.edit')) {{ 'active' }} @endif" href="{{ route('domiciliation.edit') }}">
                        <div class="sb-nav-link-icon"><i class="fas fa-pen-alt"></i></div>
                        Domiciliation & Residence
                    </a>
                    <a class="nav-link @if(\Illuminate\Support\Facades\Route::is('auditor.edit')) {{ 'active' }} @endif" href="{{ route('auditor.edit') }}">
                        <div class="sb-nav-link-icon"><i class="fas fa-pen-alt"></i></div>
                        Auditor & Accountant
                    </a>
                    <a class="nav-link @if(\Illuminate\Support\Facades\Route::is('bankings.index')) {{ 'active' }} @endif" href="{{ route('bankings.index') }}">
                        <div class="sb-nav-link-icon"><i class="fas fa-pen-alt"></i></div>
                        Banking & financial services
                    </a><a class="nav-link @if(\Illuminate\Support\Facades\Route::is('insurances.edit')) {{ 'active' }} @endif" href="{{ route('insurances.edit') }}">
                        <div class="sb-nav-link-icon"><i class="fas fa-pen-alt"></i></div>
                        Insurances
                    </a><a class="nav-link @if(\Illuminate\Support\Facades\Route::is('employments.edit')) {{ 'active' }} @endif" href="{{ route('employments.edit') }}">
                        <div class="sb-nav-link-icon"><i class="fas fa-pen-alt"></i></div>
                        Employment, immigration, Community integration
                    </a>
                    <a class="nav-link @if(\Illuminate\Support\Facades\Route::is('special_request.edit')) {{ 'active' }} @endif" href="{{ route('special_request.edit') }}">
                        <div class="sb-nav-link-icon"><i class="fas fa-pen-alt"></i></div>
                        Special request?
                    </a>
                </div>
            </div>

            <div class="sb-sidenav-footer">
                <div class="small">Logged in as:</div>
                {{ \Illuminate\Support\Facades\Auth::user()->name }} ({{ \Illuminate\Support\Facades\Auth::user()->email }})
            </div>
        </nav>
    </div>
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <h1 class="mt-4"></h1>
            </div>
        </main>
        @yield('content')
        <br>
        <hr>
        @include('master.disclaimer')
        <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid px-4">
                <div class="d-flex align-items-center justify-content-between small">
                </div>
            </div>
        </footer>
    </div>
</div>

@include('master.scripts')
</body>
</html>
