<div class="container">
    <div class="card">
        <div class="card-body">
            <h3>Remark for the Client</h3>
            <ul>
                <li>Security: All the information is hosted on a cloud server in Geneva Switzerland.</li>
                <li>Access & Permission: You can access the folders at any time. Further, you can
                    share one or all the folders to other relevant individuals directly.</li>
                <li>Data protection: We do not share your information or use it for other purposes
                    than the performance of our mandate. We do not keep other copies of this folder.</li>
                <li>Feel free to erase your data directly at the end of the mandate.</li>
                <li>IP: The content of the form is copyrighted.</li>
                <li>Filling out the form : Fill out the folder at your pace and share it with relevant
                    team members & stakeholders. You can mark the folders manually with a sign
                    (Check = Done, Round = in progress, Scope = to be reviewed, Cross or Blank = Not
                    needed).</li>
                <li>
                    Contact & Questions: We are here to help you. Fill out what you can, prepare a
                    document with your questions and remark hosted directly in this folder, set up a
                    meeting with us <a href="https://calendly.com/d/cgz-wph-dhh/arbitri-form-round-robin">https://calendly.com/d/cgz-wph-dhh/arbitri-form-round-robin</a>
                </li>
            </ul>
        </div>
    </div>


</div>