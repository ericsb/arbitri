<a>
    @if(flagStatus($tableName,$colName, $rowId, \App\Models\Flag::TYPES['check']))
        <img data-toggle="tooltip" data-placement="top" title="Mark this question for later" name="{{ $tableName.'-'.$colName.'-'.$rowId.'-'.'check' }}" width="20px" class="flag" src="https://form.arbitri.ch/assets/check-on.png" alt="Mark this question for later">
    @else
        <img data-toggle="tooltip" data-placement="top" title="Mark this question for later" name="{{ $tableName.'-'.$colName.'-'.$rowId.'-'.'check' }}" width="20px" class="flag" src="https://form.arbitri.ch/assets/check-off.png" alt="Mark this question for later">
    @endif
</a>

<a>
        @if(flagStatus($tableName,$colName, $rowId, \App\Models\Flag::TYPES['x']))
                <img data-toggle="tooltip" data-placement="top" title="Mark this question for later" name="{{ $tableName.'-'.$colName.'-'.$rowId.'-'.'x' }}" width="20px" class="flag" src="https://form.arbitri.ch/assets/x-on.png" alt="Mark this question for later">
        @else
                <img data-toggle="tooltip" data-placement="top" title="Mark this question for later" name="{{ $tableName.'-'.$colName.'-'.$rowId.'-'.'x' }}" width="20px" class="flag" src="https://form.arbitri.ch/assets/x-off.png" alt="Mark this question for later">
        @endif
</a>


<a>
        @if(flagStatus($tableName,$colName, $rowId, \App\Models\Flag::TYPES['gear']))
                <img data-toggle="tooltip" data-placement="top" title="Mark this question for later" name="{{ $tableName.'-'.$colName.'-'.$rowId.'-'.'gear' }}" width="20px" class="flag" src="https://form.arbitri.ch/assets/gear-on.png" alt="Mark this question for later">
        @else
                <img data-toggle="tooltip" data-placement="top" title="Mark this question for later" name="{{ $tableName.'-'.$colName.'-'.$rowId.'-'.'gear' }}" width="20px" class="flag" src="https://form.arbitri.ch/assets/gear-off.png" alt="Mark this question for later">
        @endif
</a>


<a>
        @if(flagStatus($tableName,$colName, $rowId, \App\Models\Flag::TYPES['search']))
                <img data-toggle="tooltip" data-placement="top" title="Mark this question for later" name="{{ $tableName.'-'.$colName.'-'.$rowId.'-'.'search' }}" width="20px" class="flag" src="https://form.arbitri.ch/assets/search-on.png" alt="Mark this question for later">
        @else
                <img data-toggle="tooltip" data-placement="top" title="Mark this question for later" name="{{ $tableName.'-'.$colName.'-'.$rowId.'-'.'search' }}" width="20px" class="flag" src="https://form.arbitri.ch/assets/search-off.png" alt="Mark this question for later">
        @endif
</a>