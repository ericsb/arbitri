@extends('master.admin-master')

@section('content')
    <div class="container">
        <h2>Users</h2>
        <div class="mt-2"></div>
        <form action="{{ route('admin.users.index') }}" method="get">
            <div class="input-group mb-3">
                <input type="text" name="search" class="form-control" placeholder="Search company name">
                <div class="input-group-append">
                    <button class="btn btn-success" type="submit">Go</button>
                </div>
            </div>
        </form>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>ID</th>
                <th>Company Name</th>
                <th>Email</th>
                <th>Joined At</th>
                <th>Login</th>
                <th>Account Status</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->name ?? '' }}</td>
                    <td>{{ $user->email ?? '' }}</td>
                    <td>{{ $user->created_at ?? '' }}</td>
                    <td><a target="_blank" href="{{ route('admin.users.login-as', $user->id) }}">Login as</a></td>
                    <td><a href="{{ route('admin.users.toggle-active', $user->id) }}">@if($user->is_active) <span style="color: green">Active ( Click to inactive )</span> @else <span style="color: red">Inactive ( Click to activate )</span> @endif</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection