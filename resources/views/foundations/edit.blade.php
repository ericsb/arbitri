@extends('master.master')

@section('content')
    <div class="container">
        <h2>Create a Company / Association or Foundation</h2>
        <hr>
        <div class="mt-2"></div>
        <div class="">
            <form action="{{ route('foundation.update') }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('post')
                @php $number = 1; @endphp

                <div>
                    <p>{{ $number++ }}.Form Type <x-flag tableName="foundations" colName="form_type" rowId="{{ $foundation->id }}" /></p>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="form_type" id="exampleRadios1" value="a" @if($foundation->form_type == "a") checked @endif>
                        <label class="form-check-label" >
                            SA
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="form_type" id="exampleRadios2" value="b"  @if($foundation->form_type == "b") checked @endif>
                        <label class="form-check-label" >
                            Sàrl
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="form_type" id="exampleRadios2" value="c"  @if($foundation->form_type == "c") checked @endif>
                        <label class="form-check-label" >
                            Association
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="form_type" id="exampleRadios2" value="d"  @if($foundation->form_type == "d") checked @endif>
                        <label class="form-check-label" >
                            Foundation
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="form_type" id="exampleRadios2" value="e"  @if($foundation->form_type == "e") checked @endif>
                        <label class="form-check-label" >
                            Other
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name">Fill this input if other: <x-flag tableName="foundations" colName="other_type" rowId="{{ $foundation->id }}" /></label>
                    <input type="text" name="other_type" class="form-control" value="{{ $foundation->other_type ?? '' }}" id="description">
                </div>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.Please type company’s form name in a swiss official
                        language (french/german/italian) (e.g. Société
                        simple, Raison individuelle, GmbH, etc.) <x-flag tableName="foundations" colName="form_type" rowId="{{ $foundation->id }}" /></label>
                    <input type="text" name="form_name" class="form-control" value="{{ $foundation->form_name ?? '' }}" id="description">
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.Company Name <x-flag tableName="foundations" colName="company_name" rowId="{{ $foundation->id }}" /></label>
                    <input type="text" name="company_name" class="form-control" value="{{ $foundation->company_name ?? '' }}" id="description">
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.Company alternative Name (if the other name is not
                        available) <x-flag tableName="foundations" colName="company_alternative_name" rowId="{{ $foundation->id }}" /></label>
                    <input type="text" name="company_alternative_name" class="form-control" value="{{ $foundation->company_alternative_name ?? '' }}" id="description">
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.Identity of the Founders <x-flag tableName="foundations" colName="founders" rowId="{{ $foundation->id }}" /></label>
                    @for($i=0; $i<10; $i++)
                        <input placeholder="name of founder {{ $i+1 }}"  type="text" name="founders[{{$i}}][name]" class="form-control form-inline mb-1" value="{{ $founders[$i]->name ?? '' }}" id="description">
                        <input placeholder="surname of founder {{ $i+1 }}" type="text" name="founders[{{$i}}][surname]" class="form-control form-inline " value="{{ $founders[$i]->surname ?? '' }}" id="description">
                        <br>
                    @endfor
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.Address of seat (Precise: address, number, postal
                        code and cc or co when necessary) <x-flag tableName="foundations" colName="address_of_seat" rowId="{{ $foundation->id }}" /></label>
                    <input type="text" name="address_of_seat" class="form-control" value="{{ $foundation->address_of_seat ?? '' }}" id="description">
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.Political municipality (e.g. Geneva) <x-flag tableName="foundations" colName="political_municipality" rowId="{{ $foundation->id }}" /></label>
                    <input type="text" name="political_municipality" class="form-control" value="{{ $foundation->political_municipality ?? '' }}" id="description">
                </div>
                <hr>
                <div>
                    <p>{{ $number++ }}.The company <x-flag tableName="foundations" colName="company_office_type" rowId="{{ $foundation->id }}" /></p>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="company_office_type" id="exampleRadios1" value="a" @if($foundation->company_office_type == "a") checked @endif>
                        <label class="form-check-label" >
                            Has its own offices (If you are looking for officies choose this option
                            and fill out a request in this sense in the folder “3. Domiciliation &
                            Residence”
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="company_office_type" id="exampleRadios2" value="b"  @if($foundation->company_office_type == "b") checked @endif>
                        <label class="form-check-label" >
                            Is domiciled by a third-party
                        </label>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.Acceptance of the election of domicile [not
                        mandatory] If the company or association is
                        domiciled with a third-party (e.g. auditor, at one of
                        the founder’s place, etc.) please attach the
                        acceptance of the election of domicile. This
                        document must be presented at the latest at the
                        time of the meeting with the notary <x-flag tableName="foundations" colName="electionFile" rowId="{{ $foundation->id }}" /></label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'foundations', @$foundation->electionFile->name) }}">
                        @if(@$foundation->electionFile->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="election_file" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.Goals (Precise the goals of the company in 5-10 lines.
                        List the domain of activities etc.) <x-flag tableName="foundations" colName="goals" rowId="{{ $foundation->id }}" /></label>
                    <textarea name="goals" class="form-control" id="description">{{ $foundation->goals ?? '' }}</textarea>
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.Does the goals involve or are in relation with real
                        estate (like trading real estate) or a property based in
                        Switzerland? (If yes, the notary will requires some
                        further documents) <x-flag tableName="foundations" colName="goals_involve" rowId="{{ $foundation->id }}" />
                    </label>
                    <textarea name="goals_involve" class="form-control" id="description">{{ $foundation->goals_involve ?? '' }}</textarea>
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.Further documents as requested and indicated by
                        the notary [not mandatory] ( File 1 ) <x-flag tableName="foundations" colName="furtherDocumentFile1" rowId="{{ $foundation->id }}" /></label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'foundations', @$foundation->furtherDocumentFile1->name) }}">
                        @if(@$foundation->furtherDocumentFile1->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="further_document_file_1" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.Further documents as requested and indicated by
                        the notary [not mandatory] ( File 2 ) <x-flag tableName="foundations" colName="furtherDocumentFile2" rowId="{{ $foundation->id }}" /></label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'foundations', @$foundation->furtherDocumentFile2->name) }}">
                        @if(@$foundation->furtherDocumentFile1->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="further_document_file_2" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.Further documents as requested and indicated by
                        the notary [not mandatory] ( File 3 ) <x-flag tableName="foundations" colName="furtherDocumentFile3" rowId="{{ $foundation->id }}" /></label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'foundations', @$foundation->furtherDocumentFile3->name) }}">
                        @if(@$foundation->furtherDocumentFile3->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="further_document_file_3" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.Capital (Minimum in CHF: Sàrl = 20’000.- / SA = 50’000.-
                        liberated / Association = 0.-) Not applicable for
                        Association <x-flag tableName="foundations" colName="capital" rowId="{{ $foundation->id }}" /></label>
                    <input type="text" name="capital" class="form-control" value="{{ $foundation->capital ?? '' }}" id="description">
                </div>
                <hr>
                <div>
                    <p>{{ $number++ }}.Is the capital fully liberated (vested) at the time of
                        the constitution? <x-flag tableName="foundations" colName="capital_liberated" rowId="{{ $foundation->id }}" /></p>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="capital_liberated" id="exampleRadios1" value="a" @if($foundation->capital_liberated == "a") checked @endif>
                        <label class="form-check-label" >
                            Yes
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="capital_liberated" id="exampleRadios2" value="b"  @if($foundation->capital_liberated == "b") checked @endif>
                        <label class="form-check-label" >
                            No
                        </label>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.How much will be liberated in CHF? <x-flag tableName="foundations" colName="liberated_in_chf" rowId="{{ $foundation->id }}" /></label>
                    <input type="text" name="liberated_in_chf" class="form-control" value="{{ $foundation->liberated_in_chf ?? '' }}" id="description">
                </div>
                <hr>
                <div>
                    <p>{{ $number++ }}.What type of liberation is it? <x-flag tableName="foundations" colName="type_of_liberation" rowId="{{ $foundation->id }}" /></p>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="form_type" id="exampleRadios1" value="a" @if($foundation->type_of_liberation == "a") checked @endif>
                        <label class="form-check-label" >
                            In cash only CHF
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="form_type" id="exampleRadios2" value="b"  @if($foundation->type_of_liberation == "b") checked @endif>
                        <label class="form-check-label" >
                            In kind only (E.g. website, application, building, etc.) (If yes to be
                            discussed further)
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="form_type" id="exampleRadios2" value="c"  @if($foundation->type_of_liberation == "c") checked @endif>
                        <label class="form-check-label" >
                            In kind and in cash (to be discussed with the notary)
                        </label>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.Bank where the capital in CHF is held in escrow <x-flag tableName="foundations" colName="bank_location_in_escrow" rowId="{{ $foundation->id }}" /></label>
                    <input type="text" name="bank_location_in_escrow" class="form-control" value="{{ $foundation->bank_location_in_escrow ?? '' }}" id="description">
                </div>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.Name of the Bank where the capital is held in escrow <x-flag tableName="foundations" colName="bank_name_in_escrow" rowId="{{ $foundation->id }}" /></label>
                    <input type="text" name="bank_name_in_escrow" class="form-control" value="{{ $foundation->bank_name_in_escrow ?? '' }}" id="description">
                </div>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.Attestation certificate (N.b. this document is sent by the bank. The original of
                        the document must be presented to the notary on the day of the signature.
                        Please refer to the Banking folder below) <x-flag tableName="foundations" colName="attestationFile" rowId="{{ $foundation->id }}" /></label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'foundations', @$foundation->attestationFile->name) }}">
                        @if(@$foundation->attestationFile->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="attestation_certificate_file" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.Precise how much will be liberated in cash and what will be the value of the
                        contribution in kind? <x-flag tableName="foundations" colName="liberated_in_cash" rowId="{{ $foundation->id }}" /></label>
                    <input type="text" name="liberated_in_cash" class="form-control" value="{{ $foundation->liberated_in_cash ?? '' }}" id="description">
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.Nature of contribution in kind <x-flag tableName="foundations" colName="nature_of_contribution" rowId="{{ $foundation->id }}" /></label>
                    <input type="text" name="nature_of_contribution" class="form-control" value="{{ $foundation->nature_of_contribution ?? '' }}" id="description">
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.What will be the nature of the contribution in kind? (List the contribution and
                        your estimation of their value in CHF) <x-flag tableName="foundations" colName="will_nature_of_contribution" rowId="{{ $foundation->id }}" /></label>
                    <input type="text" name="will_nature_of_contribution" class="form-control" value="{{ $foundation->will_nature_of_contribution ?? '' }}" id="description">
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.Foundation report (must be signed by all Founders) This document must be
                        presented at the latest at the time of the meeting with the notary. <x-flag tableName="foundations" colName="foundation_report" rowId="{{ $foundation->id }}" />
                    </label>
                    <input type="text" name="foundation_report" class="form-control" value="{{ $foundation->foundation_report ?? '' }}" id="description">
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.Foundation contract (must be signed by all Founders)This document must be
                        presented at the latest at the time of the meeting with the notary. <x-flag tableName="foundations" colName="foundation_contract" rowId="{{ $foundation->id }}" />
                    </label>
                    <input type="text" name="foundation_contract" class="form-control" value="{{ $foundation->foundation_contract ?? '' }}" id="description">
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.Auditor providing the certificate, which aknowledges the value of contribution
                        in kind. This document will be sent by the auditor and must be presented at the
                        latest at the time of the meeting with the notary. (The auditor should be Swiss) <x-flag tableName="foundations" colName="auditor_providing" rowId="{{ $foundation->id }}" />
                    </label>
                    <input type="text" name="auditor_providing" class="form-control" value="{{ $foundation->auditor_providing ?? '' }}" id="description">
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.Identity of the swiss auditor (company name) <x-flag tableName="foundations" colName="identity_of_swiss_auditor" rowId="{{ $foundation->id }}" />
                    </label>
                    <input type="text" name="identity_of_swiss_auditor" class="form-control" value="{{ $foundation->identity_of_swiss_auditor ?? '' }}" id="description">
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.Website of the auditor <x-flag tableName="foundations" colName="website_of_auditor" rowId="{{ $foundation->id }}" />
                    </label>
                    <input type="text" name="website_of_auditor" class="form-control" value="{{ $foundation->website_of_auditor ?? '' }}" id="description">
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.Email of the contact person <x-flag tableName="foundations" colName="email_of_contact" rowId="{{ $foundation->id }}" />
                    </label>
                    <input type="text" name="email_of_contact" class="form-control" value="{{ $foundation->email_of_contact ?? '' }}" id="description">
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.Detailed inventory <x-flag tableName="foundations" colName="detailedInventoryFile" rowId="{{ $foundation->id }}" />
                    </label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'foundations', @$foundation->detailedInventoryFile->name) }}">
                        @if(@$foundation->detailedInventoryFile->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="detailed_inventory_file" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.Balance sheet of the contribution <x-flag tableName="foundations" colName="balanceSheetFile" rowId="{{ $foundation->id }}" />
                    </label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'foundations', @$foundation->balanceSheetFile->name) }}">
                        @if(@$foundation->balanceSheetFile->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="balance_sheet_file" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <br>
                <hr>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.Number of shares <x-flag tableName="foundations" colName="number_of_shares" rowId="{{ $foundation->id }}" />
                    </label>
                    <input type="text" name="number_of_shares" class="form-control" value="{{ $foundation->number_of_shares ?? '' }}" id="description">
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.Nominal value of per share in CHF <x-flag tableName="foundations" colName="nominal_value" rowId="{{ $foundation->id }}" />
                    </label>
                    <input type="text" name="nominal_value" class="form-control" value="{{ $foundation->nominal_value ?? '' }}" id="description">
                </div>
                <hr>
                <div>
                    <p>{{ $number++ }}.What is the nature of the share? <x-flag tableName="foundations" colName="nature_of_the_share" rowId="{{ $foundation->id }}" /></p>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="nature_of_the_share" id="exampleRadios1" value="a" @if($foundation->nature_of_the_share == "a") checked @endif>
                        <label class="form-check-label" >
                            In cash only CHF
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="nature_of_the_share" id="exampleRadios2" value="b"  @if($foundation->nature_of_the_share == "b") checked @endif>
                        <label class="form-check-label" >
                            Nominative share (by default)
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="nature_of_the_share" id="exampleRadios2" value="c"  @if($foundation->nature_of_the_share == "c") checked @endif>
                        <label class="form-check-label" >
                            Others (To be discussed with the notary
                        </label>
                    </div>
                </div>
                <hr>
                <div>
                    <p>{{ $number++ }}.Communication <x-flag tableName="foundations" colName="communication" rowId="{{ $foundation->id }}" /></p>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="communication[]" id="exampleRadios1" value="a" @if($foundation->nature_of_the_share == "a") checked @endif>
                        <label class="form-check-label" >
                            In writing (e.g. simple letter)
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="communication[]" id="exampleRadios2" value="b"  @if($foundation->nature_of_the_share == "b") checked @endif>
                        <label class="form-check-label" >
                            Registered letter
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="communication[]" id="exampleRadios2" value="c"  @if($foundation->nature_of_the_share == "c") checked @endif>
                        <label class="form-check-label" >
                            Fax
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="communication[]" id="exampleRadios2" value="c"  @if($foundation->nature_of_the_share == "c") checked @endif>
                        <label class="form-check-label" >
                            Email
                        </label>
                    </div>
                </div>
                <hr>
                <div>
                    <p>{{ $number++ }}.Issuance of all shares by the Notary <x-flag tableName="foundations" colName="issuance_of_all" rowId="{{ $foundation->id }}" /></p>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="issuance_of_all" id="exampleRadios1" value="a" @if($foundation->issuance_of_all == "a") checked @endif>
                        <label class="form-check-label" >
                            Yes
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="issuance_of_all" id="exampleRadios2" value="b"  @if($foundation->issuance_of_all == "b") checked @endif>
                        <label class="form-check-label" >
                            No
                        </label>
                    </div>
                </div>
                <hr>
                <div>
                    <p>{{ $number++ }}.Issuance of share certificate(s) <x-flag tableName="foundations" colName="issuance_of_share" rowId="{{ $foundation->id }}" /></p>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="issuance_of_share" id="exampleRadios1" value="a" @if($foundation->issuance_of_share == "a") checked @endif>
                        <label class="form-check-label" >
                            Yes
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="issuance_of_share" id="exampleRadios2" value="b"  @if($foundation->issuance_of_share == "b") checked @endif>
                        <label class="form-check-label" >
                            No
                        </label>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.Detail regarding the Issuance of share certificate <x-flag tableName="foundations" colName="detail_regarding" rowId="{{ $foundation->id }}" />
                    </label>
                    <input type="text" name="detail_regarding" class="form-control" value="{{ $foundation->detail_regarding ?? '' }}" id="description">
                </div>
                <hr>
                <div>
                    <p>{{ $number++ }}.Does the company plan to acquire another
                        company or an asset in possession of one of the
                        Founders now or in the near future? (e.g. cession of
                        the shares from another corporation hold by the
                        founder to the new company) <x-flag tableName="foundations" colName="plan_to_acquire_another_company" rowId="{{ $foundation->id }}" /></p>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="plan_to_acquire_another_company" id="exampleRadios1" value="a" @if($foundation->plan_to_acquire_another_company == "a") checked @endif>
                        <label class="form-check-label" >
                            Yes
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="plan_to_acquire_another_company" id="exampleRadios2" value="b"  @if($foundation->plan_to_acquire_another_company == "b") checked @endif>
                        <label class="form-check-label" >
                            No
                        </label>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.Foundation contract (must be signed by all Founders) This document must be
                        presented at the latest at the time of the meeting with the notary. <x-flag tableName="foundations" colName="anotherFoundationContractFile" rowId="{{ $foundation->id }}" /></label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'foundations', @$foundation->anotherFoundationContractFile->name) }}">
                        @if(@$foundation->anotherFoundationContractFile->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="another_company_foundation_contract_file" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.Takeover contract (must be signed by all Founders) (This document must be
                        presented at the latest at the time of the meeting with the notary.) <x-flag tableName="foundations" colName="anotherTakeoverContractFile" rowId="{{ $foundation->id }}" /></label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'foundations', @$foundation->anotherTakeoverContractFile->name) }}">
                        @if(@$foundation->anotherTakeoverContractFile->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="another_company_takeover_contract_file" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.Auditor providing the certificate in relation to the takeover. (This document
                        must be presented at the latest at the time of the meeting with the notary) <x-flag tableName="foundations" colName="anotherAuditorFile" rowId="{{ $foundation->id }}" /></label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'foundations', @$foundation->anotherAuditorFile->name) }}">
                        @if(@$foundation->anotherAuditorFile->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="another_company_auditor_providing_file" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.Identity of the auditor (company name) (The auditor should be Swiss) <x-flag tableName="foundations" colName="another_company_identity_of_swiss_auditor" rowId="{{ $foundation->id }}" />
                    </label>
                    <input type="text" name="another_company_identity_of_swiss_auditor" class="form-control" value="{{ $foundation->another_company_identity_of_swiss_auditor ?? '' }}" id="description">
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.Website of the auditor <x-flag tableName="foundations" colName="another_company_website_of_auditor" rowId="{{ $foundation->id }}" />
                    </label>
                    <input type="text" name="another_company_website_of_auditor" class="form-control" value="{{ $foundation->another_company_website_of_auditor ?? '' }}" id="description">
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.Email of the contact person <x-flag tableName="foundations" colName="another_company_email_of_contact" rowId="{{ $foundation->id }}" />
                    </label>
                    <input type="text" name="another_company_email_of_contact" class="form-control" value="{{ $foundation->another_company_email_of_contact ?? '' }}" id="description">
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.Direction (Depending on the form: Board of Directors /
                        Committee of Association / Board of Foundation) <x-flag tableName="foundations" colName="directions" rowId="{{ $foundation->id }}" /></label>
                    @for($i=0; $i<10; $i++)
                        <p>Person {{ $i+1 }}</p>
                        <input placeholder="Name {{ $i+1 }}"  type="text" name="directions[{{$i}}][name]" class="form-control form-inline mb-1" value="{{ $directions[$i]->name ?? '' }}" id="description">
                        <input placeholder="Surname {{ $i+1 }}" type="text" name="directions[{{$i}}][surname]" class="form-control form-inline mb-1" value="{{ $directions[$i]->surname ?? '' }}" id="description">
                        <input placeholder="Role(s) (e.g President, Vice-President, Member of the Board, etc.) (Associations
need at least a President, secretary, treasurer ; Companies and Foundations
need at least to specify who is the President) {{ $i+1 }}" type="text" name="directions[{{$i}}][role]" class="form-control form-inline mb-1" value="{{ $directions[$i]->role ?? '' }}" id="description">
                        <input placeholder="signature power {{ $i+1 }}: Individual signature / Collective signature / According to the Statutes
/ Other" type="text" name="directions[{{$i}}][signature_power]" class="form-control form-inline mb-1" value="{{ $directions[$i]->signature_power ?? '' }}" id="description">
                        <br>
                    @endfor
                </div>
                <hr>
                <div>
                    <p>{{ $number++ }}.Does at least one member of the Board is domiciled in Switzerland and has
                        individual signatory power? <x-flag tableName="foundations" colName="member_signature_power" rowId="{{ $foundation->id }}" /></p>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="member_signature_power" id="exampleRadios1" value="a" @if($foundation->member_signature_power == "a") checked @endif>
                        <label class="form-check-label" >
                            Yes
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="member_signature_power" id="exampleRadios2" value="b"  @if($foundation->member_signature_power == "b") checked @endif>
                        <label class="form-check-label" >
                            No (Swiss law requires for corporations like SA or Sàrl that at
                            least one of the board members is domiciled in Switzerland and
                            has individual signatory power. We recommend for any entity to
                            fill out this condition as it ease the administration locally. In any
                            case, we can provide a Swiss administrator if needs be. Please
                            specify that you want Arbitri to propose you someone in the
                            folder “7. Employment & ali” )

                        </label>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.Shareholders <x-flag tableName="foundations" colName="shareholders" rowId="{{ $foundation->id }}" /></label>
                    @for($i=0; $i<10; $i++)
                        <p>Person {{ $i+1 }}</p>
                        <input placeholder="Name {{ $i+1 }}"  type="text" name="shareholders[{{$i}}][name]" class="form-control form-inline mb-1" value="{{ $shareholders[$i]->name ?? '' }}" id="description">
                        <input placeholder="Surname {{ $i+1 }}" type="text" name="shareholders[{{$i}}][surname]" class="form-control form-inline mb-1" value="{{ $shareholders[$i]->surname ?? '' }}" id="description">
                        <input placeholder="Number of shares {{ $i+1 }}" type="text" name="shareholders[{{$i}}][number_of_shares]" class="form-control form-inline mb-1" value="{{ $shareholders[$i]->number_of_shares ?? '' }}" id="description">
                        <br>
                    @endfor
                </div>
                <hr>
                <div>
                    <p>{{ $number++ }}.What type of audit is applicable? <x-flag tableName="foundations" colName="audit_applicable" rowId="{{ $foundation->id }}" /></p>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="audit_applicable" id="exampleRadios1" value="a" @if($foundation->audit_applicable == "a") checked @endif>
                        <label class="form-check-label" >
                            No audit (recommended when not mandatory. This possibility is
                            possible for small company of less than 10 employees and for small
                            association or foundation (69b CC / 83b CC / 727 CO) )
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="audit_applicable" id="exampleRadios2" value="b"  @if($foundation->audit_applicable == "b") checked @endif>
                        <label class="form-check-label" >
                            Limited audit (69b CC / 83b CC / 727 CO)
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="audit_applicable" id="exampleRadios2" value="b"  @if($foundation->audit_applicable == "b") checked @endif>
                        <label class="form-check-label" >
                            Ordinary audit (69b CC / 83b CC / 727 CO)
                        </label>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.Who will be the auditor? <x-flag tableName="foundations" colName="who_will_auditor" rowId="{{ $foundation->id }}" />
                    </label>
                    <input type="text" name="who_will_auditor" class="form-control" value="{{ $foundation->who_will_auditor ?? '' }}" id="description">
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.Declaration of acceptance of the mandate from <x-flag tableName="foundations" colName="mandateFile" rowId="{{ $foundation->id }}" />
                        the Auditor</label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'foundations', @$foundation->mandateFile->name) }}">
                        @if(@$foundation->mandateFile->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="mandate_file" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.Excerpt from the commercial register regarding
                        the Auditor’s company (the date of the excerpt
                        cannot be older than 2 months at the time of the
                        creation) <x-flag tableName="foundations" colName="excerptFile" rowId="{{ $foundation->id }}" /></label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'foundations', @$foundation->excerptFile->name) }}">
                        @if(@$foundation->excerptFile->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="excerpt_file" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <hr>
                <div>
                    <p>{{ $number++ }}.Financial year <x-flag tableName="foundations" colName="financial_year" rowId="{{ $foundation->id }}" /></p>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="financial_year" id="exampleRadios1" value="a" @if($foundation->financial_year == "a") checked @endif>
                        <label class="form-check-label" >
                            Financial year begins on the 1st of January and ends on the 31st
                            of December
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="financial_year" id="exampleRadios2" value="b"  @if($foundation->financial_year == "b") checked @endif>
                        <label class="form-check-label" >
                            Other
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name">If no, please specify <x-flag tableName="foundations" colName="financial_year_comment" rowId="{{ $foundation->id }}" />
                    </label>
                    <input type="text" name="financial_year_comment" class="form-control" value="{{ $foundation->financial_year_comment ?? '' }}" id="description">
                </div>
                <hr>
                <div>
                    <p>{{ $number++ }}.Closing of the first financial year <x-flag tableName="foundations" colName="closing_of_the_first_financial_year" rowId="{{ $foundation->id }}" /></p>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="closing_of_the_first_financial_year" id="exampleRadios1" value="a" @if($foundation->closing_of_the_first_financial_year == "a") checked @endif>
                        <label class="form-check-label" >
                            Financial year ends on the 31st of December of the year of the creation
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="closing_of_the_first_financial_year" id="exampleRadios2" value="b"  @if($foundation->closing_of_the_first_financial_year == "b") checked @endif>
                        <label class="form-check-label" >
                            Other
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name">If no, please specify <x-flag tableName="foundations" colName="closing_of_the_first_financial_year_comment" rowId="{{ $foundation->id }}" />
                    </label>
                    <input type="text" name="closing_of_the_first_financial_year_comment" class="form-control" value="{{ $foundation->closing_of_the_first_financial_year_comment ?? '' }}" id="description">
                </div>
                <hr>
                <div>
                    <p>{{ $number++ }}.Do you wish to set up a minimum quorum for taking decisions? <x-flag tableName="foundations" colName="minimum_quorum" rowId="{{ $foundation->id }}" /></p>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="minimum_quorum" id="exampleRadios1" value="a" @if($foundation->minimum_quorum == "a") checked @endif>
                        <label class="form-check-label" >
                            Yes
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="minimum_quorum" id="exampleRadios2" value="b"  @if($foundation->minimum_quorum == "b") checked @endif>
                        <label class="form-check-label" >
                            No
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name">If no, please specify <x-flag tableName="foundations" colName="minimum_quorum_comment" rowId="{{ $foundation->id }}" />
                    </label>
                    <input type="text" name="minimum_quorum_comment" class="form-control" value="{{ $foundation->minimum_quorum_comment ?? '' }}" id="description">
                </div>
                <hr>
                <div>
                    <p>{{ $number++ }}.Is a qualified majority required? <x-flag tableName="foundations" colName="majority_required" rowId="{{ $foundation->id }}" />
                    </p>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="majority_required" id="exampleRadios1" value="a" @if($foundation->majority_required == "a") checked @endif>
                        <label class="form-check-label" >
                            Yes
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="majority_required" id="exampleRadios2" value="b"  @if($foundation->majority_required == "b") checked @endif>
                        <label class="form-check-label" >
                            No
                        </label>
                    </div>
                </div>
                <hr>
                <div>
                    <p>{{ $number++ }}.Do you need a fiduciary contract? (Please discuss it
                        with the notary or your legal counsel) <x-flag tableName="foundations" colName="fiduciary_contract" rowId="{{ $foundation->id }}" />
                    </p>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="fiduciary_contract" id="exampleRadios1" value="a" @if($foundation->fiduciary_contract == "a") checked @endif>
                        <label class="form-check-label" >
                            Yes
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="fiduciary_contract" id="exampleRadios2" value="b"  @if($foundation->fiduciary_contract == "b") checked @endif>
                        <label class="form-check-label" >
                            No
                        </label>
                    </div>
                </div>
                <hr>
                <div>
                    <p>{{ $number++ }}.Do you need a shareholder agreement?Please discuss it
                        with the notary or your legal counsel) <x-flag tableName="foundations" colName="shareholder_agreement" rowId="{{ $foundation->id }}" />
                    </p>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="shareholder_agreement" id="exampleRadios1" value="a" @if($foundation->shareholder_agreement == "a") checked @endif>
                        <label class="form-check-label" >
                            Yes
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="shareholder_agreement" id="exampleRadios2" value="b"  @if($foundation->shareholder_agreement == "b") checked @endif>
                        <label class="form-check-label" >
                            No
                        </label>
                    </div>
                </div>
                <hr>
                <div>
                    <p>{{ $number++ }}.In the eventuality the entity does not
                        mandatorily receive or need a legal personality, does
                        it plan to request one from the Commercial Register
                        anyway? <x-flag tableName="foundations" colName="commercial_register" rowId="{{ $foundation->id }}" />
                    </p>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="commercial_register" id="exampleRadios1" value="a" @if($foundation->commercial_register == "a") checked @endif>
                        <label class="form-check-label" >
                            Yes
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="commercial_register" id="exampleRadios2" value="b"  @if($foundation->commercial_register == "b") checked @endif>
                        <label class="form-check-label" >
                            No
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label for="name">{{ $number++ }}.If yes, please specify the name & surname of the
                        person who will request it <x-flag tableName="foundations" colName="commercial_register_person" rowId="{{ $foundation->id }}" />
                    </label>
                    <input type="text" name="commercial_register_person" class="form-control" value="{{ $foundation->commercial_register_person ?? '' }}" id="description">
                </div>
                <hr>
                <div>
                    <p>63.Does the entity require a tax exemption status?
                        (art.56 al.1 le.g LIFD) <x-flag tableName="foundations" colName="tax_exemption" rowId="{{ $foundation->id }}" />
                    </p>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="tax_exemption" id="exampleRadios1" value="a" @if($foundation->tax_exemption == "a") checked @endif>
                        <label class="form-check-label" >
                            Yes (please discuss this point with your legal counsel)
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="tax_exemption" id="exampleRadios2" value="b"  @if($foundation->tax_exemption == "b") checked @endif>
                        <label class="form-check-label" >
                            No
                        </label>
                    </div>
                </div>
                <hr>
                <div>
                    <p>{{ $number++ }}.Does this entity require a tax ruling from the
                        tax Authorities? <x-flag tableName="foundations" colName="tax_ruling" rowId="{{ $foundation->id }}" />
                    </p>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="tax_ruling" id="exampleRadios1" value="a" @if($foundation->tax_ruling == "a") checked @endif>
                        <label class="form-check-label" >
                            Yes (please discuss this point with your legal counsel)
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="tax_ruling" id="exampleRadios2" value="b"  @if($foundation->tax_ruling == "b") checked @endif>
                        <label class="form-check-label" >
                            No
                        </label>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.Is this new company part of a group of companies, a
                        financial conglomerate or is it a subsidiary? If yes give us
                        some details <x-flag tableName="foundations" colName="part_of_group" rowId="{{ $foundation->id }}" />
                    </label>
                    <input type="text" name="part_of_group" class="form-control" value="{{ $foundation->part_of_group ?? '' }}" id="description">
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.How many people will be employed by this entity? <x-flag tableName="foundations" colName="people_count" rowId="{{ $foundation->id }}" />
                    </label>
                    <input type="text" name="people_count" class="form-control" value="{{ $foundation->people_count ?? '' }}" id="description">
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.Do you have any comments or additional information to
                        provide for this section? <x-flag tableName="foundations" colName="additional_comments" rowId="{{ $foundation->id }}" />
                    </label>
                    <input type="text" name="additional_comments" class="form-control" value="{{ $foundation->additional_comments ?? '' }}" id="description">
                </div>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.Additional Files 1: <x-flag tableName="foundations" colName="additionalFile1" rowId="{{ $foundation->id }}" /></label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'foundations', @$foundation->additionalFile1->name) }}">
                        @if(@$foundation->additionalFile1->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="additional_file_1" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.Additional Files 2: <x-flag tableName="foundations" colName="additionalFile2" rowId="{{ $foundation->id }}" /></label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'foundations', @$foundation->additionalFile2->name) }}">
                        @if(@$foundation->additionalFile2->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="additional_file_2" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name">{{ $number++ }}.Additional Files 3: <x-flag tableName="foundations" colName="additionalFile3" rowId="{{ $foundation->id }}" /></label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'foundations', @$foundation->additionalFile3->name) }}">
                        @if(@$foundation->additionalFile3->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="additional_file_3" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Save!</button>
            </form>
        </div>
    </div>
@endsection