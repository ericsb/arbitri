@extends('master.master')

@section('content')
    <div class="container">
        <h2>Special requests</h2>
        <div class="mt-2"></div>
        <div class="">
            <form action="{{ route('special_request.update') }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('post')
                <div class="form-group">
                    <label for="name">1.Do you have any other requests? <x-flag tableName="special_requests" colName="description" rowId="{{ $specialRequest->id }}" /></label>
                    <input type="text" name="description" class="form-control" value="{{ $specialRequest->description ?? '' }}" id="description">
                </div>
                <div class="form-group">
                    <label for="name">Additional files (1): <x-flag tableName="special_requests" colName="file1" rowId="{{ $specialRequest->id }}" /></label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'special_requests', @$specialRequest->file1->name) }}">
                        @if(@$specialRequest->file1->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="file1" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>

                </div>
                <div class="form-group">
                    <label for="name">2.Additional files (2): <x-flag tableName="special_requests" colName="file2" rowId="{{ $specialRequest->id }}" /></label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'special_requests', @$specialRequest->file2->name) }}">
                        @if(@$specialRequest->file2->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="file2" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name">3.Additional files (3): <x-flag tableName="special_requests" colName="file3" rowId="{{ $specialRequest->id }}" /></label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'special_requests', @$specialRequest->file3->name) }}">
                        @if(@$specialRequest->file3->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="file3" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name">4.Additional files (4): <x-flag tableName="special_requests" colName="file4" rowId="{{ $specialRequest->id }}" /></label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'special_requests', @$specialRequest->file4->name) }}">
                        @if(@$specialRequest->file4->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="file4" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Save!</button>
            </form>
        </div>
    </div>
@endsection