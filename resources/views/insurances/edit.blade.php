@extends('master.master')

@section('content')
    <div class="container">
        <h2>Company Insurance</h2>
        <hr>
        <div class="mt-2"></div>
        <div class="">
            <form action="{{ route('insurances.update') }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('post')
                <p>1.Do you want to conclude an insurance policy for your
                    company, the employees or the company’s assets?
                    (N.b. Some insurances e.g. accidents are mandatory
                    under Swiss law; see AVS, AI, AMat, APG, AC, LPP 2 &
                    3 pillar, LAA) <x-flag tableName="insurances" colName="conclude_insurance_status" rowId="{{ $insurance->id }}" />
                </p>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="conclude_insurance_status" id="exampleRadios1" value="Yes" @if($insurance->conclude_insurance_status == "Yes") checked @endif>
                    <label class="form-check-label" >
                        Yes
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="conclude_insurance_status" id="exampleRadios2" value="No"  @if($insurance->conclude_insurance_status == "No") checked @endif>
                    <label class="form-check-label" >
                        No
                    </label>
                </div>

                <div class="form-group">
                    <label for="conclude_insurance_comment">2.If yes, one in particular or you are potentially
                        interested in all? <x-flag tableName="insurances" colName="conclude_insurance_comment" rowId="{{ $insurance->id }}" />
                    </label>
                    <input type="text" name="conclude_insurance_comment" class="form-control" value="{{ $insurance->conclude_insurance_comment ?? '' }}" id="conclude_insurance_comment">
                </div>
                <hr>
                <p>3.Do you want to conclude a private insurance policy
                    for yourself or help your employees conclude one?
                    (E.g. Health, Dental, Travel, etc.; AVS, AI, AMat, APG,
                    AC, LPP 2 & 3 pillar, LAA) <x-flag tableName="insurances" colName="conclude_private_status" rowId="{{ $insurance->id }}" />
                </p>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="conclude_private_status" id="exampleRadios1" value="Yes" @if($insurance->conclude_private_status == "Yes") checked @endif>
                    <label class="form-check-label" >
                        Yes
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="conclude_private_status" id="exampleRadios2" value="No"  @if($insurance->conclude_private_status == "No") checked @endif>
                    <label class="form-check-label" >
                        No
                    </label>
                </div>

                <div class="form-group">
                    <label for="conclude_private_comment">4.If yes, one in particular or you are potentially
                        interested in all? <x-flag tableName="insurances" colName="conclude_private_comment" rowId="{{ $insurance->id }}" />
                    </label>
                    <input type="text" name="conclude_private_comment" class="form-control" value="{{ $insurance->conclude_private_comment ?? '' }}" id="conclude_private_comment">
                </div>
                <hr>
                <div class="form-group">
                    <label for="specific_requirements">5.Do you have specific requirements or needs regarding
                        the conclusion of an insurance policy? (Partially
                        Mandatory under Swiss law) <x-flag tableName="insurances" colName="specific_requirements" rowId="{{ $insurance->id }}" />
                    </label>
                    <input type="text" name="specific_requirements" class="form-control" value="{{ $insurance->specific_requirements ?? '' }}" id="specific_requirements">
                </div>
                <hr>
                <div class="form-group">
                    <label for="additional_comments">6.Do you have any comments or additional information to
                        provide for this section? <x-flag tableName="insurances" colName="additional_comments" rowId="{{ $insurance->id }}" />
                    </label>
                    <input type="text" name="additional_comments" class="form-control" value="{{ $insurance->additional_comments ?? '' }}" id="additional_comments">
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">7.Additional files (1): <x-flag tableName="insurances" colName="file1" rowId="{{ $insurance->id }}" /></label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'insurances', @$insurance->file1->name) }}">
                        @if(@$insurance->file1->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="file1" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">8.Additional files (2): <x-flag tableName="insurances" colName="file2" rowId="{{ $insurance->id }}" /></label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'insurances', @$insurance->file2->name) }}">
                        @if(@$insurance->file2->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="file2" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">9.Additional files (3): <x-flag tableName="insurances" colName="file3" rowId="{{ $insurance->id }}" /></label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'insurances', @$insurance->file3->name) }}">
                        @if(@$insurance->file3->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="file3" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <hr>
                <button type="submit" class="btn btn-primary">Save!</button>
            </form>
        </div>
    </div>
@endsection