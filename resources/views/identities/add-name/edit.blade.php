@extends('master.master')

@section('content')
    <div class="container">
        <h2>Identities - Add Name</h2>
        <div class="mt-2"></div>
        <div class="">
            <form action="{{ route('identities.add-name.update', $identity->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('post')
                <div class="form-group">
                    <label >1.Name (official): <x-flag tableName="identities" colName="name" rowId="{{ $identity->id }}" /></label>
                    <input type="text" name="name" class="form-control" value="{{ $identity->name }}" id="name">
                </div>
                <div class="form-group">
                    <label >2.Surname (official): <x-flag tableName="identities" colName="surname" rowId="{{ $identity->id }}" /></label>
                    <input type="text" name="surname" class="form-control" value="{{ $identity->surname }}" >
                </div>
                <div class="form-group">
                    <label >3.Birth date (DD/MM/YYYY) : <x-flag tableName="identities" colName="birth_date" rowId="{{ $identity->id }}" /></label>
                    <input type="text" name="birth_date" class="form-control" value="{{ $identity->birth_date ?? '' }}" >
                </div>
                <div class="form-group">
                    <label >4.Address of domicile: <x-flag tableName="identities" colName="address_of_domicile" rowId="{{ $identity->id }}" /></label>
                    <input type="text" name="address_of_domicile" class="form-control" value="{{ $identity->address_of_domicile }}" >
                </div>
                <div class="form-group">
                    <label >5.Phone number: <x-flag tableName="identities" colName="phone_number" rowId="{{ $identity->id }}" /></label>
                    <input type="text" name="phone_number" class="form-control" value="{{ $identity->phone_number }}" >
                </div>
                <div class="form-group">
                    <label >6.Email: <x-flag tableName="identities" colName="email" rowId="{{ $identity->id }}" /></label>
                    <input type="text" name="email" class="form-control" value="{{ $identity->email }}" >
                </div>
                <div class="form-group">
                    <label >7.Profession: <x-flag tableName="identities" colName="profession" rowId="{{ $identity->id }}" /></label>
                    <input type="text" name="profession" class="form-control" value="{{ $identity->profession }}" >
                </div>
                <div class="form-group">
                    <label >8.Company’s name [not mandatory]: <x-flag tableName="identities" colName="company_name" rowId="{{ $identity->id }}" /></label>
                    <input type="text" name="company_name" class="form-control" value="{{ $identity->company_name }}" >
                </div>
                <div class="form-group">
                    <label >9.Role in the company [not mandatory]: <x-flag tableName="identities" colName="role_in_the_company" rowId="{{ $identity->id }}" /></label>
                    <input type="text" name="role_in_the_company" class="form-control" value="{{ $identity->role_in_the_company }}" >
                </div>
                <div class="form-group">
                    <label >10.Linkedin [not mandatory]: <x-flag tableName="identities" colName="linkedin" rowId="{{ $identity->id }}" /></label>
                    <input type="text" name="linkedin" class="form-control" value="{{ $identity->linkedin }}" >
                </div>
                <div class="form-group">
                    <label >11.Previous year annual salary in CHF: <x-flag tableName="identities" colName="previous_year_annual_salary_in_chf" rowId="{{ $identity->id }}" /></label>
                    <input type="text" name="previous_year_annual_salary_in_chf" class="form-control" value="{{ $identity->previous_year_annual_salary_in_chf }}" >
                </div>
                <div class="form-group">
                    <label >12.Nationality: <x-flag tableName="identities" colName="nationality" rowId="{{ $identity->id }}" /></label>
                    <input type="text" name="nationality" class="form-control" value="{{ $identity->nationality }}" >
                </div>
                <div class="form-group">
                    <label >13.Other Nationalities (add them all with a “,” in between) <x-flag tableName="identities" colName="other_nationalities" rowId="{{ $identity->id }}" /></label>
                    <input type="text" name="other_nationalities" class="form-control" value="{{ $identity->other_nationalities }}" >
                </div>
                <div class="form-group">
                    <label >14.City of origin <x-flag tableName="identities" colName="city_of_origin" rowId="{{ $identity->id }}" /></label>
                    <input type="text" name="city_of_origin" class="form-control" value="{{ $identity->city_of_origin }}" >
                </div>
                <div class="form-group">
                    <label >15.Municipality of origin <x-flag tableName="identities" colName="municipality_of_origin" rowId="{{ $identity->id }}" /></label>
                    <input type="text" name="municipality_of_origin" class="form-control" value="{{ $identity->municipality_of_origin }}" >
                </div>

                <div class="form-group">
                    <label >16.ID (recto): <x-flag tableName="identities" colName="idRectoFile" rowId="{{ $identity->id }}" /></label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'identities', @$identity->idRectoFile->name) }}">
                        @if(@$identity->idRectoFile->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="id_recto_file_id" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <div class="form-group">
                    <label >17.ID (verso): <x-flag tableName="identities" colName="idVersoFile" rowId="{{ $identity->id }}" /></label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'identities', @$identity->idVersoFile->name) }}">
                        @if(@$identity->idVersoFile->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="id_verso_file_id" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <div class="form-group">
                    <label >18.Passport: <x-flag tableName="identities" colName="passportFile" rowId="{{ $identity->id }}" /></label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'identities', @$identity->passportFile->name) }}">
                        @if(@$identity->passportFile->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="passport_file_id" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <div class="form-group">
                    <label >19.Utility Bill (Must be less than 3 months old): <x-flag tableName="identities" colName="utilityBillFile" rowId="{{ $identity->id }}" /></label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'identities', @$identity->utilityBillFile->name) }}">
                        @if(@$identity->utilityBillFile->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="utility_bill_file_id" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <div class="form-group">
                    <label >20.Criminal record extract [not mandatory](Must be less than 3 months old) <x-flag tableName="identities" colName="criminalRecordExtractFile" rowId="{{ $identity->id }}" /></label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'identities', @$identity->criminalRecordExtractFile->name) }}">
                        @if(@$identity->criminalRecordExtractFile->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="criminal_record_extract_file_id" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>

                <div class="form-group">
                    <label >21.Company’s name or Person’s name and Surname that you represent <x-flag tableName="identities" colName="company_person_represent" rowId="{{ $identity->id }}" /></label>
                    <input type="text" name="company_person_represent" class="form-control" value="{{ $identity->company_person_represent }}" >
                </div>

                <div class="form-group">
                    <label >22.Company’s legal form (Association, Ltd,...) [not mandatory] <x-flag tableName="identities" colName="company_legal_form" rowId="{{ $identity->id }}" /></label>
                    <input type="text" name="company_legal_form" class="form-control" value="{{ $identity->company_legal_form }}" >
                </div>

                <div class="form-group">
                    <label >23.Address (street, number, country) <x-flag tableName="identities" colName="company_address" rowId="{{ $identity->id }}" /></label>
                    <input type="text" name="company_address" class="form-control" value="{{ $identity->company_address }}" >
                </div>

                <div class="form-group">
                    <label >24.ID number of the company <x-flag tableName="identities" colName="company_id_number" rowId="{{ $identity->id }}" /></label>
                    <input type="text" name="company_id_number" class="form-control" value="{{ $identity->company_id_number }}" >
                </div>

                <div class="form-group">
                    <label >25.Commercial Register Extract [not mandatory] <x-flag tableName="identities" colName="company_commercial_register_extract" rowId="{{ $identity->id }}" /></label>
                    <input type="text" name="company_commercial_register_extract" class="form-control" value="{{ $identity->company_commercial_register_extract }}" >
                </div>

                <div class="form-group">
                    <label >26.Proof of existence of the company (e.g. utility bill or Banking documents with the name of the company. Must be less than 3 months old) [not mandatory] <x-flag tableName="identities" colName="companyProofOfExistenceFile" rowId="{{ $identity->id }}" /></label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'identities', @$identity->companyProofOfExistenceFile->name) }}">
                        @if(@$identity->companyProofOfExistenceFile->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="company_proof_of_existence_file_id" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>

                <div class="form-group">
                    <label >27.Signed Procuration / Power of attorney (Important: Signed, date, Add the specific powers like opening the bank account in the , etc. ) <x-flag tableName="identities" colName="companySignedProcurationFile" rowId="{{ $identity->id }}" /></label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'identities', @$identity->companySignedProcurationFile->name) }}">
                        @if(@$identity->companySignedProcurationFile->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="company_signed_procuration_file_id" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>


                <div class="form-group">
                    <label >28.Will you act on behalf of the
                        company/association/else? (e.g. by representing
                        other founder during the creation, opening the bank
                        account for the company, operate the inscription to
                        the commercial registry, etc.) Proof of existence of the company (e.g. utility bill or Banking documents with the name of the company. Must be less than 3 months old) [not mandatory] <x-flag tableName="identities" colName="companyOtherProofOfExistenceFile" rowId="{{ $identity->id }}" /></label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'identities', @$identity->companyOtherProofOfExistenceFile->name) }}">
                        @if(@$identity->companyOtherProofOfExistenceFile->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="company_other_proof_of_existence_file_id" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>

                <div class="form-group">
                    <label >29.Will you act on behalf of the
                        company/association/else? (e.g. by representing
                        other founder during the creation, opening the bank
                        account for the company, operate the inscription to
                        the commercial registry, etc.) Signed Procuration / Power of attorney (Important: Signed, date, Add the specific powers like opening the bank account in the , etc. ) <x-flag tableName="identities" colName="companyOtherSignedProcurationFile" rowId="{{ $identity->id }}" /></label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'identities', @$identity->companyOtherSignedProcurationFile->name) }}">
                        @if(@$identity->companyOtherSignedProcurationFile->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="company_other_signed_procuration_file_id" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>

                <div class="form-group">
                    <label >30.Do you have any comments or additional information to provide for this section? <x-flag tableName="identities" colName="additional_comments" rowId="{{ $identity->id }}" /></label>
                    <input  type="text" name="additional_comments" class="form-control" value="{{ $identity->additional_comments }}" >
                </div>

                <div class="form-group">
                    <label >31.Add additional files in this section <x-flag tableName="identities" colName="additional1File" rowId="{{ $identity->id }}" /></label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'identities', @$identity->additional1File->name) }}">
                        @if(@$identity->additional1File->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="additional_1_file_id" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>

                <div class="form-group">
                    <label >32.Add additional files in this section <x-flag tableName="identities" colName="additional2File" rowId="{{ $identity->id }}" /></label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'identities', @$identity->additional2File->name) }}">
                        @if(@$identity->additional2File->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="additional_2_file_id" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>

                <div class="form-group">
                    <label >33.Add additional files in this section <x-flag tableName="identities" colName="additional3File" rowId="{{ $identity->id }}" /></label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'identities', @$identity->additional3File->name) }}">
                        @if(@$identity->additional3File->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="additional_3_file_id" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>

                <button type="submit" class="btn btn-primary">Save!</button>
            </form>
        </div>
    </div>
@endsection