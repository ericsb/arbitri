@extends('master.master')

@section('content')
    <div class="container">
        <h2>Identities - Add Name</h2>
        <div class="mt-2"></div>
        <div class="">
            <form action="{{ route('identities.add-name.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('post')
                <div class="form-group">
                    <label for="name">1.Name (official):</label>
                    <input type="text" name="name" class="form-control" placeholder="" id="name">
                </div>
                <div class="form-group">
                    <label for="name">2.Surname (official):</label>
                    <input type="text" name="surname" class="form-control" placeholder="" id="surname">
                </div>
                <div class="form-group">
                    <label for="name">3.Birth date (DD/MM/YYYY) :</label>
                    <input type="text" name="birth_date" class="form-control" placeholder="" id="surname">
                </div>
                <div class="form-group">
                    <label for="name">4.Address of domicile:</label>
                    <input type="text" name="address_of_domicile" class="form-control" placeholder="" id="surname">
                </div>
                <div class="form-group">
                    <label for="name">5.Phone number:</label>
                    <input type="text" name="phone_number" class="form-control" placeholder="" id="surname">
                </div>
                <div class="form-group">
                    <label for="name">6.Email:</label>
                    <input type="text" name="email" class="form-control" placeholder="" id="surname">
                </div>
                <div class="form-group">
                    <label for="name">7.Profession:</label>
                    <input type="text" name="profession" class="form-control" placeholder="" id="surname">
                </div>
                <div class="form-group">
                    <label for="name">8.Company’s name [not mandatory]:</label>
                    <input type="text" name="company_name" class="form-control" placeholder="" id="surname">
                </div>
                <div class="form-group">
                    <label for="name">9.Role in the company [not mandatory]:</label>
                    <input type="text" name="role_in_the_company" class="form-control" placeholder="" id="surname">
                </div>
                <div class="form-group">
                    <label for="name">10.Linkedin [not mandatory]:</label>
                    <input type="text" name="linkedin" class="form-control" placeholder="" id="surname">
                </div>
                <div class="form-group">
                    <label for="name">11.Previous year annual salary in CHF:</label>
                    <input type="text" name="previous_year_annual_salary_in_chf" class="form-control" placeholder="" id="surname">
                </div>
                <div class="form-group">
                    <label for="name">12.Nationality:</label>
                    <input type="text" name="nationality" class="form-control" placeholder="" id="surname">
                </div>
                <div class="form-group">
                    <label for="name">13.Other Nationalities (add them all with a “,” in between)</label>
                    <input type="text" name="other_nationalities" class="form-control" placeholder="" id="surname">
                </div>
                <div class="form-group">
                    <label for="name">14.City of origin</label>
                    <input type="text" name="city_of_origin" class="form-control" placeholder="" id="surname">
                </div>
                <div class="form-group">
                    <label for="name">15.Municipality of origin</label>
                    <input type="text" name="municipality_of_origin" class="form-control" placeholder="" id="surname">
                </div>

                <div class="form-group">
                    <label for="name">16.ID (recto):</label>
                    <div class="custom-file">
                        <input name="id_recto_file_id" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name">17.ID (verso):</label>
                    <div class="custom-file">
                        <input name="id_verso_file_id" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name">18.Passport:</label>
                    <div class="custom-file">
                        <input name="passport_file_id" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name">19.Utility Bill (Must be less than 3 months old):</label>
                    <div class="custom-file">
                        <input name="utility_bill_file_id" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name">20.Criminal record extract [not mandatory](Must be less than 3 months old)</label>
                    <div class="custom-file">
                        <input name="criminal_record_extract_file_id" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>

                <div class="form-group">
                    <label for="name">21.Company’s name or Person’s name and Surname that you represent</label>
                    <input type="text" name="company_person_represent" class="form-control" placeholder="" id="surname">
                </div>

                <div class="form-group">
                    <label for="name">22.Company’s legal form (Association, Ltd,...) [not mandatory]</label>
                    <input type="text" name="company_legal_form" class="form-control" placeholder="" id="surname">
                </div>

                <div class="form-group">
                    <label for="name">23.Address (street, number, country)</label>
                    <input type="text" name="company_address" class="form-control" placeholder="" id="surname">
                </div>

                <div class="form-group">
                    <label for="name">24.ID number of the company</label>
                    <input type="text" name="company_id_number" class="form-control" placeholder="" id="surname">
                </div>

                <div class="form-group">
                    <label for="name">25.Commercial Register Extract [not mandatory]</label>
                    <input type="text" name="company_commercial_register_extract" class="form-control" placeholder="" id="surname">
                </div>

                <div class="form-group">
                    <label for="name">26.Proof of existence of the company (e.g. utility bill or Banking documents with the name of the company. Must be less than 3 months old) [not mandatory]</label>
                    <div class="custom-file">
                        <input name="company_proof_of_existence_file_id" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>

                <div class="form-group">
                    <label for="name">27.Signed Procuration / Power of attorney (Important: Signed, date, Add the specific powers like opening the bank account in the , etc. )</label>
                    <div class="custom-file">
                        <input name="company_signed_procuration_file_id" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>


                <div class="form-group">
                    <label for="name">28.Will you act on behalf of the
                        company/association/else? (e.g. by representing
                        other founder during the creation, opening the bank
                        account for the company, operate the inscription to
                        the commercial registry, etc.) Proof of existence of the company (e.g. utility bill or Banking documents with the name of the company. Must be less than 3 months old) [not mandatory]</label>
                    <div class="custom-file">
                        <input name="company_other_proof_of_existence_file_id" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>

                <div class="form-group">
                    <label for="name">29.Will you act on behalf of the
                        company/association/else? (e.g. by representing
                        other founder during the creation, opening the bank
                        account for the company, operate the inscription to
                        the commercial registry, etc.) Signed Procuration / Power of attorney (Important: Signed, date, Add the specific powers like opening the bank account in the , etc. )</label>
                    <div class="custom-file">
                        <input name="company_other_signed_procuration_file_id" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>

                <div class="form-group">
                    <label for="name">30.Do you have any comments or additional information to provide for this section?</label>
                    <input  type="text" name="additional_comments" class="form-control" placeholder="" id="surname">
                </div>

                <div class="form-group">
                    <label for="name">31.Add additional files in this section</label>
                    <div class="custom-file">
                        <input name="additional_1_file_id" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>

                <div class="form-group">
                    <label for="name">32.Add additional files in this section</label>
                    <div class="custom-file">
                        <input name="additional_2_file_id" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>

                <div class="form-group">
                    <label for="name">33.Add additional files in this section</label>
                    <div class="custom-file">
                        <input name="additional_3_file_id" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>

                <button type="submit" class="btn btn-primary">Save!</button>
            </form>
        </div>
    </div>
@endsection