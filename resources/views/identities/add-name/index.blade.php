@extends('master.master')

@section('content')
    <div class="container">
        <h2>Identities</h2>
        <a href="{{ route('identities.add-name.create') }}">
            <button type="button" class="btn btn-success">Add new individuals</button>
        </a>
        <div class="mt-2"></div>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Surname</th>
                <th>Birth date</th>
                <th>Phone Number</th>
                <th>Email</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($identities as $identity)
                <tr>
                    <td>{{ $identity->id }}</td>
                    <td>{{ $identity->name }}</td>
                    <td>{{ $identity->surname }}</td>
                    <td>{{ $identity->birth_date }}</td>
                    <td>{{ $identity->phone_number }}</td>
                    <td>{{ $identity->email }}</td>
                    <td><a href="{{ route('identities.add-name.edit', $identity->id) }}">Edit</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection