<table>
    <thead>
    <tr>
        <th>Name</th>
        <th>Surname</th>
    </tr>
    </thead>
    <tbody>
    @foreach($foundationFounders as $ff)
        <tr>
            <td>{{ $ff->name }}</td>
            <td>{{ $ff->surname }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
