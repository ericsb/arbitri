<table>
    <thead>
    <tr>
        <th>Do you want to conclude an insurance policy for your company the employees</th>
        <th>If yes one in particular or you are potentially interested in all</th>
        <th>Do you want to conclude a private insurance policy for yourself or help your employees conclude one</th>
        <th>If yes one in particular or you are potentially interested in all</th>
        <th>Do you have specific requirements or needs regarding the conclusion of an insurance policy</th>
        <th>Do you have any comments or additional information to provide for this section</th>
        <th>File 1</th>
        <th>File 2</th>
        <th>File 3</th>
    </tr>
    </thead>
    <tbody>
    @foreach($insurances as $i)
        <tr>
            <td>{{ $i->conclude_insurance_status }}</td>
            <td>{{ $i->conclude_insurance_comment }}</td>
            <td>{{ $i->conclude_private_status }}</td>
            <td>{{ $i->conclude_private_comment }}</td>
            <td>{{ $i->specific_requirements }}</td>
            <td>{{ $i->additional_comments }}</td>
            @if(@$i->file1->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'insurances', @$i->file1->name) }}</td>
            @endif
            @if(@$i->file2->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'insurances', @$i->file2->name) }}</td>
            @endif
            @if(@$i->file3->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'insurances', @$i->file3->name) }}</td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>
