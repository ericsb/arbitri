<table>
    <thead>
    <tr>
        <th>Name</th>
        <th>Surname</th>
        <th>Number of shares</th>
    </tr>
    </thead>
    <tbody>
    @foreach($foundationShareholders as $fs)
        <tr>
            <td>{{ $fs->name }}</td>
            <td>{{ $fs->surname }}</td>
            <td>{{ $fs->number_of_shares }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
