<table>
    <thead>
    <tr>
        <th>Name</th>
        <th>Surname</th>
        <th>Role</th>
        <th>Signature Power</th>
    </tr>
    </thead>
    <tbody>
    @foreach($foundationDirections as $fd)
        <tr>
            <td>{{ $fd->name }}</td>
            <td>{{ $fd->surname }}</td>
            <td>{{ $fd->role }}</td>
            <td>{{ $fd->signature_power }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
