<table>
    <thead>
    <tr>
        <th>Name</th>
        <th>What type of fiat currencies must be supported by the account</th>
        <th>What type of cryptocurrencies must be supported by the account</th>
        <th>What is the yearly average volume of transactions you make</th>
        <th>What is the average amount in custody you expect in CHF</th>
        <th>What is the source of wealth</th>
        <th>Does the source of wealth come from cryptoassets or transactions from crypto exchanges?</th>

        <th>File 1</th>
        <th>File 2</th>
        <th>File 3</th>
    </tr>
    </thead>
    <tbody>
    @foreach($bankingPersonals as $b)
        <tr>
            <td>{{ @$b->identity->name }}</td>
            <td>{{ $b->fiat_currencies }}</td>
            <td>{{ $b->crypto_currencies }}</td>
            <td>{{ $b->yearly_transaction_volume }}</td>
            <td>{{ $b->avg_amount_in_custody }}</td>
            <td>{{ $b->source_of_wealth }}</td>
            <td>{{ $b->wealth_from_crypto_status }}</td>

            @if(@$b->file1->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'banking_personal', @$b->file1->name) }}</td>
            @else
                <td>-</td>
            @endif

            @if(@$b->file2->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'banking_personal', @$b->file2->name) }}</td>
            @else
                <td>-</td>
            @endif

            @if(@$b->file3->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'banking_personal', @$b->file3->name) }}</td>
            @else
                <td>-</td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>
