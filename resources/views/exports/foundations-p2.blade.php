<table>
    <thead>
    <tr>
        <th>Issuance of all shares by the Notary</th>
        <th>Issuance of share certificate</th>
        <th>Detail regarding the Issuance of share certificate</th>
        <th>Does the company plan to acquire another company or an asset in possession of one of the Founders now or in the near future?</th>
        <th>Identity of the auditor</th>
        <th>Website of the auditor</th>
        <th>Email of the contact person</th>
        <th>Does at least one member of the Board is domiciled in Switzerland and has individual signatory power?</th>
        <th>What type of audit is applicable?</th>
        <th>Who will be the auditor?</th>
        <th>Financial year</th>
        <th>Financial year comment</th>
        <th>Closing of the first financial year</th>
        <th>Closing of the first financial year comment</th>
        <th>Voting specificities</th>
        <th>Voting specificities comment</th>
        <th>Is a qualified majority required?</th>
        <th>Do you need a fiduciary contract?</th>
        <th>Do you need a shareholder agreement?</th>
        <th>In the eventuality the entity does not mandatorily?</th>
        <th>Name surname of the person who will request it </th>
        <th>Does the entity require a tax exemption status?</th>
        <th>Does this entity require a tax ruling from the tax Authorities?</th>
        <th>Is this new company part of a group of companies, a financial conglomerate or is it a subsidiary?</th>
        <th>How many people will be employed by this entity?</th>
        <th>Do you have any comments or additional information to provide for this section?</th>

        <td>File 1 Election</td>
        <td>File 2 Doc</td>
        <td>File 3 Doc</td>
        <td>File 4 Doc</td>
        <td>File 5 Certificate</td>
        <td>File 6 Inventory</td>
        <td>File 7 Balance</td>
        <td>File 8 Foundation</td>
        <td>File 9 Takeover</td>
        <td>File 10 Auditor Providing</td>
        <td>File 11 Mandate</td>
        <td>File 12 Excerpt</td>
        <td>File 13 Additional</td>
        <td>File 14 Additional</td>
        <td>File 15 Additional</td>
    </tr>
    </thead>
    <tbody>
    @foreach($foundations as $foundation)
        <tr>
            <td>{{ $foundation->issuance_of_all }}</td>
            <td>{{ $foundation->issuance_of_share }}</td>
            <td>{{ $foundation->detail_regarding }}</td>
            <td>{{ $foundation->plan_to_acquire_another_company }}</td>
            <td>{{ $foundation->another_company_identity_of_swiss_auditor }}</td>
            <td>{{ $foundation->another_company_website_of_auditor }}</td>
            <td>{{ $foundation->another_company_email_of_contact }}</td>
            <td>{{ $foundation->member_signature_power }}</td>
            <td>{{ $foundation->audit_applicable }}</td>
            <td>{{ $foundation->who_will_auditor }}</td>
            <td>{{ $foundation->financial_year }}</td>
            <td>{{ $foundation->financial_year_comment }}</td>
            <td>{{ $foundation->closing_of_the_first_financial_year }}</td>
            <td>{{ $foundation->closing_of_the_first_financial_year_comment }}</td>
            <td>{{ $foundation->minimum_quorum }}</td>
            <td>{{ $foundation->minimum_quorum_comment }}</td>
            <td>{{ $foundation->majority_required }}</td>
            <td>{{ $foundation->fiduciary_contract }}</td>
            <td>{{ $foundation->shareholder_agreement }}</td>
            <td>{{ $foundation->commercial_register }}</td>
            <td>{{ $foundation->commercial_register_person }}</td>
            <td>{{ $foundation->tax_exemption }}</td>
            <td>{{ $foundation->tax_ruling }}</td>
            <td>{{ $foundation->part_of_group }}</td>
            <td>{{ $foundation->people_count }}</td>
            <td>{{ $foundation->additional_comments }}</td>

            @if(@$foundation->electionFile->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'foundations', @$foundation->electionFile->name) }}</td>
            @else
                <td>-</td>
            @endif

            @if(@$foundation->furtherDocumentFile1->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'foundations', @$foundation->furtherDocumentFile1->name) }}</td>
            @else
                <td>-</td>
            @endif

            @if(@$foundation->furtherDocumentFile2->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'foundations', @$foundation->furtherDocumentFile2->name) }}</td>
            @else
                <td>-</td>
            @endif

            @if(@$foundation->furtherDocumentFile3->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'foundations', @$foundation->furtherDocumentFile3->name) }}</td>
            @else
                <td>-</td>
            @endif

            @if(@$foundation->attestationFile->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'foundations', @$foundation->attestationFile->name) }}</td>
            @else
                <td>-</td>
            @endif

            @if(@$foundation->detailedInventoryFile->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'foundations', @$foundation->detailedInventoryFile->name) }}</td>
            @else
                <td>-</td>
            @endif


            @if(@$foundation->balanceSheetFile->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'foundations', @$foundation->balanceSheetFile->name) }}</td>
            @else
                <td>-</td>
            @endif

            @if(@$foundation->anotherFoundationContractFile->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'foundations', @$foundation->anotherFoundationContractFile->name) }}</td>
            @else
                <td>-</td>
            @endif


            @if(@$foundation->anotherTakeoverContractFile->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'foundations', @$foundation->anotherTakeoverContractFile->name) }}</td>
            @else
                <td>-</td>
            @endif


            @if(@$foundation->anotherAuditorFile->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'foundations', @$foundation->anotherAuditorFile->name) }}</td>
            @else
                <td>-</td>
            @endif

            @if(@$foundation->mandateFile->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'foundations', @$foundation->mandateFile->name) }}</td>
            @else
                <td>-</td>
            @endif

            @if(@$foundation->excerptFile->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'foundations', @$foundation->excerptFile->name) }}</td>
            @else
                <td>-</td>
            @endif

            @if(@$foundation->additionalFile1->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'foundations', @$foundation->additionalFile1->name) }}</td>
            @else
                <td>-</td>
            @endif


            @if(@$foundation->additionalFile2->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'foundations', @$foundation->additionalFile2->name) }}</td>
            @else
                <td>-</td>
            @endif

            @if(@$foundation->additionalFile3->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'foundations', @$foundation->additionalFile3->name) }}</td>
            @else
                <td>-</td>
            @endif

        </tr>
    @endforeach
    </tbody>
</table>
