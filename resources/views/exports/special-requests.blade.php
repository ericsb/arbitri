<table>
    <thead>
    <tr>
        <th>Comment</th>
        <th>File 1</th>
        <th>File 2</th>
        <th>File 3</th>
        <th>File 4</th>
    </tr>
    </thead>
    <tbody>
    @foreach($specialRequests as $r)
        <tr>
            <td>{{ $r->description }}</td>
            @if(@$r->file1->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'special_requests', @$r->file1->name) }}</td>
            @endif
            @if(@$r->file2->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'special_requests', @$r->file2->name) }}</td>
            @endif
            @if(@$r->file3->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'special_requests', @$r->file3->name) }}</td>
            @endif
            @if(@$r->file4->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'special_requests', @$r->file4->name) }}</td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>
