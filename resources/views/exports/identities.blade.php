<table>
    <thead>
    <tr>
        <th>Name</th>
        <th>Surname</th>
        <th>Birthdate</th>
        <th>Address of domicile</th>
        <th>Phone</th>
        <th>Email</th>
        <th>Profession</th>
        <th>Company name</th>
        <th>Role in the company</th>
        <th>Linkedin</th>
        <th>Previous year annual salary in CHF</th>
        <th>Nationality</th>
        <th>Other Nationalities</th>
        <th>City of origin</th>
        <th>Municipality of origin</th>
        <th>Company’s name or Person’s name and Surname that you represent</th>
        <th>Company’s legal form</th>
        <th>Address</th>
        <th>ID number of the company</th>
        <th>Commercial Register Extract</th>
        <th>Comments</th>
        <th>File 1 Recto</th>
        <th>File 2 Verso</th>
        <th>File 3 Passport</th>
        <th>File 4 Utility Bill</th>
        <th>File 5 Criminal Record</th>
        <th>File 6 Proof</th>
        <th>File 7 Signed</th>
        <th>File 8 Proof 2</th>
        <th>File 9 Signed 2</th>
        <th>File 10 Additional</th>
        <th>File 11 Additional</th>
        <th>File 12 Additional</th>
    </tr>
    </thead>
    <tbody>
    @foreach($identities as $identity)
        <tr>
            <td>{{ $identity->name }}</td>
            <td>{{ $identity->surname }}</td>
            <td>{{ $identity->birth_date }}</td>
            <td>{{ $identity->address_of_domicile }}</td>
            <td>{{ $identity->phone_number }}</td>
            <td>{{ $identity->email }}</td>
            <td>{{ $identity->profession }}</td>
            <td>{{ $identity->company_name }}</td>
            <td>{{ $identity->role_in_the_company }}</td>
            <td>{{ $identity->linkedin }}</td>
            <td>{{ $identity->previous_year_annual_salary_in_chf }}</td>
            <td>{{ $identity->nationality }}</td>
            <td>{{ $identity->other_nationalities }}</td>
            <td>{{ $identity->city_of_origin }}</td>
            <td>{{ $identity->municipality_of_origin }}</td>
            <td>{{ $identity->company_person_represent }}</td>
            <td>{{ $identity->company_legal_form }}</td>
            <td>{{ $identity->company_address }}</td>
            <td>{{ $identity->company_id_number }}</td>
            <td>{{ $identity->company_commercial_register_extract }}</td>
            <td>{{ $identity->additional_comments }}</td>
            @if(@$identity->idRectoFile->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'identities', @$identity->idRectoFile->name) }}</td>
            @else
                <td>-</td>
            @endif
            @if(@$identity->idVersoFile->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'identities', @$identity->idVersoFile->name) }}</td>
            @else
                <td>-</td>
            @endif
            @if(@$identity->passportFile->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'identities', @$identity->passportFile->name) }}</td>
            @else
                <td>-</td>
            @endif
            @if(@$identity->utilityBillFile->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'identities', @$identity->utilityBillFile->name) }}</td>
            @else
                <td>-</td>
            @endif
            @if(@$identity->criminalRecordExtractFile->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'identities', @$identity->criminalRecordExtractFile->name) }}</td>
            @else
                <td>-</td>
            @endif
            @if(@$identity->companyProofOfExistenceFile->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'identities', @$identity->companyProofOfExistenceFile->name) }}</td>
            @else
                <td>-</td>
            @endif
            @if(@$identity->companySignedProcurationFile->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'identities', @$identity->companySignedProcurationFile->name) }}</td>
            @else
                <td>-</td>
            @endif
            @if(@$identity->companyOtherProofOfExistenceFile->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'identities', @$identity->companyOtherProofOfExistenceFile->name) }}</td>
            @else
                <td>-</td>
            @endif
            @if(@$identity->companyOtherSignedProcurationFile->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'identities', @$identity->companyOtherSignedProcurationFile->name) }}</td>
            @else
                <td>-</td>
            @endif
            @if(@$identity->additional1File->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'identities', @$identity->additional1File->name) }}</td>
            @else
                <td>-</td>
            @endif
            @if(@$identity->additional2File->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'identities', @$identity->additional2File->name) }}</td>
            @else
                <td>-</td>
            @endif
            @if(@$identity->additional3File->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'identities', @$identity->additional3File->name) }}</td>
            @else
                <td>-</td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>
