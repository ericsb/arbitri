<table>
    <thead>
        <tr>
            <th>Form</th>
            <th>Companys form name in a swiss official</th>
            <th>Company Name</th>
            <th>Company alternative Name</th>
            <th>Address of seat</th>
            <th>Political municipality</th>
            <th>The company</th>
            <th>Goals</th>
            <th>Does the goals involve</th>
            <th>Is the capital fully liberated</th>
            <th>How much will be liberated in CHF</th>
            <th>What type of liberation is it</th>
            <th>Bank where the capital in CHF is held in escrow</th>
            <th>Name of the Bank where the capital is held in escrow</th>
            <th>Precise how much will be liberated in cash and what will be the value of the contribution in kind?</th>
            <th>Nature of contribution in kind</th>
            <th>What will be the nature of the contribution in kind?</th>
            <th>Foundation report</th>
            <th>Foundation contract</th>
            <th>Auditor providing the certificate, which aknowledges the value of contribution in kind</th>
            <th>Identity of the swiss auditor</th>
            <th>Website of the auditor</th>
            <th>Email of the contact person</th>
            <th>Number of shares</th>
            <th>Nominal value of per share in CHF</th>
            <th>What is the nature of the share?</th>
            <th>Communication</th>
        </tr>
    </thead>
    <tbody>
    @foreach($foundations as $foundation)
        <tr>
            <td>{{ $foundation->form_type }}</td>
            <td>{{ $foundation->form_name }}</td>
            <td>{{ $foundation->company_name }}</td>
            <td>{{ $foundation->company_alternative_name }}</td>
            <td>{{ $foundation->address_of_seat }}</td>
            <td>{{ $foundation->political_municipality }}</td>
            <td>{{ $foundation->company_office_type }}</td>
            <td>{{ $foundation->goals }}</td>
            <td>{{ $foundation->goals_involve }}</td>
            <td>{{ $foundation->capital_liberated }}</td>
            <td>{{ $foundation->liberated_in_chf }}</td>
            <td>{{ $foundation->type_of_liberation }}</td>
            <td>{{ $foundation->bank_location_in_escrow }}</td>
            <td>{{ $foundation->bank_name_in_escrow }}</td>
            <td>{{ $foundation->liberated_in_cash }}</td>
            <td>{{ $foundation->nature_of_contribution }}</td>
            <td>{{ $foundation->will_nature_of_contribution }}</td>
            <td>{{ $foundation->foundation_report }}</td>
            <td>{{ $foundation->foundation_contract }}</td>
            <td>{{ $foundation->auditor_providing }}</td>
            <td>{{ $foundation->identity_of_swiss_auditor }}</td>
            <td>{{ $foundation->website_of_auditor }}</td>
            <td>{{ $foundation->email_of_contact }}</td>
            <td>{{ $foundation->number_of_shares }}</td>
            <td>{{ $foundation->nominal_value }}</td>
            <td>{{ $foundation->nature_of_the_share }}</td>
            <td>{{ $foundation->communication }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
