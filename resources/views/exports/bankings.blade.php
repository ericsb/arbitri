<table>
    <thead>
    <tr>
        <th>Personal</th>
        <th>Capital</th>
        <th>Company</th>
    </tr>
    </thead>
    <tbody>
    @foreach($bankings as $b)
        <tr>
            <td>{{ $b->personal_bank_account_status }}</td>
            <td>{{ $b->capital_bank_account_status }}</td>
            <td>{{ $b->company_bank_account_status }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
