<table>
    <thead>
    <tr>
        <th>Describe briefly the economic activities of the entity</th>
        <th>What is the yearly average volume of transaction expected</th>
        <th>How many book entries</th>
        <th>List the countries or regions</th>
        <th>List all fiat currencies you hold</th>
        <th>List all cryptocurrencies you accept </th>
        <th>How many people will be employed</th>
        <th>For what type of activities do you need an auditor</th>
        <th>Do you have any comments or additional information to provide for this section</th>
        <th>File 1 Business Plan</th>
        <th>File 2 Internal Chart</th>
        <th>File 3 Overall Chart</th>
        <th>File 4 Additional File</th>
        <th>File 5 Additional File</th>
        <th>File 6 Additional File</th>
    </tr>
    </thead>
    <tbody>
    @foreach($auditors as $a)
        <tr>
            <td>{{ $a->economic_activities }}</td>
            <td>{{ $a->avg_volume_transaction }}</td>
            <td>{{ $a->book_entries }}</td>
            <td>{{ $a->countries_or_regions }}</td>
            <td>{{ $a->fiat_currencies }}</td>
            <td>{{ $a->crypto_currencies }}</td>
            <td>{{ $a->employ_count }}</td>
            <td>{{ $a->regulated_activity_status }}</td>
            <td>{{ $a->comments }}</td>


            @if(@$a->businessPlanFile->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'auditors', @$a->businessPlanFile->name) }}</td>
            @else
                <td>-</td>
            @endif

            @if(@$a->internalChartFile->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'auditors', @$a->internalChartFile->name) }}</td>
            @else
                <td>-</td>
            @endif

            @if(@$a->overallChartFile->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'auditors', @$a->overallChartFile->name) }}</td>
            @else
                <td>-</td>
            @endif

            @if(@$a->additionalFile1->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'auditors', @$a->additionalFile1->name) }}</td>
            @else
                <td>-</td>
            @endif

            @if(@$a->additionalFile2->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'auditors', @$a->additionalFile2->name) }}</td>
            @else
                <td>-</td>
            @endif

            @if(@$a->additionalFile3->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'auditors', @$a->additionalFile3->name) }}</td>
            @else
                <td>-</td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>
