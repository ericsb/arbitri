<table>
    <thead>
    <tr>
        <th>Do you want to hire employees in Switzerland</th>
        <th>Do you want to hire a Swiss Director</th>
        <th>Do you need help regarding immigration law</th>
        <th>Do you need to find a personal domicile for Directors or Employees</th>
        <th>Do you need an introduction into the Swiss blockchain or financial Communities</th>
        <th>Would you be interested in setting up a recovery mechanism procedure for your private keys</th>
        <th>Do you have any comments</th>
        <th>File 1</th>
        <th>File 2</th>
        <th>File 3</th>
   </tr>
    </thead>
    <tbody>
    @foreach($employments as $e)
        <tr>
            <td>{{ $e->hire_employees_status }}</td>
            <td>{{ $e->hire_director_status }}</td>
            <td>{{ $e->immigration_law_status }}</td>
            <td>{{ $e->find_personal_status }}</td>
            <td>{{ $e->introduction_blockchain_status }}</td>
            <td>{{ $e->recover_private_key_status }}</td>
            <td>{{ $e->comments }}</td>

            @if(@$e->file1->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'employments', @$e->file1->name) }}</td>
            @else
                <td>-</td>
            @endif

            @if(@$e->file2->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'employments', @$e->file2->name) }}</td>
            @else
                <td>-</td>
            @endif

            @if(@$e->file3->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'employments', @$e->file3->name) }}</td>
            @else
                <td>-</td>
            @endif

        </tr>
    @endforeach
    </tbody>
</table>
