<table>
    <thead>
    <tr>
        <th>Name</th>
        <th>What type of fiat currencies must be supported by the account</th>
        <th>What type of cryptocurrencies must be supported by the account</th>
        <th>What is the yearly average volume of transactions you make</th>
        <th>What is the average amount in custody you expect in CHF</th>
        <th>What is the source of wealth</th>
        <th>Does the source of wealth come from cryptoassets or transactions from crypto exchanges?</th>
        <th>Any comments</th>

        <th>File 1 Transaction</th>
        <th>File 2 Transaction</th>
        <th>File 3 Transaction</th>
        <th>File 4 Excerpt</th>
        <th>File 5 Memo</th>
        <th>File 6 Memo</th>
        <th>File 7 Annual Report</th>
        <th>File 8 Preliminary</th>
        <th>File 9 Organization File</th>
        <th>File 10 Organization File</th>
        <th>File 11 Shareholder</th>
        <th>File 12 Additional File</th>
        <th>File 13 Additional File</th>
        <th>File 14 Additional File</th>
    </tr>
    </thead>
    <tbody>
    @foreach($bankingCompanies as $b)
        <tr>
            <td>{{ @$b->identity->name }}</td>
            <td>{{ $b->fiat_currencies }}</td>
            <td>{{ $b->crypto_currencies }}</td>
            <td>{{ $b->yearly_transaction_volume }}</td>
            <td>{{ $b->avg_amount_in_custody }}</td>
            <td>{{ $b->source_of_wealth }}</td>
            <td>{{ $b->wealth_from_crypto_status }}</td>
            <td>{{ $b->additional_comment }}</td>

            @if(@$b->transactionFile1->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'banking_company', @$b->transactionFile1->name) }}</td>
            @else
                <td>-</td>
            @endif

            @if(@$b->transactionFile2->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'banking_company', @$b->transactionFile2->name) }}</td>
            @else
                <td>-</td>
            @endif

            @if(@$b->transactionFile3->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'banking_company', @$b->transactionFile3->name) }}</td>
            @else
                <td>-</td>
            @endif

            @if(@$b->excerptFile->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'banking_company', @$b->excerptFile->name) }}</td>
            @else
                <td>-</td>
            @endif

            @if(@$b->memoFile1->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'banking_company', @$b->memoFile1->name) }}</td>
            @else
                <td>-</td>
            @endif

            @if(@$b->memoFile2->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'banking_company', @$b->memoFile2->name) }}</td>
            @else
                <td>-</td>
            @endif

            @if(@$b->annualReportFile->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'banking_company', @$b->annualReportFile->name) }}</td>
            @else
                <td>-</td>
            @endif

            @if(@$b->preliminaryFile->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'banking_company', @$b->preliminaryFile->name) }}</td>
            @else
                <td>-</td>
            @endif

            @if(@$b->organizationFile1->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'banking_company', @$b->organizationFile1->name) }}</td>
            @else
                <td>-</td>
            @endif

            @if(@$b->organizationFile2->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'banking_company', @$b->organizationFile2->name) }}</td>
            @else
                <td>-</td>
            @endif

            @if(@$b->shareholderFile->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'banking_company', @$b->shareholderFile->name) }}</td>
            @else
                <td>-</td>
            @endif

            @if(@$b->additionalFile1->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'banking_company', @$b->additionalFile1->name) }}</td>
            @else
                <td>-</td>
            @endif

            @if(@$b->additionalFile2->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'banking_company', @$b->additionalFile2->name) }}</td>
            @else
                <td>-</td>
            @endif

            @if(@$b->additionalFile3->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'banking_company', @$b->additionalFile3->name) }}</td>
            @else
                <td>-</td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>
