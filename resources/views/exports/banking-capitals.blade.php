<table>
    <thead>
    <tr>
        <th>Name</th>
        <th>Deposit</th>
        <th>Source of wealth</th>
        <th>Wealth from crypto</th>
        <th>Employ count</th>
        <th>File 1</th>
        <th>File 2</th>
        <th>File 3</th>
    </tr>
    </thead>
    <tbody>
    @foreach($bankingCapitals as $b)
        <tr>
            <td>{{ @$b->identity->name }}</td>
            <td>{{ $b->deposit }}</td>
            <td>{{ $b->source_of_wealth }}</td>
            <td>{{ $b->wealth_from_crypto_status }}</td>
            <td>{{ $b->employ_count }}</td>

            @if(@$b->file1->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'banking_capital', @$b->file1->name) }}</td>
            @else
                <td>-</td>
            @endif

            @if(@$b->file2->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'banking_capital', @$b->file2->name) }}</td>
            @else
                <td>-</td>
            @endif

            @if(@$b->file3->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'banking_capital', @$b->file3->name) }}</td>
            @else
                <td>-</td>
            @endif

        </tr>
    @endforeach
    </tbody>
</table>
