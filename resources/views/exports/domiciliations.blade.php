<table>
    <thead>
    <tr>
        <th>Do you want us to find a third-party</th>
        <th>Do you have any specific requirements</th>
        <th>Are you looking for renting or buying</th>
        <th>Budget per month with all charges included</th>
        <th>Purchasing budget all fees included</th>
        <th>Do you need a residence or a domiciliation?</th>
        <th>In what Country</th>
        <th>In what Canton or Region?</th>
        <th>In or nearby what City?</th>
        <th>Do you have a preferred neighborhood</th>
        <th>Would you like to be near specific infrastructure</th>
        <th>In the eventuality you need multiple offices, select “yes”, and discuss it directly with your counsel.</th>
        <th>What is your preferred style</th>
        <th>How many m2 do you want</th>
        <th>How many people will be occupying the offices</th>
        <th>Do you need any specific infrastructure in the nearby building</th>
        <th>Do you need any specific infrastructure in the building ?</th>
        <th>Do you have any specific requirements for the offices?</th>
        <th>Any special requests</th>
        <th>Do you have any comments</th>

        <th>File 1</th>
        <th>File 2</th>
        <th>File 3</th>
    </tr>
    </thead>
    <tbody>
    @foreach($domiciliations as $d)
        <tr>
            <td>{{ $d->third_party_status }}</td>
            <td>{{ $d->third_party_requirements }}</td>
            <td>{{ $d->looking_renting_status }}</td>
            <td>{{ $d->budget_per_month }}</td>
            <td>{{ $d->purchasing_budget }}</td>
            <td>{{ $d->need_residence_status }}</td>
            <td>{{ $d->country }}</td>
            <td>{{ $d->canton }}</td>
            <td>{{ $d->city }}</td>
            <td>{{ $d->neighborhood }}</td>
            <td>{{ $d->infrastructure }}</td>
            <td>{{ $d->multiple_office_status }}</td>
            <td>{{ $d->preferred_style }}</td>
            <td>{{ $d->m2_count }}</td>
            <td>{{ $d->people_count }}</td>
            <td>{{ $d->infrastructure_near_by }}</td>
            <td>{{ $d->infrastructure_in }}</td>
            <td>{{ $d->specific_requirements }}</td>
            <td>{{ $d->comments }}</td>
            <td>{{ $d->multiple_office_status }}</td>

            @if(@$d->file1->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'domiciliations', @$d->file1->name) }}</td>
            @else
                <td>-</td>
            @endif

            @if(@$d->file2->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'domiciliations', @$d->file2->name) }}</td>
            @else
                <td>-</td>
            @endif

            @if(@$d->file3->name)
                <td>{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'domiciliations', @$d->file3->name) }}</td>
            @else
                <td>-</td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>
