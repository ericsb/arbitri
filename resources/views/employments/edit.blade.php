@extends('master.master')

@section('content')
    <div class="container">
        <h2>Special requests</h2>
        <hr>
        <div class="mt-2"></div>
        <div class="">
            <form action="{{ route('employments.update') }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('post')
                <p>1.Do you want to hire employees in Switzerland? <x-flag tableName="employments" colName="hire_employees_status" rowId="{{ $employment->id }}" /></p>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="hire_employees_status" id="exampleRadios1" value="Yes" @if($employment->hire_employees_status == "Yes") checked @endif>
                    <label class="form-check-label" >
                        Yes
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="hire_employees_status" id="exampleRadios2" value="No"  @if($employment->hire_employees_status == "No") checked @endif>
                    <label class="form-check-label" >
                        No
                    </label>
                </div>
                <hr>
                <p>2.Do you want to hire a Swiss Director / Administrator?
                    (Mandatory when creating a corporation like SA or Sàrl) <x-flag tableName="employments" colName="hire_director_status" rowId="{{ $employment->id }}" /></p>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="hire_director_status" id="exampleRadios1" value="Yes" @if($employment->hire_director_status == "Yes") checked @endif>
                    <label class="form-check-label" >
                        Yes
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="hire_director_status" id="exampleRadios2" value="No"  @if($employment->hire_director_status == "No") checked @endif>
                    <label class="form-check-label" >
                        No
                    </label>
                </div>
                <hr>
                <p>3.Do you need help regarding immigration law? <x-flag tableName="employments" colName="immigration_law_status" rowId="{{ $employment->id }}" /></p>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="immigration_law_status" id="exampleRadios1" value="Yes" @if($employment->immigration_law_status == "Yes") checked @endif>
                    <label class="form-check-label" >
                        Yes
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="immigration_law_status" id="exampleRadios2" value="No"  @if($employment->immigration_law_status == "No") checked @endif>
                    <label class="form-check-label" >
                        No
                    </label>
                </div>
                <hr>
                <p>4.Do you need to find a personal domicile for Directors or
                    Employees? (N.b. this section deals with personal
                    domicile and not company’s seat as mentioned above) <x-flag tableName="employments" colName="find_personal_status" rowId="{{ $employment->id }}" />
                </p>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="find_personal_status" id="exampleRadios1" value="Yes" @if($employment->find_personal_status == "Yes") checked @endif>
                    <label class="form-check-label" >
                        Yes
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="find_personal_status" id="exampleRadios2" value="No"  @if($employment->find_personal_status == "No") checked @endif>
                    <label class="form-check-label" >
                        No
                    </label>
                </div>
                <hr>
                <p>5.Do you need an introduction into the Swiss blockchain
                    or financial Communities? (E.g. Association’s
                    membership etc.) <x-flag tableName="employments" colName="introduction_blockchain_status" rowId="{{ $employment->id }}" />
                </p>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="introduction_blockchain_status" id="exampleRadios1" value="Yes" @if($employment->find_personal_status == "Yes") checked @endif>
                    <label class="form-check-label" >
                        Yes
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="introduction_blockchain_status" id="exampleRadios2" value="No"  @if($employment->find_personal_status == "No") checked @endif>
                    <label class="form-check-label" >
                        No
                    </label>
                </div>
                <hr>
                <p>6.Would you be interested in setting up a recovery
                    mechanism procedure for your private keys? (E.g. Crypto
                    Vault, notary procedure, etc.) <x-flag tableName="employments" colName="recover_private_key_status" rowId="{{ $employment->id }}" />
                </p>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="recover_private_key_status" id="exampleRadios1" value="Yes" @if($employment->find_personal_status == "Yes") checked @endif>
                    <label class="form-check-label" >
                        Yes
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="recover_private_key_status" id="exampleRadios2" value="No"  @if($employment->find_personal_status == "No") checked @endif>
                    <label class="form-check-label" >
                        No
                    </label>
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">7.Do you have any comments or additional information to
                        provide for this section? <x-flag tableName="employments" colName="comments" rowId="{{ $employment->id }}" />
                    </label>
                    <input type="text" name="comments" class="form-control" value="{{ $employment->comments ?? '' }}" id="description">
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">8.Additional files (1): <x-flag tableName="employments" colName="file1" rowId="{{ $employment->id }}" /></label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'employments', @$employment->file1->name) }}">
                        @if(@$employment->file1->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="file1" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">9.Additional files (2): <x-flag tableName="employments" colName="file2" rowId="{{ $employment->id }}" /></label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'employments', @$employment->file2->name) }}">
                        @if(@$employment->file2->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="file2" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">10.Additional files (3): <x-flag tableName="employments" colName="file3" rowId="{{ $employment->id }}" /></label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'employments', @$employment->file3->name) }}">
                        @if(@$employment->file3->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="file3" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>
@endsection