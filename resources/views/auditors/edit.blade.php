@extends('master.master')

@section('content')
    <div class="container">
        <h2>Auditor & Accountant</h2>
        <hr>
        <div class="mt-2"></div>
        <div class="">
            <form action="{{ route('auditor.update') }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('post')
                <div class="form-group">
                    <label for="name">1.Describe briefly the economic activities of the entity
                        (Typical Credit & Debit operations happening) <x-flag tableName="auditors" colName="economic_activities" rowId="{{ $auditor->id }}" />
                    </label>
                    <input type="text" name="economic_activities" class="form-control" value="{{ $auditor->economic_activities ?? '' }}" id="description">
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">2.Attach your business plan if any: <x-flag tableName="auditors" colName="businessPlanFile" rowId="{{ $auditor->id }}" /></label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'auditors', @$auditor->businessPlanFile->name) }}">
                        @if(@$auditor->businessPlanFile->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="business_plan_file" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">3.Internal Organisational chart of the entity if any (e.g.
                        including department, financial .): <x-flag tableName="auditors" colName="internalChartFile" rowId="{{ $auditor->id }}" /></label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'auditors', @$auditor->internalChartFile->name) }}">
                        @if(@$auditor->internalChartFile->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="internal_chart_file" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">4.Overall Organisational chart of the entity if any (e.g.
                        including mother company, subsidiaries, etc.): <x-flag tableName="auditors" colName="overallChartFile" rowId="{{ $auditor->id }}" /></label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'auditors', @$auditor->overallChartFile->name) }}">
                        @if(@$auditor->overallChartFile->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="overall_chart_file" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">5.What is the yearly average volume of transaction
                        expected (estimation in CHF)? <x-flag tableName="auditors" colName="avg_volume_transaction" rowId="{{ $auditor->id }}" />
                    </label>
                    <input type="text" name="avg_volume_transaction" class="form-control" value="{{ $auditor->avg_volume_transaction ?? '' }}" id="description">
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">6.How many book entries (transactions) does it
                        represent per year? <x-flag tableName="auditors" colName="book_entries" rowId="{{ $auditor->id }}" />
                    </label>
                    <input type="text" name="book_entries" class="form-control" value="{{ $auditor->book_entries ?? '' }}" id="description">
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">7.List the countries or regions and the estimated
                        proportion from where the benefits come from (e.g.
                        20% in CH / 50% in EU / 30% in the US). <x-flag tableName="auditors" colName="countries_or_regions" rowId="{{ $auditor->id }}" />
                    </label>
                    <input type="text" name="countries_or_regions" class="form-control" value="{{ $auditor->countries_or_regions ?? '' }}" id="description">
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">8.List all fiat currencies you hold in your bank
                        accounts. <x-flag tableName="auditors" colName="fiat_currencies" rowId="{{ $auditor->id }}" />
                    </label>
                    <input type="text" name="fiat_currencies" class="form-control" value="{{ $auditor->fiat_currencies ?? '' }}" id="description">
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">9.List all cryptocurrencies you accept as payment and
                        hold in your accounts or public addresses. <x-flag tableName="auditors" colName="crypto_currencies" rowId="{{ $auditor->id }}" />
                    </label>
                    <input type="text" name="crypto_currencies" class="form-control" value="{{ $auditor->crypto_currencies ?? '' }}" id="description">
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">10.How many people will be employed by this entity? <x-flag tableName="auditors" colName="employ_count" rowId="{{ $auditor->id }}" /></label>
                    <input type="text" name="employ_count" class="form-control" value="{{ $auditor->employ_count ?? '' }}" id="description">
                </div>
                <hr>
                <p>11.For what type of activities do you need an auditor? <x-flag tableName="auditors" colName="regulated_activity_status" rowId="{{ $auditor->id }}" />
                </p>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="regulated_activity_status" id="exampleRadios1" value="Yes" @if($auditor->regulated_activity_status == "Yes") checked @endif>
                    <label class="form-check-label" >
                        In the context of a regulated activity (e.g AML authorisation)
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="certification_status" id="exampleRadios2" value="Yes"  @if($auditor->certification_status == "Yes") checked @endif>
                    <label class="form-check-label" >
                        In the context of a certification regarding a the value of a contribution in-kind or a
                        takeover (e.g durin the constitution of the company)
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="ordinary_status" id="exampleRadios2" value="Yes"  @if($auditor->ordinary_status == "Yes") checked @endif>
                    <label class="form-check-label" >
                        In the context of a limited or an ordinary audit (as requested by law, or in case of
                        voluntary opt-in)
                    </label>
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">12.Do you have any comments or additional information to
                        provide for this section? <x-flag tableName="auditors" colName="comments" rowId="{{ $auditor->id }}" /></label>
                    <input type="text" name="comments" class="form-control" value="{{ $auditor->comments ?? '' }}" id="description">
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">13.Add additional files in this section (1): <x-flag tableName="auditors" colName="additionalFile1" rowId="{{ $auditor->id }}" /></label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'auditors', @$auditor->additionalFile1->name) }}">
                        @if(@$auditor->additionalFile1->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="additional_file_1" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name">14.Add additional files in this section (2): <x-flag tableName="auditors" colName="additionalFile2" rowId="{{ $auditor->id }}" /></label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'auditors', @$auditor->additionalFile2->name) }}">
                        @if(@$auditor->additionalFile2->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="additional_file_2" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name">15.Add additional files in this section (3): <x-flag tableName="auditors" colName="additionalFile3" rowId="{{ $auditor->id }}" /></label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'auditors', @$auditor->additionalFile3->name) }}">
                        @if(@$auditor->additionalFile3->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="additional_file_3" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>

                <button type="submit" class="btn btn-primary">Save!</button>
            </form>
        </div>
    </div>
@endsection