@extends('master.master')

@section('content')
    <div class="container">
        <h2>Banking & financial services</h2>
        <hr>
        <div class="mt-2"></div>
        <div class="">
            <form action="{{ route('bankings.update') }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('post')
                <p>1.What are the types of bank account(s) do you need?</p>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="personal_bank_account_status" id="exampleRadios1" value="Yes" @if($banking->personal_bank_account_status == "Yes") checked @endif>
                    <label class="form-check-label" >
                        Personal bank account(s) (N.b. A personal bank account in your name)
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="capital_bank_account_status" id="exampleRadios2" value="Yes" @if($banking->capital_bank_account_status == "Yes") checked @endif >
                    <label class="form-check-label" >
                        A capital deposit bank account for the creation of a company (N.b. This
                        bank account is necessary during the incorporation phase for corporation
                        like SA or Sàrl)
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="company_bank_account_status" id="exampleRadios2" value="Yes" @if($banking->company_bank_account_status == "Yes") checked @endif>
                    <label class="form-check-label" >
                        Company’s commercial bank account(s) (N.b. bank account(s) for the
                        company’s operation and day-to-day business)
                    </label>
                </div>
                <br>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
        <hr>
        @if($banking->personal_bank_account_status == "Yes")
            <h3>Personal bank account <x-flag tableName="bankings" colName="personal_bank_account_status" rowId="{{ $banking->id }}" /></h3>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Unique ID</th>
                    <th>Name</th>
                    <th>Surname</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($identities as $identity)
                    <tr>
                        <td>ABT{{ $identity->id }}</td>
                        <td>{{ $identity->name }}</td>
                        <td>{{ $identity->surname }}</td>
                        <td><a href="{{ route('bankings.personal.edit', $identity->id) }}">Submit</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <hr>
        @endif
        @if($banking->capital_bank_account_status == "Yes")
            <h3>Capital deposit bank account <x-flag tableName="bankings" colName="capital_bank_account_status" rowId="{{ $banking->id }}" />
            </h3>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Unique ID</th>
                    <th>Name</th>
                    <th>Surname</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($identities as $identity)
                    <tr>
                        <td>ABT{{ $identity->id }}</td>
                        <td>{{ $identity->name }}</td>
                        <td>{{ $identity->surname }}</td>
                        <td><a href="{{ route('bankings.capital.edit', $identity->id) }}">Submit</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <hr>
        @endif
        @if($banking->company_bank_account_status == "Yes")
            <h3>Company’s bank account <x-flag tableName="bankings" colName="company_bank_account_status" rowId="{{ $banking->id }}" /></h3>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Unique ID</th>
                    <th>Name</th>
                    <th>Surname</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($identities as $identity)
                    <tr>
                        <td>ABT{{ $identity->id }}</td>
                        <td>{{ $identity->name }}</td>
                        <td>{{ $identity->surname }}</td>
                        <td><a href="{{ route('bankings.company.edit', $identity->id) }}">Submit</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <hr>
        @endif

    </div>
@endsection