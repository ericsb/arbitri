@extends('master.master')

@section('content')
    <div class="container">
        <h2>Company’s bank account ({{ $identity->name }})</h2>
        <hr>
        <div class="mt-2"></div>
        <div class="">
            <form action="{{ route('bankings.company.update', $identity->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('post')
                <div class="form-group">
                    <label for="name">1.What type of fiat currencies must be supported by the account(s)?</label>
                    <input type="text" name="fiat_currencies" class="form-control" value="{{ $bankingCompany->fiat_currencies ?? '' }}" id="surname">
                </div>

                <div class="form-group">
                    <label for="name">2.What type of cryptocurrencies must be supported by the account(s)?</label>
                    <input type="text" name="crypto_currencies" class="form-control" value="{{ $bankingCompany->crypto_currencies ?? '' }}" id="surname">
                </div>

                <div class="form-group">
                    <label for="name">3.What is the yearly average volume of transactions you make (estimation in CHF)?</label>
                    <input type="text" name="yearly_transaction_volume" class="form-control" value="{{ $bankingCompany->yearly_transaction_volume ?? '' }}" id="surname">
                </div>

                <div class="form-group">
                    <label for="name">4.What is the average amount in custody you expect
                        in CHF?</label>
                    <input type="text" name="avg_amount_in_custody" class="form-control" value="{{ $bankingCompany->avg_amount_in_custody ?? '' }}" id="surname">
                </div>

                <div class="form-group">
                    <label for="name">5.What is the source of wealth? (N.b. Explanation of
                        where your income comes from)
                    </label>
                    <input type="text" name="source_of_wealth" class="form-control" value="{{ $bankingCompany->source_of_wealth ?? '' }}" id="surname">
                </div>

                <p>6.Does the source of wealth come from cryptoassets
                    or transactions from crypto exchanges?
                </p>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="wealth_from_crypto_status" id="exampleRadios1" value="Yes" @if($bankingCompany->wealth_from_crypto_status == "Yes") checked @endif>
                    <label class="form-check-label" >
                        Yes
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="wealth_from_crypto_status" id="exampleRadios2" value="No"  @if($bankingCompany->wealth_from_crypto_status == "No") checked @endif>
                    <label class="form-check-label" >
                        No
                    </label>
                </div>
                <br>
                <p>
                    7.If yes, please provide the list of transactions and an
                    account statement from your wallet provider (if
                    available). By submitting the wallets you confirm
                    being the sole owner of the wallet(s).
                </p>

                <div class="form-group">

                    <label for="name">8.File (1):</label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'banking_company', @$bankingCompany->transactionFile1->name) }}">
                        @if(@$bankingCompany->transactionFile1->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="transaction_file_1" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <br>
                <div class="form-group">

                    <label for="name">9.File (2):</label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'banking_company', @$bankingCompany->transactionFile2->name) }}">
                        @if(@$bankingCompany->transactionFile2->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="transaction_file_2" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <br>
                <div class="form-group">

                    <label for="name">10.File (3):</label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'banking_company', @$bankingCompany->transactionFile3->name) }}">
                        @if(@$bankingCompany->transactionFile3->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="transaction_file_3" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <hr>
                <h3>Information regarding the Company/Association</h3>
                <div class="form-group">

                    <label for="name">11.Excerpt of the commercial register:</label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'banking_company', @$bankingCompany->excerptFile->name) }}">
                        @if(@$bankingCompany->excerptFile->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="excerpt_file" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>

                <div class="form-group">
                    <label for="name">12.Memorandum & Articles of Association (simple copy) 1 </label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'banking_company', @$bankingCompany->memoFile1->name) }}">
                        @if(@$bankingCompany->memoFile1->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="memo_file_1" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>

                <div class="form-group">

                    <label for="name">13.Memorandum & Articles of Association (simple copy) 2</label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'banking_company', @$bankingCompany->memoFile2->name) }}">
                        @if(@$bankingCompany->memoFile2->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="memo_file_2" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>

                <div class="form-group">

                    <label for="name">14.Copy of last annual report (if available)</label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'banking_company', @$bankingCompany->annualReportFile->name) }}">
                        @if(@$bankingCompany->annualReportFile->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="annual_report_file" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>

                <div class="form-group">

                    <label for="name">15.Preliminary business plan</label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'banking_company', @$bankingCompany->preliminaryFile->name) }}">
                        @if(@$bankingCompany->preliminaryFile->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="preliminary_file" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>

                <div class="form-group">

                    <label for="name">16.Current Organizational chart of internal structures and external Organizational
                        chart of group structures (holding, parent company, subsidiaries), including
                        participation shares, including Name, CV of beneficial owner(s) of company. File 1</label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'banking_company', @$bankingCompany->organizationFile1->name) }}">
                        @if(@$bankingCompany->organizationFile1->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="current_organization_file_1" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>

                <div class="form-group">

                    <label for="name">17.Current Organizational chart of internal structures and external Organizational
                        chart of group structures (holding, parent company, subsidiaries), including
                        participation shares, including Name, CV of beneficial owner(s) of company. File 2</label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'banking_company', @$bankingCompany->organizationFile2->name) }}">
                        @if(@$bankingCompany->organizationFile2->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="current_organization_file_2" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>

                <div class="form-group">

                    <label for="name">18.List of shareholders (including : name, nationality, domicile, percentage of
                        participation / or company name, country and place of registration and
                        percentage of participation
                    </label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'banking_company', @$bankingCompany->shareholderFile->name) }}">
                        @if(@$bankingCompany->shareholderFile->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="shareholder_file" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>

                <div class="form-group">
                    <label for="name">19.Do you have any comments or additional information
                        to provide for this section?
                    </label>
                    <input type="text" name="additional_comment" class="form-control" value="{{ $bankingCompany->additional_comment ?? '' }}" id="surname">
                </div>


                <div class="form-group">

                    <label for="name">20.Add additional files in this section 1
                    </label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'banking_company', @$bankingCompany->additionalFile1->name) }}">
                        @if(@$bankingCompany->additionalFile1->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="additional_file_1" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>

                <div class="form-group">

                    <label for="name">21.Add additional files in this section 2
                    </label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'banking_company', @$bankingCompany->additionalFile2->name) }}">
                        @if(@$bankingCompany->additionalFile2->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="additional_file_2" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>

                <div class="form-group">

                    <label for="name">22.Add additional files in this section 3
                    </label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'banking_company', @$bankingCompany->additionalFile3->name) }}">
                        @if(@$bankingCompany->additionalFile3->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="additional_file_3" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Save!</button>
            </form>
        </div>
    </div>
@endsection