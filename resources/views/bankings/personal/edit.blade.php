@extends('master.master')

@section('content')
    <div class="container">
        <h2>Personal bank account ({{ $identity->name }})</h2>
        <hr>
        <div class="mt-2"></div>
        <div class="">
            <form action="{{ route('bankings.personal.update', $identity->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('post')
                <div class="form-group">
                    <label for="name">1.What type of fiat currencies must be supported by the account(s)?</label>
                    <input type="text" name="fiat_currencies" class="form-control" value="{{ $bankingPersonal->fiat_currencies }}" id="surname">
                </div>

                <div class="form-group">
                    <label for="name">2.What type of cryptocurrencies must be supported by the account(s)?</label>
                    <input type="text" name="crypto_currencies" class="form-control" value="{{ $bankingPersonal->crypto_currencies }}" id="surname">
                </div>

                <div class="form-group">
                    <label for="name">3.What is the yearly average volume of transactions
                        you make (estimation in CHF)?
                    </label>
                    <input type="text" name="yearly_transaction_volume" class="form-control" value="{{ $bankingPersonal->yearly_transaction_volume }}" id="surname">
                </div>


                <div class="form-group">
                    <label for="name">4.What is the average amount in custody you expect in
                        CHF?
                    </label>
                    <input type="text" name="avg_amount_in_custody" class="form-control" value="{{ $bankingPersonal->avg_amount_in_custody }}" id="surname">
                </div>

                <div class="form-group">
                    <label for="name">5.What is the source of wealth? (N.b. Explanation of
                        where your income comes from)
                    </label>
                    <input type="text" name="source_of_wealth" class="form-control" value="{{ $bankingPersonal->source_of_wealth }}" id="surname">
                </div>

                <p>6.Does the source of wealth come from cryptoassets
                    or transactions from crypto exchanges?
                </p>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="wealth_from_crypto_status" id="exampleRadios1" value="Yes" @if($bankingPersonal->wealth_from_crypto_status == "Yes") checked @endif>
                    <label class="form-check-label" >
                        Yes
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="wealth_from_crypto_status" id="exampleRadios2" value="No"  @if($bankingPersonal->wealth_from_crypto_status == "No") checked @endif>
                    <label class="form-check-label" >
                        No
                    </label>
                </div>
                <br>
                <p>
                    7.If yes, please provide the list of transactions and an
                    account statement from your wallet provider (if
                    available). By submitting the wallets you confirm
                    being the sole owner of the wallet(s).
                </p>

                <div class="form-group">

                    <label for="name">8.Additional files (1):</label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'banking_personal', @$bankingPersonal->file1->name) }}">
                        @if(@$bankingPersonal->file1->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="file1" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <br>
                <div class="form-group">

                    <label for="name">9.Additional files (2):</label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'banking_personal', @$bankingPersonal->file2->name) }}">
                        @if(@$bankingPersonal->file2->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="file2" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <br>
                <div class="form-group">

                    <label for="name">10.Additional files (3):</label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'banking_personal', @$bankingPersonal->file3->name) }}">
                        @if(@$bankingPersonal->file3->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="file3" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <br>


                <button type="submit" class="btn btn-primary">Save!</button>
            </form>
        </div>
    </div>
@endsection