@extends('master.master')

@section('content')
    <div class="container">
        <h2>Capital deposit bank account ({{ $identity->name }})</h2>
        <hr>
        <div class="mt-2"></div>
        <div class="">
            <form action="{{ route('bankings.capital.update', $identity->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('post')
                <div class="form-group">
                    <label for="name">1.How much will be deposited in the account in CHF?</label>
                    <input type="text" name="deposit" class="form-control" value="{{ $bankingCapital->deposit }}" id="surname">
                </div>

                <div class="form-group">
                    <label for="name">2.What is the source of wealth? (N.b. Explanation of
                        where your income comes from)</label>
                    <input type="text" name="source_of_wealth" class="form-control" value="{{ $bankingCapital->source_of_wealth }}" id="surname">
                </div>


                <p>3.Does the source of wealth come from cryptoassets
                    or transactions from crypto exchanges?
                </p>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="wealth_from_crypto_status" id="exampleRadios1" value="Yes" @if($bankingCapital->wealth_from_crypto_status == "Yes") checked @endif>
                    <label class="form-check-label" >
                        Yes
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="wealth_from_crypto_status" id="exampleRadios2" value="No"  @if($bankingCapital->wealth_from_crypto_status == "No") checked @endif>
                    <label class="form-check-label" >
                        No
                    </label>
                </div>
                <br>
                <p>
                    4.If yes, please provide the list of transactions and an
                    account statement from your wallet provider (if
                    available). By submitting the wallets you confirm
                    being the sole owner of the wallet(s).
                </p>

                <div class="form-group">
                    <label for="name">5.Additional files (1):</label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'banking_capital', @$bankingCapital->file1->name) }}">
                        @if(@$bankingCapital->file1->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="file1" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <br>
                <div class="form-group">

                    <label for="name">6.Additional files (2):</label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'banking_capital', @$bankingCapital->file2->name) }}">
                        @if(@$bankingCapital->file2->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="file2" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <br>
                <div class="form-group">

                    <label for="name">7.Additional files (3):</label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'banking_capital', @$bankingCapital->file3->name) }}">
                        @if(@$bankingCapital->file3->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="file3" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <br>
                <div class="form-group">
                    <label for="name">8.How many people does the company employ or will
                        employ?
                    </label>
                    <input type="text" name="employ_count" class="form-control" value="{{ $bankingCapital->employ_count }}" id="surname">
                </div>

                <button type="submit" class="btn btn-primary">Save!</button>
            </form>
        </div>
    </div>
@endsection