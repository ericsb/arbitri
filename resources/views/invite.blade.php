@extends('master.master')
@section('content')
    <div class="container">
        <div class="col-12">
            <h2>Invite by email</h2>
            <hr>
            <div class="mt-2"></div>
            <div class="">
                <form action="{{ route('store.invite') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('post')

                    @if(!empty(\Illuminate\Support\Facades\Session::get('success')))
                    <p class="alert-success p-3">
                        {{  \Illuminate\Support\Facades\Session::get('success')}}
                    </p>
                    @endif

                    @if ($errors->any())
                        @foreach ($errors->all() as $error)
                            <p class="alert-danger p-3">
                                {{ $error }}
                            </p>
                        @endforeach
                    @endif


                    <div class="form-group">
                        <label for="name">
                            Enter email address:
                        </label>
                        <input type="text" name="email" class="form-control" id="description">
                    </div>

                    <button type="submit" class="btn btn-primary">Save</button>
                </form>
            </div>
        </div>
    </div>
@endsection