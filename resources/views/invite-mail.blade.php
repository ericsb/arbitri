Hello
<br>
You have been invited to edit the form for company {{ $companyName }}.
<br>
Please login via this link: <a href="https://form.arbitri.ch/login">form.arbitri.ch/login</a>
<br>
Your password is: {{ $password }}
<br>
Thanks