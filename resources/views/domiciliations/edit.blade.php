@extends('master.master')

@section('content')
    <div class="container">
        <h2>Domiciliation & Residence</h2>
        <hr>
        <div class="mt-2"></div>
        <div class="">
            <form action="{{ route('domiciliation.update') }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('post')


                <p>1.Do you want us to find a third-party accepting the
                    domiciliation of your entity in its offices? (e.g. the
                    Auditor can provide those services) <x-flag tableName="domiciliations" colName="third_party_status" rowId="{{ $domiciliation->id }}" /></p>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="third_party_status" id="exampleRadios1" value="Yes" @if($domiciliation->third_party_status == "Yes") checked @endif>
                    <label class="form-check-label" >
                        Yes
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="third_party_status" id="exampleRadios2" value="No"  @if($domiciliation->third_party_status == "No") checked @endif>
                    <label class="form-check-label" >
                        No
                    </label>
                </div>
                <div class="form-group">
                    <label for="name"> If yes, do you have any specific requirements? <x-flag tableName="domiciliations" colName="third_party_requirements" rowId="{{ $domiciliation->id }}" /></label>
                    <input type="text" name="third_party_requirements" class="form-control" value="{{ $domiciliation->third_party_requirements ?? '' }}" id="description">
                </div>


                <hr>


                <p>2.Are you looking for renting or buying? <x-flag tableName="domiciliations" colName="looking_renting_status" rowId="{{ $domiciliation->id }}" /></p>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="looking_renting_status" id="exampleRadios1" value="Yes" @if($domiciliation->looking_renting_status == "Yes") checked @endif>
                    <label class="form-check-label" >
                        Yes
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="looking_renting_status" id="exampleRadios2" value="No"  @if($domiciliation->looking_renting_status == "No") checked @endif>
                    <label class="form-check-label" >
                        No
                    </label>
                </div>


                <hr>

                <div class="form-group">
                    <label for="name">3.Budget per month with all charges included <x-flag tableName="domiciliations" colName="budget_per_month" rowId="{{ $domiciliation->id }}" />
                    </label>
                    <input type="text" name="budget_per_month" class="form-control" value="{{ $domiciliation->budget_per_month ?? '' }}" id="description">
                </div>


                <hr>

                <div class="form-group">
                    <label for="name">4.Purchasing budget all fees included <x-flag tableName="domiciliations" colName="purchasing_budget" rowId="{{ $domiciliation->id }}" />
                    </label>
                    <input type="text" name="purchasing_budget" class="form-control" value="{{ $domiciliation->purchasing_budget ?? '' }}" id="description">
                </div>

                <hr>
                <p>5.Do you need a residence or a domiciliation?
                    (Domiciliation will provide an official address for your
                    company whilst residence only provide you a working
                    space) <x-flag tableName="domiciliations" colName="need_residence_status" rowId="{{ $domiciliation->id }}" /></p>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="need_residence_status" id="exampleRadios1" value="Yes" @if($domiciliation->need_residence_status == "Yes") checked @endif>
                    <label class="form-check-label" >
                        I need an official address of domiciliation
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="need_residence_status" id="exampleRadios2" value="No"  @if($domiciliation->need_residence_status == "No") checked @endif>
                    <label class="form-check-label" >
                        I only need an office but not necessarily an address of domiciliation
                    </label>
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">6.In what Country? <x-flag tableName="domiciliations" colName="country" rowId="{{ $domiciliation->id }}" />
                    </label>
                    <input type="text" name="country" class="form-control" value="{{ $domiciliation->country ?? '' }}" id="description">
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">7.In what Canton or Region? <x-flag tableName="domiciliations" colName="canton" rowId="{{ $domiciliation->id }}" />
                    </label>
                    <input type="text" name="canton" class="form-control" value="{{ $domiciliation->canton ?? '' }}" id="description">
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">8.In or nearby what City? <x-flag tableName="domiciliations" colName="city" rowId="{{ $domiciliation->id }}" />
                    </label>
                    <input type="text" name="city" class="form-control" value="{{ $domiciliation->city ?? '' }}" id="description">
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">9.Do you have a preferred neighborhood? (e.g. city center, etc.) <x-flag tableName="domiciliations" colName="neighborhood" rowId="{{ $domiciliation->id }}" /></label>
                    <input type="text" name="neighborhood" class="form-control" value="{{ $domiciliation->neighborhood ?? '' }}" id="description">
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">10.Would you like to be near specific infrastructure? (e.g. airport, train station, etc.) <x-flag tableName="infrastructure" colName="third_party_status" rowId="{{ $domiciliation->id }}" /></label>
                    <input type="text" name="infrastructure" class="form-control" value="{{ $domiciliation->infrastructure ?? '' }}" id="description">
                </div>
                <hr>
                <p>11.In the eventuality you need multiple offices, select “yes”, and discuss it directly
                    with your counsel. <x-flag tableName="domiciliations" colName="multiple_office_status" rowId="{{ $domiciliation->id }}" />
                </p>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="multiple_office_status" id="exampleRadios1" value="Yes" @if($domiciliation->multiple_office_status == "Yes") checked @endif>
                    <label class="form-check-label" >
                        Yes, I need several offices.
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="multiple_office_status" id="exampleRadios2" value="No"  @if($domiciliation->multiple_office_status == "No") checked @endif>
                    <label class="form-check-label" >
                        No
                    </label>
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">12.What is your preferred style? (modern, authentic, etc.) <x-flag tableName="domiciliations" colName="preferred_style" rowId="{{ $domiciliation->id }}" />
                    </label>
                    <input type="text" name="preferred_style" class="form-control" value="{{ $domiciliation->preferred_style ?? '' }}" id="description">
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">13.How many m2 do you want? <x-flag tableName="domiciliations" colName="m2_count" rowId="{{ $domiciliation->id }}" /></label>
                    <input type="text" name="m2_count" class="form-control" value="{{ $domiciliation->m2_count ?? '' }}" id="description">
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">14.How many people will be occupying the offices? <x-flag tableName="domiciliations" colName="people_count" rowId="{{ $domiciliation->id }}" /></label>
                    <input type="text" name="people_count" class="form-control" value="{{ $domiciliation->people_count ?? '' }}" id="description">
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">15.Do you need any specific infrastructure in the nearby building ?(gym,
                        restaurant, bus station) <x-flag tableName="domiciliations" colName="infrastructure_near_by" rowId="{{ $domiciliation->id }}" /></label>
                    <input type="text" name="infrastructure_near_by" class="form-control" value="{{ $domiciliation->infrastructure_near_by ?? '' }}" id="description">
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">16.Do you need any specific infrastructure in the building ?(gym, lift, terrace,
                        cafeteria, reception) <x-flag tableName="domiciliations" colName="infrastructure_in" rowId="{{ $domiciliation->id }}" /></label>
                    <input type="text" name="infrastructure_in" class="form-control" value="{{ $domiciliation->infrastructure_in ?? '' }}" id="description">
                </div>
                <hr>
                <p>17.Do you have any specific requirements for the offices? <x-flag tableName="domiciliations" colName="specific_requirements" rowId="{{ $domiciliation->id }}" />
                </p>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="specific_requirements[]" id="exampleRadios1" value="Furnished" @if(!empty($domiciliation->specific_requirements) && in_array('Furnished',json_decode($domiciliation->specific_requirements))) checked @endif>
                    <label class="form-check-label" >
                        Furnished
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="specific_requirements[]" id="exampleRadios2" value="High ceiling"  @if(!empty($domiciliation->specific_requirements) && in_array('High ceiling',json_decode($domiciliation->specific_requirements))) checked @endif>
                    <label class="form-check-label" >
                        High ceiling
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="specific_requirements[]" id="exampleRadios2" value="Interphone"  @if(!empty($domiciliation->specific_requirements) && in_array('Interphone',json_decode($domiciliation->specific_requirements))) checked @endif>
                    <label class="form-check-label" >
                        Interphone
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="specific_requirements[]" id="exampleRadios2" value="Alarm"  @if(!empty($domiciliation->specific_requirements) && in_array('Alarm',json_decode($domiciliation->specific_requirements))) checked @endif>
                    <label class="form-check-label" >
                        Alarm
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="specific_requirements[]" id="exampleRadios2" value="Air conditioning"  @if(!empty($domiciliation->specific_requirements) && in_array('Air conditioning',json_decode($domiciliation->specific_requirements))) checked @endif>
                    <label class="form-check-label" >
                        Air conditioning
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="specific_requirements[]" id="exampleRadios2" value="Balcony / terrace"  @if(!empty($domiciliation->specific_requirements) && in_array('Balcony / terrace',json_decode($domiciliation->specific_requirements))) checked @endif>
                    <label class="form-check-label" >
                        Balcony / terrace
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="specific_requirements[]" id="exampleRadios2" value="Independent space"  @if(!empty($domiciliation->specific_requirements) && in_array('Independent space',json_decode($domiciliation->specific_requirements))) checked @endif>
                    <label class="form-check-label" >
                        Independent space
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="specific_requirements[]" id="exampleRadios2" value="Collective space"  @if(!empty($domiciliation->specific_requirements) && in_array('Collective space',json_decode($domiciliation->specific_requirements))) checked @endif>
                    <label class="form-check-label" >
                        Collective space
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="specific_requirements[]" id="exampleRadios2" value="Dish washer, mixer, etc."  @if(!empty($domiciliation->specific_requirements) && in_array('Dish washer, mixer, etc.',json_decode($domiciliation->specific_requirements))) checked @endif>
                    <label class="form-check-label" >
                        Dish washer, mixer, etc.
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="specific_requirements[]" id="exampleRadios2" value="Kitchen"  @if(!empty($domiciliation->specific_requirements) && in_array('Kitchen',json_decode($domiciliation->specific_requirements))) checked @endif>
                    <label class="form-check-label" >
                        Kitchen
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="specific_requirements[]" id="exampleRadios2" value="Lift"  @if(!empty($domiciliation->specific_requirements) && in_array('Lift',json_decode($domiciliation->specific_requirements))) checked @endif>
                    <label class="form-check-label" >
                        Lift
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="specific_requirements[]" id="exampleRadios2" value="Garden"  @if(!empty($domiciliation->specific_requirements) && in_array('Garden',json_decode($domiciliation->specific_requirements))) checked @endif>
                    <label class="form-check-label" >
                        Garden
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="specific_requirements[]" id="exampleRadios2" value="View"  @if(!empty($domiciliation->specific_requirements) && in_array('View',json_decode($domiciliation->specific_requirements))) checked @endif>
                    <label class="form-check-label" >
                        View
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="specific_requirements[]" id="exampleRadios2" value="Top floor"  @if(!empty($domiciliation->specific_requirements) && in_array('Top floor',json_decode($domiciliation->specific_requirements))) checked @endif>
                    <label class="form-check-label" >
                        Top floor
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="specific_requirements[]" id="exampleRadios2" value="Ground floor"  @if(!empty($domiciliation->specific_requirements) && in_array('Ground floor',json_decode($domiciliation->specific_requirements))) checked @endif>
                    <label class="form-check-label" >
                        Ground floor
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="specific_requirements[]" id="exampleRadios2" value="Part of a local Community"  @if(!empty($domiciliation->specific_requirements) && in_array('Part of a local Community',json_decode($domiciliation->specific_requirements))) checked @endif>
                    <label class="form-check-label" >
                        Part of a local Community
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="specific_requirements[]" id="exampleRadios2" value="Sunny (oriented south)"  @if(!empty($domiciliation->specific_requirements) && in_array('Sunny (oriented south)',json_decode($domiciliation->specific_requirements))) checked @endif>
                    <label class="form-check-label" >
                        Sunny (oriented south)
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="specific_requirements[]" id="exampleRadios2" value="Reception space"  @if(!empty($domiciliation->specific_requirements) && in_array('Reception space',json_decode($domiciliation->specific_requirements))) checked @endif>
                    <label class="form-check-label" >
                        Reception space
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="specific_requirements[]" id="exampleRadios2" value="Conference room"  @if(!empty($domiciliation->specific_requirements) && in_array('Conference room',json_decode($domiciliation->specific_requirements))) checked @endif>
                    <label class="form-check-label" >
                        Conference room
                    </label>
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">18.Do you have any comments or additional information to
                        provide for this section? <x-flag tableName="domiciliations" colName="comments" rowId="{{ $domiciliation->id }}" /></label>
                    <input type="text" name="comments" class="form-control" value="{{ $domiciliation->comments ?? '' }}" id="description">
                </div>
                <hr>
                <div class="form-group">
                    <label for="name">19.Additional files (1): <x-flag tableName="domiciliations" colName="file1" rowId="{{ $domiciliation->id }}" /></label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'domiciliations', @$domiciliation->file1->name) }}">
                        @if(@$domiciliation->file1->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="file1" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name">20.Additional files (2): <x-flag tableName="domiciliations" colName="file2" rowId="{{ $domiciliation->id }}" /></label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'domiciliations', @$domiciliation->file2->name) }}">
                        @if(@$domiciliation->file2->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="file2" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name">21.Additional files (3): <x-flag tableName="domiciliations" colName="file3" rowId="{{ $domiciliation->id }}" /></label>
                    <a href="{{ getFile(\Illuminate\Support\Facades\Auth::user()->id,'domiciliations', @$domiciliation->file3->name) }}">
                        @if(@$domiciliation->file3->name)
                            <div>
                                <button class="btn btn-secondary">
                                    Download file
                                </button>
                            </div>
                            <br>
                        @endif
                    </a>
                    <div class="custom-file">
                        <input name="file3" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>
@endsection