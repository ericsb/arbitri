<?php

namespace App\Exports;

use App\Models\Foundation;
use App\Models\FoundationDirection;
use App\Models\Identity;
use Illuminate\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;


class FoundationDirectionExport implements FromView
{
    protected $userId;
    /**
     *  constructor.
     */
    public function __construct($userId)
    {
        $this->userId = $userId;
    }


    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        //
    }

    public function view(): View
    {
        $foundationDirections = FoundationDirection::query()
            ->where('user_id', $this->userId)
            ->get();
        return view('exports.foundation-directions', [
            'foundationDirections' => $foundationDirections,
        ]);
    }
}
