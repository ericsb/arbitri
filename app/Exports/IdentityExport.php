<?php

namespace App\Exports;

use App\Models\Identity;
use Illuminate\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;


class IdentityExport implements FromView
{
    protected $userId;
    /**
     * IdentityExport constructor.
     */
    public function __construct($userId)
    {
        $this->userId = $userId;
    }


    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        //
    }

    public function view(): View
    {
        $identities = Identity::query()
            ->where('user_id', $this->userId)
            ->get();
        return view('exports.identities', [
            'identities' => $identities,
        ]);
    }
}
