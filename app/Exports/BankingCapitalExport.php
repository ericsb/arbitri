<?php

namespace App\Exports;

use App\Models\Banking;
use App\Models\BankingCapital;
use App\Models\Domiciliation;
use App\Models\Foundation;
use App\Models\FoundationDirection;
use App\Models\Identity;
use Illuminate\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;


class BankingCapitalExport implements FromView
{
    protected $userId;
    /**
     *  constructor.
     */
    public function __construct($userId)
    {
        $this->userId = $userId;
    }


    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        //
    }

    public function view(): View
    {
        $identityIds = Identity::query()
            ->where('user_id', $this->userId)
            ->get()->pluck('id');
        $bankingCapitals = BankingCapital::query()
            ->whereIn('identity_id', $identityIds)
            ->get();
        return view('exports.banking-capitals', [
            'bankingCapitals' => $bankingCapitals,
        ]);
    }
}
