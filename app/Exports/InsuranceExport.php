<?php

namespace App\Exports;

use App\Models\Auditor;
use App\Models\Domiciliation;
use App\Models\Foundation;
use App\Models\FoundationDirection;
use App\Models\Identity;
use App\Models\Insurance;
use Illuminate\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;


class InsuranceExport implements FromView
{
    protected $userId;
    /**
     *  constructor.
     */
    public function __construct($userId)
    {
        $this->userId = $userId;
    }


    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        //
    }

    public function view(): View
    {
        $insurances = Insurance::query()
            ->where('user_id', $this->userId)
            ->get();
        return view('exports.insurances', [
            'insurances' => $insurances,
        ]);
    }
}
