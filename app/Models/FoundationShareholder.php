<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FoundationShareholder extends Model
{
    protected $table = 'foundation_shareholders';
    protected $guarded = [];
}
