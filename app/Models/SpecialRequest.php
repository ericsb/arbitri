<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SpecialRequest extends Model
{
    protected $table = 'special_requests';
    protected $guarded = [];


    public function file1()
    {
        return $this->hasOne(File::class,'id','additional_file_id_1');
    }

    public function file2()
    {
        return $this->hasOne(File::class,'id','additional_file_id_2');
    }


    public function file3()
    {
        return $this->hasOne(File::class,'id','additional_file_id_3');
    }


    public function file4()
    {
        return $this->hasOne(File::class,'id','additional_file_id_4');
    }

}
