<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Domiciliation extends Model
{
    protected $table = 'domiciliations';
    protected $guarded = [];

    public function file1()
    {
        return $this->hasOne(File::class,'id','additional_file_id_1');
    }

    public function file2()
    {
        return $this->hasOne(File::class,'id','additional_file_id_2');
    }

    public function file3()
    {
        return $this->hasOne(File::class,'id','additional_file_id_3');
    }
}
