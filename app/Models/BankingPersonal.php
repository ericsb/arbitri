<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BankingPersonal extends Model
{
    protected $table = 'banking_personals';
    protected $guarded = [];

    public function identity()
    {
        return $this->hasOne(Identity::class,'id','identity_id');
    }

    public function file1()
    {
        return $this->hasOne(File::class,'id','transaction_file_id_1');
    }

    public function file2()
    {
        return $this->hasOne(File::class,'id','transaction_file_id_2');
    }

    public function file3()
    {
        return $this->hasOne(File::class,'id','transaction_file_id_3');
    }
}
