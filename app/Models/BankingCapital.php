<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BankingCapital extends Model
{
    protected $table = 'banking_capitals';
    protected $guarded = [];

    public function file1()
    {
        return $this->hasOne(File::class,'id','transaction_file_id_1');
    }

    public function file2()
    {
        return $this->hasOne(File::class,'id','transaction_file_id_2');
    }

    public function file3()
    {
        return $this->hasOne(File::class,'id','transaction_file_id_3');
    }

    public function identity()
    {
        return $this->hasOne(Identity::class,'id','identity_id');
    }
}
