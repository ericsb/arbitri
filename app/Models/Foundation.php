<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Foundation extends Model
{
    protected $table = 'foundations';
    protected $guarded = [];


    public function electionFile()
    {
        return $this->hasOne(File::class,'id','election_file_id');
    }

    public function furtherDocumentFile1()
    {
        return $this->hasOne(File::class,'id','further_document_file_id_1');
    }

    public function furtherDocumentFile2()
    {
        return $this->hasOne(File::class,'id','further_document_file_id_2');
    }

    public function furtherDocumentFile3()
    {
        return $this->hasOne(File::class,'id','further_document_file_id_3');
    }

    public function attestationFile()
    {
        return $this->hasOne(File::class,'id','attestation_certificate_file_id');
    }

    public function detailedInventoryFile()
    {
        return $this->hasOne(File::class,'id','detailed_inventory_file_id');
    }

    public function balanceSheetFile()
    {
        return $this->hasOne(File::class,'id','balance_sheet_file_id');
    }

    public function anotherFoundationContractFile()
    {
        return $this->hasOne(File::class,'id','another_company_foundation_contract_file_id');
    }

    public function anotherTakeoverContractFile()
    {
        return $this->hasOne(File::class,'id','another_company_takeover_contract_file_id');
    }

    public function anotherAuditorFile()
    {
        return $this->hasOne(File::class,'id','another_company_auditor_providing_file_id');
    }

    public function mandateFile()
    {
        return $this->hasOne(File::class,'id','mandate_file_id');
    }

    public function excerptFile()
    {
        return $this->hasOne(File::class,'id','excerpt_file_id');
    }

    public function additionalFile1()
    {
        return $this->hasOne(File::class,'id','additional_file_id_1');
    }

    public function additionalFile2()
    {
        return $this->hasOne(File::class,'id','additional_file_id_2');
    }

    public function additionalFile3()
    {
        return $this->hasOne(File::class,'id','additional_file_id_3');
    }
}
