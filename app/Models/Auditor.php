<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Auditor extends Model
{
    protected $table = 'auditors';
    protected $guarded = [];

    public function businessPlanFile()
    {
        return $this->hasOne(File::class,'id','business_plan_file_id');
    }

    public function internalChartFile()
    {
        return $this->hasOne(File::class,'id','internal_chart_file_id');
    }

    public function overallChartFile()
    {
        return $this->hasOne(File::class,'id','overall_chart_file_id');
    }

    public function additionalFile1()
    {
        return $this->hasOne(File::class,'id','additional_file_id_1');
    }

    public function additionalFile2()
    {
        return $this->hasOne(File::class,'id','additional_file_id_2');
    }

    public function additionalFile3()
    {
        return $this->hasOne(File::class,'id','additional_file_id_3');
    }
}
