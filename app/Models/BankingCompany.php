<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BankingCompany extends Model
{
    protected $table = 'banking_companies';
    protected $guarded = [];

    public function identity()
    {
        return $this->hasOne(Identity::class,'id','identity_id');
    }

    public function transactionFile1()
    {
        return $this->hasOne(File::class,'id','transaction_file_id_1');
    }

    public function transactionFile2()
    {
        return $this->hasOne(File::class,'id','transaction_file_id_2');
    }

    public function transactionFile3()
    {
        return $this->hasOne(File::class,'id','transaction_file_id_3');
    }

    public function excerptFile()
    {
        return $this->hasOne(File::class,'id','excerpt_file_id');

    }

    public function memoFile1()
    {
        return $this->hasOne(File::class,'id','memo_file_id_1');

    }

    public function memoFile2()
    {
        return $this->hasOne(File::class,'id','memo_file_id_2');

    }

    public function annualReportFile()
    {
        return $this->hasOne(File::class,'id','annual_file_id');

    }

    public function preliminaryFile()
    {
        return $this->hasOne(File::class,'id','preliminary_file_id');

    }

    public function organizationFile1()
    {
        return $this->hasOne(File::class,'id','current_organization_file_id_1');

    }

    public function organizationFile2()
    {
        return $this->hasOne(File::class,'id','current_organization_file_id_2');

    }

    public function shareholderFile()
    {
        return $this->hasOne(File::class,'id','shareholders_file_id');

    }

    public function additionalFile1()
    {
        return $this->hasOne(File::class,'id','additional_file_id_1');

    }

    public function additionalFile2()
    {
        return $this->hasOne(File::class,'id','additional_file_id_2');

    }

    public function additionalFile3()
    {
        return $this->hasOne(File::class,'id','additional_file_id_3');

    }
}
