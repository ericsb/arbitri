<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Identity extends Model
{
    protected $table = 'identities';
    protected $guarded = [];
    protected $with = [
        'idRectoFile',
        'idVersoFile',
        'passportFile',
        'utilityBillFile',
        'criminalRecordExtractFile',
        'companyProofOfExistenceFile',
        'companySignedProcurationFile',
        'companyOtherProofOfExistenceFile',
        'companyOtherSignedProcurationFile',
        'additional1File',
        'additional2File',
        'additional3File',
    ];

    public function idRectoFile()
    {
        return $this->hasOne(File::class,'id','id_recto_file_id');
    }

    public function idVersoFile()
    {
        return $this->hasOne(File::class,'id','id_verso_file_id');
    }

    public function passportFile()
    {
        return $this->hasOne(File::class,'id','passport_file_id');
    }

    public function utilityBillFile()
    {
        return $this->hasOne(File::class,'id','utility_bill_file_id');
    }

    public function criminalRecordExtractFile()
    {
        return $this->hasOne(File::class,'id','criminal_record_extract_file_id');
    }

    public function companyProofOfExistenceFile()
    {
        return $this->hasOne(File::class,'id','company_proof_of_existence_file_id');
    }

    public function companySignedProcurationFile()
    {
        return $this->hasOne(File::class,'id','company_signed_procuration_file_id');
    }

    public function companyOtherProofOfExistenceFile()
    {
        return $this->hasOne(File::class,'id','company_other_proof_of_existence_file_id');
    }

    public function companyOtherSignedProcurationFile()
    {
        return $this->hasOne(File::class,'id','company_other_signed_procuration_file_id');
    }

    public function additional1File()
    {
        return $this->hasOne(File::class,'id','additional_1_file_id');
    }

    public function additional2File()
    {
        return $this->hasOne(File::class,'id','additional_2_file_id');
    }

    public function additional3File()
    {
        return $this->hasOne(File::class,'id','additional_3_file_id');
    }
}
