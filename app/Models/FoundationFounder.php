<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FoundationFounder extends Model
{
    protected $table = 'foundation_founders';
    protected $guarded = [];
}
