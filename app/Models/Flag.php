<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Flag extends Model
{
    protected $table = 'flags';
    protected $guarded = [];

    const TYPES = [
        'check' => 'check',
        'x' => 'x',
        'gear' => 'gear',
        'search' => 'search'
    ];
}
