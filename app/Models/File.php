<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class File extends Model
{
    protected $table = 'files';
    protected $guarded = [];

    public static function upload($file, $prefixPath = null)
    {
        $uploadPath = 'uploads';
        if(!empty($prefixPath))
            $uploadPath = $prefixPath. '/uploads';
        $fileName = time().'_'.$file->getClientOriginalName();
        $file->storeAs($uploadPath, $fileName, 'public');
        $model = self::query()
            ->create([
                'name' => $fileName,
            ]);

        return $model->id;
    }

    public static function uploadV2($file, $postfixPath, $fileName, $userId)
    {
        $uploadPath = 'uploads';
        if(!empty($postfixPath))
            $uploadPath = $userId. '/'. 'uploads'. '/' . $postfixPath;
        $fileName = $fileName. '_' .time(). '_'. Str::random(5) .'.'.$file->getClientOriginalExtension();
        $file->storeAs($uploadPath, $fileName, 'public');
        $model = self::query()
            ->create([
                'name' => $fileName,
            ]);

        return $model->id;
    }
}
