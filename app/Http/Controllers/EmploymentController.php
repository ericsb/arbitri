<?php

namespace App\Http\Controllers;

use App\Exports\EmploymentExport;
use App\Helpers\DAVHelper;
use App\Jobs\UploadExcelToNextCloudJob;
use App\Jobs\UploadFileToNextCloudJob;
use App\Models\Employment;
use App\Models\File;
use App\Models\Identity;
use App\Models\SpecialRequest;
use App\Services\IdentifyServices;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EmploymentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');
    }

    public function edit()
    {
        $employment = Employment::query()
            ->where('user_id', Auth::user()->id)
            ->first();

        if(!$employment)
        {
            $employment = Employment::query()
                ->create([
                    'user_id' => Auth::user()->id
                ]);
        }
        return view('employments.edit', compact('employment'));
    }

    public function update(Request $request)
    {
        $dav = (new DAVHelper());

        $employment = Employment::query()
            ->where('user_id', Auth::user()->id)
            ->first();

        if(!$employment)
        {
            $employment = Employment::query()
                ->create([
                    'user_id' => Auth::user()->id
                ]);
        }

        $employment->hire_employees_status = $request->hire_employees_status;
        $employment->hire_director_status = $request->hire_director_status;
        $employment->immigration_law_status = $request->immigration_law_status;
        $employment->find_personal_status = $request->find_personal_status;
        $employment->introduction_blockchain_status = $request->introduction_blockchain_status;
        $employment->recover_private_key_status = $request->recover_private_key_status;
        $employment->comments = $request->comments;
        $employment->save();

        if(!empty($request->file1))
        {
            $fileId = File::uploadV2($request->file('file1'),'employments','additional_file_1', Auth::user()->id);
            $employment->file_id_1 = $fileId;
            $employment->save();

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'employments',$file->name);

        }

        if(!empty($request->file2))
        {
            $fileId = File::uploadV2($request->file('file2'),'employments','additional_file_2', Auth::user()->id);
            $employment->file_id_2 = $fileId;
            $employment->save();

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'employments',$file->name);

        }

        if(!empty($request->file3))
        {
            $fileId = File::uploadV2($request->file('file3'),'employments','additional_file_3', Auth::user()->id);
            $employment->file_id_3 = $fileId;
            $employment->save();

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'employments',$file->name);

        }

        UploadExcelToNextCloudJob::dispatchSync($dav,Auth::user(),new EmploymentExport(Auth::user()->id),'employments','employments.xlsx');

        return redirect()->back();
    }


}
