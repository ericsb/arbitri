<?php

namespace App\Http\Controllers;

use App\Exports\AuditorExport;
use App\Exports\BankingCapitalExport;
use App\Exports\BankingCompanyExport;
use App\Exports\BankingExport;
use App\Exports\BankingPersonalExport;
use App\Exports\DomiciliationExport;
use App\Exports\EmploymentExport;
use App\Exports\FoundationDirectionExport;
use App\Exports\FoundationFounderExport;
use App\Exports\FoundationPartOneExport;
use App\Exports\FoundationPartTwoExport;
use App\Exports\FoundationShareholderExport;
use App\Exports\IdentityExport;
use App\Exports\InsuranceExport;
use App\Exports\SpecialRequestExport;
use App\Helpers\DAVHelper;
use App\Models\Flag;
use App\Models\User;
use Dotenv\Util\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use ZipArchive;
use File;



class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');
    }

    public function createInvite()
    {
        return view('invite');
    }

    public function storeInvite(Request $request)
    {
        $request->validate([
            'email' => ['required', 'email', 'unique:users,email'],
        ]);

        $randomPassword = \Illuminate\Support\Str::random(10);
        $user = new User();
        $user->email = $request->email;
        $user->password = bcrypt($randomPassword);
        $user->name = $request->email;
        $user->parent_id = Auth::user()->id;
        $user->save();

        $data = [
            'companyName' => Auth::user()->name,
            'password' => $randomPassword,
        ];

        Mail::send('invite-mail', $data, function($message) use($request){
            $message->to($request->email, $request->email)
                ->subject('Invite');
            $message->from('no-reply@arbitri.ch','Arbitri');
        });

        return redirect()->back()->with('success', 'Invited successfully.');


    }

    public function toggleFlag(Request $request)
    {
        $name = $request->name;
        $exploded = explode('-', $name);
        $tableName = $exploded[0];
        $colName = $exploded[1];
        $rowId = $exploded[2];
        $type = $exploded[3];

        $flag = \App\Models\Flag::query()
            ->where('table_name', $tableName)
            ->where('col_name', $colName)
            ->where('row_id', $rowId)
            ->where('type', $type)
            ->first();

        if($flag)
        {
            $flag->delete();
        }
        else
        {
            Flag::query()
                ->create([
                    'table_name' => $tableName,
                    'col_name' => $colName,
                    'row_id' => $rowId,
                    'type' => $type
                ]);
        }

        return response()
            ->json(null,200);
    }

    public function index()
    {
        $pageTitle = 'Welcome';
        return view('dashboard', compact('pageTitle'));
    }

    public function zip()
    {
        $userId = Auth::user()->id;
        Excel::store(new IdentityExport($userId), 'public/'.$userId.'/excels/identities.xlsx');
        Excel::store(new FoundationPartOneExport($userId), 'public/'.$userId.'/excels/foundations-p1.xlsx');
        Excel::store(new FoundationPartTwoExport($userId), 'public/'.$userId.'/excels/foundations-p2.xlsx');
        Excel::store(new FoundationDirectionExport($userId), 'public/'.$userId.'/excels/foundation-directions.xlsx');
        Excel::store(new FoundationFounderExport($userId), 'public/'.$userId.'/excels/foundation-founders.xlsx');
        Excel::store(new FoundationShareholderExport($userId), 'public/'.$userId.'/excels/foundation-shareholders.xlsx');
        Excel::store(new DomiciliationExport($userId), 'public/'.$userId.'/excels/domiciliations.xlsx');
        Excel::store(new AuditorExport($userId), 'public/'.$userId.'/excels/auditors.xlsx');
        Excel::store(new BankingExport($userId), 'public/'.$userId.'/excels/bankings.xlsx');
        Excel::store(new BankingCapitalExport($userId), 'public/'.$userId.'/excels/banking-capitals.xlsx');
        Excel::store(new BankingCompanyExport($userId), 'public/'.$userId.'/excels/banking-companies.xlsx');
        Excel::store(new BankingPersonalExport($userId), 'public/'.$userId.'/excels/banking-personals.xlsx');
        Excel::store(new InsuranceExport($userId), 'public/'.$userId.'/excels/insurances.xlsx');
        Excel::store(new EmploymentExport($userId), 'public/'.$userId.'/excels/employments.xlsx');
        Excel::store(new SpecialRequestExport($userId), 'public/'.$userId.'/excels/special-requests.xlsx');


        $zipFilename = Auth::user()->name . ".zip";
        // Initialize archive object
        $zip = new ZipArchive();
        $zip->open($zipFilename, ZipArchive::CREATE | ZipArchive::OVERWRITE);

        $rootPath = storage_path('app/public/'.Auth::user()->id);
        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($rootPath), RecursiveIteratorIterator::LEAVES_ONLY);

        foreach ($files as $name => $file)
        {
            // Get real and relative path for current file
            $filePath = $file->getRealPath();
            $relativePath = substr($filePath, strlen($rootPath) + 1);

            if (!$file->isDir())
            {
                // Add current file to archive
                $zip->addFile($filePath, $relativePath);
            }else {
                if($relativePath !== false)
                    $zip->addEmptyDir($relativePath);
            }
        }

// Zip archive will be created only after closing object
        $zip->close();

        (new DAVHelper())
            ->mkdir(Auth::user()->name);

        (new DAVHelper())
            ->upload(Auth::user()->name.'/'.time().Auth::user()->name.".zip",file_get_contents("https://form.arbitri.ch/".Auth::user()->name.".zip"));

        return view('thanks');

    }
}
