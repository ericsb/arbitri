<?php

namespace App\Http\Controllers;

use App\Exports\BankingCapitalExport;
use App\Exports\BankingCompanyExport;
use App\Exports\BankingExport;
use App\Exports\BankingPersonalExport;
use App\Helpers\DAVHelper;
use App\Jobs\UploadExcelToNextCloudJob;
use App\Jobs\UploadFileToNextCloudJob;
use App\Models\Banking;
use App\Models\BankingCapital;
use App\Models\BankingCompany;
use App\Models\BankingPersonal;
use App\Models\File;
use App\Models\Identity;
use App\Services\IdentifyServices;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BankingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');
    }

    public function index()
    {
        $user = Auth::user();

        $banking = Banking::query()
            ->where('user_id', $user->id)
            ->first();

        if(!$banking)
        {
            $banking = Banking::query()
                ->create([
                    'user_id' => $user->id,
                ]);
        }

        $identities = Identity::query()
            ->where('user_id', $user->id)
            ->get();

        return view('bankings.index', compact('banking', 'identities'));
    }


    public function update(Request $request)
    {
        $user = Auth::user();

        $banking = Banking::query()
            ->where('user_id', $user->id)
            ->first();

        if(!$banking)
        {
            $banking = Banking::query()
                ->create([
                    'user_id' => $user->id,
                ]);
        }

        if(!empty($request->personal_bank_account_status) && $request->personal_bank_account_status == "Yes")
        {
            $banking->personal_bank_account_status = $request->personal_bank_account_status;
        }
        else
            $banking->personal_bank_account_status = null;


        if(!empty($request->capital_bank_account_status) && $request->capital_bank_account_status == "Yes")
        {
            $banking->capital_bank_account_status = $request->capital_bank_account_status;
        }
        else
            $banking->capital_bank_account_status = null;


        if(!empty($request->company_bank_account_status) && $request->company_bank_account_status == "Yes")
        {
            $banking->company_bank_account_status = $request->company_bank_account_status;
        }
        else
            $banking->company_bank_account_status = null;


        $banking->save();

        $dav = (new DAVHelper());
        UploadExcelToNextCloudJob::dispatchSync($dav,Auth::user(),new BankingExport(Auth::user()->id),'bankings','bankings.xlsx');


        return redirect()->back();
    }

    /* PERSONALS */
    public function editPersonal($id)
    {
        $identity = Identity::query()
            ->where('user_id', Auth::user()->id)
            ->where('id', $id)
            ->firstOrFail();

        $bankingPersonal = BankingPersonal::query()
            ->where('identity_id', $identity->id)
            ->first();

        if(!$bankingPersonal)
        {
            $bankingPersonal = BankingPersonal::query()
                ->create([
                    'identity_id' => $identity->id
                ]);
        }

        return view('bankings.personal.edit', compact('identity', 'bankingPersonal'));
    }

    public function updatePersonal(Request $request, $id)
    {
        $dav = (new DAVHelper());


        $identity = Identity::query()
            ->where('user_id', Auth::user()->id)
            ->where('id', $id)
            ->firstOrFail();

        $bankingPersonal = BankingPersonal::query()
            ->where('identity_id', $identity->id)
            ->first();

        if(!$bankingPersonal)
        {
            $bankingPersonal = BankingPersonal::query()
                ->create([
                    'identity_id' => $identity->id
                ]);
        }

        $bankingPersonal->fiat_currencies = $request->fiat_currencies;
        $bankingPersonal->crypto_currencies = $request->crypto_currencies;
        $bankingPersonal->yearly_transaction_volume = $request->yearly_transaction_volume;
        $bankingPersonal->avg_amount_in_custody = $request->avg_amount_in_custody;
        $bankingPersonal->source_of_wealth = $request->source_of_wealth;
        $bankingPersonal->wealth_from_crypto_status = $request->wealth_from_crypto_status;

        if(!empty($request->file1))
        {
            $fileId = File::uploadV2($request->file('file1'),'banking_personal','additional_file_1', Auth::user()->id);
            $bankingPersonal->transaction_file_id_1 = $fileId;
            $bankingPersonal->save();

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'banking_personal',$file->name);

        }

        if(!empty($request->file2))
        {
            $fileId = File::uploadV2($request->file('file2'),'banking_personal','additional_file_2', Auth::user()->id);
            $bankingPersonal->transaction_file_id_2 = $fileId;
            $bankingPersonal->save();

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'banking_personal',$file->name);

        }

        if(!empty($request->file3))
        {
            $fileId = File::uploadV2($request->file('file3'),'banking_personal','additional_file_3', Auth::user()->id);
            $bankingPersonal->transaction_file_id_3 = $fileId;
            $bankingPersonal->save();

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'banking_personal',$file->name);

        }
        $bankingPersonal->save();

        UploadExcelToNextCloudJob::dispatchSync($dav,Auth::user(),new BankingPersonalExport(Auth::user()->id),'banking_personal','banking_personal.xlsx');


        return redirect()->back();
    }

    /* CAPITALS */
    public function editCapital($id)
    {
        $identity = Identity::query()
            ->where('user_id', Auth::user()->id)
            ->where('id', $id)
            ->firstOrFail();

        $bankingCapital = BankingCapital::query()
            ->where('identity_id', $identity->id)
            ->first();

        if(!$bankingCapital)
        {
            $bankingCapital = BankingCapital::query()
                ->create([
                    'identity_id' => $identity->id
                ]);
        }

        return view('bankings.capital.edit', compact('identity', 'bankingCapital'));
    }

    public function updateCapital(Request $request, $id)
    {
        $dav = (new DAVHelper());

        $identity = Identity::query()
            ->where('user_id', Auth::user()->id)
            ->where('id', $id)
            ->firstOrFail();

        $bankingCapital = BankingCapital::query()
            ->where('identity_id', $identity->id)
            ->first();

        if(!$bankingCapital)
        {
            $bankingCapital = BankingCapital::query()
                ->create([
                    'identity_id' => $identity->id
                ]);
        }

        $bankingCapital->deposit = $request->deposit;
        $bankingCapital->source_of_wealth = $request->source_of_wealth;
        $bankingCapital->wealth_from_crypto_status = $request->wealth_from_crypto_status;
        $bankingCapital->employ_count = $request->employ_count;

        if(!empty($request->file1))
        {
            $fileId = File::uploadV2($request->file('file1'),'banking_capital','additional_file_1', Auth::user()->id);
            $bankingCapital->transaction_file_id_1 = $fileId;
            $bankingCapital->save();

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'banking_capital',$file->name);

        }

        if(!empty($request->file2))
        {
            $fileId = File::uploadV2($request->file('file2'),'banking_capital','additional_file_2', Auth::user()->id);
            $bankingCapital->transaction_file_id_2 = $fileId;
            $bankingCapital->save();

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'banking_capital',$file->name);

        }

        if(!empty($request->file3))
        {
            $fileId = File::uploadV2($request->file('file3'),'banking_capital','additional_file_3', Auth::user()->id);
            $bankingCapital->transaction_file_id_3 = $fileId;
            $bankingCapital->save();

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'banking_capital',$file->name);

        }
        $bankingCapital->save();

        UploadExcelToNextCloudJob::dispatchSync($dav,Auth::user(),new BankingCapitalExport(Auth::user()->id),'banking_capital','banking_capital.xlsx');

        return redirect()->back();
    }

    /* COMPANIES */
    public function editCompany($id)
    {
        $identity = Identity::query()
            ->where('user_id', Auth::user()->id)
            ->where('id', $id)
            ->firstOrFail();

        $bankingCompany = BankingCompany::query()
            ->where('identity_id', $identity->id)
            ->first();

        if(!$bankingCompany)
        {
            $bankingCompany = BankingCompany::query()
                ->create([
                    'identity_id' => $identity->id
                ]);
        }

        return view('bankings.company.edit', compact('identity', 'bankingCompany'));
    }

    public function updateCompany(Request $request, $id)
    {
        $dav = (new DAVHelper());

        $identity = Identity::query()
            ->where('user_id', Auth::user()->id)
            ->where('id', $id)
            ->firstOrFail();

        $bankingCompany = BankingCompany::query()
            ->where('identity_id', $identity->id)
            ->first();

        if(!$bankingCompany)
        {
            $bankingCompany = BankingCompany::query()
                ->create([
                    'identity_id' => $identity->id
                ]);
        }

        $bankingCompany->fiat_currencies = $request->fiat_currencies;
        $bankingCompany->crypto_currencies = $request->crypto_currencies;
        $bankingCompany->yearly_transaction_volume = $request->yearly_transaction_volume;
        $bankingCompany->avg_amount_in_custody = $request->avg_amount_in_custody;
        $bankingCompany->source_of_wealth = $request->source_of_wealth;
        $bankingCompany->wealth_from_crypto_status = $request->wealth_from_crypto_status;
        $bankingCompany->additional_comment = $request->additional_comment;

        $bankingCompany->save();

        if(!empty($request->transaction_file_1))
        {
            $fileId = File::uploadV2($request->file('transaction_file_1'),'banking_company', 'transaction_file_1', Auth::user()->id);
            $bankingCompany->transaction_file_id_1 = $fileId;
            $bankingCompany->save();

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'banking_company',$file->name);
        }

        if(!empty($request->transaction_file_2))
        {
            $fileId = File::uploadV2($request->file('transaction_file_2'),'banking_company', 'transaction_file_2', Auth::user()->id);
            $bankingCompany->transaction_file_id_2 = $fileId;
            $bankingCompany->save();

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'banking_company',$file->name);
        }

        if(!empty($request->transaction_file_3))
        {
            $fileId = File::uploadV2($request->file('transaction_file_3'),'banking_company', 'transaction_file_3', Auth::user()->id);
            $bankingCompany->transaction_file_id_3 = $fileId;
            $bankingCompany->save();

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'banking_company',$file->name);
        }

        if(!empty($request->excerpt_file))
        {
            $fileId = File::uploadV2($request->file('excerpt_file'),'banking_company', 'excerpt_file', Auth::user()->id);
            $bankingCompany->excerpt_file_id = $fileId;
            $bankingCompany->save();

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'banking_company',$file->name);
        }

        if(!empty($request->memo_file_1))
        {
            $fileId = File::uploadV2($request->file('memo_file_1'),'banking_company', 'memo_file_1', Auth::user()->id);
            $bankingCompany->memo_file_id_1 = $fileId;
            $bankingCompany->save();

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'banking_company',$file->name);
        }


        if(!empty($request->memo_file_2))
        {
            $fileId = File::uploadV2($request->file('memo_file_2'),'banking_company', 'memo_file_2', Auth::user()->id);
            $bankingCompany->memo_file_id_2 = $fileId;
            $bankingCompany->save();

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'banking_company',$file->name);
        }

        if(!empty($request->annual_report_file))
        {
            $fileId = File::uploadV2($request->file('annual_report_file'),'banking_company', 'annual_report_file', Auth::user()->id);
            $bankingCompany->annual_file_id = $fileId;
            $bankingCompany->save();

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'banking_company',$file->name);
        }

        if(!empty($request->preliminary_file))
        {
            $fileId = File::uploadV2($request->file('preliminary_file'),'banking_company', 'preliminary_file', Auth::user()->id);
            $bankingCompany->preliminary_file_id = $fileId;
            $bankingCompany->save();

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'banking_company',$file->name);
        }

        if(!empty($request->current_organization_file_1))
        {
            $fileId = File::uploadV2($request->file('current_organization_file_1'),'banking_company', 'current_organization_file_1', Auth::user()->id);
            $bankingCompany->current_organization_file_id_1 = $fileId;
            $bankingCompany->save();

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'banking_company',$file->name);
        }

        if(!empty($request->current_organization_file_2))
        {
            $fileId = File::uploadV2($request->file('current_organization_file_2'),'banking_company', 'current_organization_file_2', Auth::user()->id);
            $bankingCompany->current_organization_file_id_2 = $fileId;
            $bankingCompany->save();

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'banking_company',$file->name);
        }

        if(!empty($request->shareholder_file))
        {
            $fileId = File::uploadV2($request->file('shareholder_file'),'banking_company', 'shareholder_file', Auth::user()->id);
            $bankingCompany->shareholders_file_id = $fileId;
            $bankingCompany->save();

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'banking_company',$file->name);
        }

        if(!empty($request->additional_file_1))
        {
            $fileId = File::uploadV2($request->file('additional_file_1'),'banking_company', 'additional_file_1', Auth::user()->id);
            $bankingCompany->additional_file_id_1 = $fileId;
            $bankingCompany->save();

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'banking_company',$file->name);
        }

        if(!empty($request->additional_file_2))
        {
            $fileId = File::uploadV2($request->file('additional_file_2'),'banking_company', 'additional_file_2', Auth::user()->id);
            $bankingCompany->additional_file_id_2 = $fileId;
            $bankingCompany->save();

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'banking_company',$file->name);
        }

        if(!empty($request->additional_file_3))
        {
            $fileId = File::uploadV2($request->file('additional_file_3'),'banking_company', 'additional_file_3', Auth::user()->id);
            $bankingCompany->additional_file_id_3 = $fileId;
            $bankingCompany->save();

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'banking_company',$file->name);
        }

        UploadExcelToNextCloudJob::dispatchSync($dav,Auth::user(),new BankingCompanyExport(Auth::user()->id),'identities','banking_company.xlsx');

        return redirect()->back();
    }
}
