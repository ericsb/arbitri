<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {
        $users = User::query()
            ->orderByDesc('id')
            ->where('parent_id', '=', null);

        if(!empty($request->search))
        {
            $users->where('name', 'like', '%'. $request->search . '%');
        }

        $users = $users->get();

        return view('admin.users.index', compact('users'));
    }

    public function loginAs($id)
    {
        $user = User::findOrFail($id);
        Auth::guard('web')->login($user);
        return redirect()->route('dashboard');
    }

    public function toggleActive($id)
    {
        $user = User::findOrFail($id);

        if($user->is_active == 1)
        {
            $user->is_active = 0;
            $user->save();

            User::query()
                ->where('parent_id', $id)
                ->update([
                    'is_active' => 0,
                ]);
        }
        else{
            $user->is_active = 1;
            $user->save();

            User::query()
                ->where('parent_id', $id)
                ->update([
                    'is_active' => 1,
                ]);
        }


        return redirect()->back();
    }
}
