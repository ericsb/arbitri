<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{

    public function __construct()
    {
        $this->middleware('guest')
            ->except(['logout']);
    }


    public function loginForm()
    {
        return view('admin-login');
    }


    public function login(Request $request)
    {
        $this->validate($request,[
            'email' => 'required',
            'password' => 'required|min:6'
        ]);

        if(Auth::guard('admin')->attempt(['email' => $request->email , 'password' => $request->password]))
        {
            return redirect()->route('admin.dashboard');
        }

        return redirect()->back()
            ->withInput($request->only('email' ))
            ->with('errors', 'Invalid email or password.');
    }

    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect()->route('admin.login.form');
    }



}
