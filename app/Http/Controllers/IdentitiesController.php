<?php

namespace App\Http\Controllers;

use App\Helpers\DAVHelper;
use App\Models\File;
use App\Models\Identity;
use App\Services\IdentifyServices;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IdentitiesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');
    }

    public function index()
    {
        $user = Auth::user();

        $identities = Identity::query()
            ->where('user_id', $user->id)
            ->get();

        return view('identities.add-name.index', compact('identities'));
    }

    public function create()
    {
        return view('identities.add-name.create');
    }

    public function store(Request $request)
    {

        (new IdentifyServices())
            ->updateOrCreate($request);

        $user = Auth::user();

        $identities = Identity::query()
            ->where('user_id', $user->id)
            ->get();

        return view('identities.add-name.index', compact('identities'));
    }

    public function edit($id)
    {
        $identity = Identity::query()
            ->where('user_id', Auth::user()->id)
            ->where('id', $id)
            ->firstOrFail();
        return view('identities.add-name.edit', compact('identity'));
    }

    public function update(Request $request, $id)
    {
        $user = Auth::user();

        Identity::query()
            ->where('user_id', $user->id)
            ->where('id', $id)
            ->firstOrFail();

        (new IdentifyServices())
            ->updateOrCreate($request,$id);


        $identities = Identity::query()
            ->where('user_id', $user->id)
            ->get();

        return view('identities.add-name.index', compact('identities'));
    }

    public function destroy($id)
    {
        $identity = Identity::query()
            ->where('id', $id)
            ->where('user_id', Auth::user()->id)
            ->firstOrFail();

        $identity->delete();

        return redirect()->back();
    }

}
