<?php

namespace App\Http\Controllers;

use App\Exports\FoundationDirectionExport;
use App\Exports\FoundationFounderExport;
use App\Exports\FoundationPartOneExport;
use App\Exports\FoundationPartTwoExport;
use App\Exports\FoundationShareholderExport;
use App\Helpers\DAVHelper;
use App\Jobs\UploadExcelToNextCloudJob;
use App\Jobs\UploadFileToNextCloudJob;
use App\Models\Domiciliation;
use App\Models\Employment;
use App\Models\File;
use App\Models\Foundation;
use App\Models\FoundationDirection;
use App\Models\FoundationFounder;
use App\Models\FoundationShareholder;
use App\Models\Identity;
use App\Models\SpecialRequest;
use App\Services\IdentifyServices;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FoundationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');
    }


    public function edit()
    {
        $foundation = Foundation::query()
            ->where('user_id', Auth::user()->id)
            ->first();

        if(!$foundation)
        {
            $foundation = Foundation::query()
                ->create([
                    'user_id' => Auth::user()->id
                ]);
        }

        $founders = FoundationFounder::query()
            ->where('user_id', Auth::user()->id)
            ->get();

        $directions = FoundationDirection::query()
            ->where('user_id', Auth::user()->id)
            ->get();

        $shareholders = FoundationShareholder::query()
            ->where('user_id', Auth::user()->id)
            ->get();
        return view('foundations.edit', compact('foundation', 'founders', 'directions', 'shareholders'));
    }

    public function update(Request $request)
    {
        $dav = (new DAVHelper());

        $foundation = Foundation::query()
            ->where('user_id', Auth::user()->id)
            ->first();

        if(!$foundation)
        {
            $foundation = Foundation::query()
                ->create([
                    'user_id' => Auth::user()->id
                ]);
        }

        $foundation->form_type = $request->form_type;
        $foundation->other_type = $request->other_type;
        $foundation->form_name = $request->form_name;
        $foundation->company_name = $request->company_name;
        $foundation->company_alternative_name = $request->company_alternative_name;
        $this->updateFounders($request->founders);
        $this->updateDirections($request->directions);
        $this->updateShareholders($request->shareholders);
        $foundation->address_of_seat = $request->address_of_seat;
        $foundation->political_municipality = $request->political_municipality;
        $foundation->company_office_type = $request->company_office_type;
        $foundation->goals = $request->goals;
        $foundation->goals_involve = $request->goals_involve;
        $foundation->capital = $request->capital;
        $foundation->capital_liberated = $request->capital_liberated;
        $foundation->liberated_in_chf = $request->liberated_in_chf;
        $foundation->type_of_liberation = $request->type_of_liberation;
        $foundation->bank_location_in_escrow = $request->bank_location_in_escrow;
        $foundation->bank_name_in_escrow = $request->bank_name_in_escrow;
        $foundation->liberated_in_cash = $request->liberated_in_cash;
        $foundation->nature_of_contribution = $request->nature_of_contribution;
        $foundation->will_nature_of_contribution = $request->will_nature_of_contribution;
        $foundation->foundation_report = $request->foundation_report;
        $foundation->foundation_contract = $request->foundation_contract;
        $foundation->auditor_providing = $request->auditor_providing;
        $foundation->identity_of_swiss_auditor = $request->identity_of_swiss_auditor;
        $foundation->website_of_auditor = $request->website_of_auditor;
        $foundation->email_of_contact = $request->email_of_contact;
        $foundation->number_of_shares = $request->number_of_shares;
        $foundation->nominal_value = $request->nominal_value;
        $foundation->nature_of_the_share = $request->nature_of_the_share;
        $foundation->communication = $request->communication;
        $foundation->issuance_of_all = $request->issuance_of_all;
        $foundation->issuance_of_share = $request->issuance_of_share;
        $foundation->detail_regarding = $request->detail_regarding;
        $foundation->plan_to_acquire_another_company = $request->plan_to_acquire_another_company;
        $foundation->another_company_identity_of_swiss_auditor = $request->another_company_identity_of_swiss_auditor;
        $foundation->another_company_website_of_auditor = $request->another_company_website_of_auditor;
        $foundation->another_company_email_of_contact = $request->another_company_email_of_contact;
        $foundation->audit_applicable = $request->audit_applicable;
        $foundation->who_will_auditor = $request->who_will_auditor;
        $foundation->financial_year = $request->financial_year;
        $foundation->financial_year_comment = $request->financial_year_comment;
        $foundation->closing_of_the_first_financial_year = $request->closing_of_the_first_financial_year;
        $foundation->closing_of_the_first_financial_year_comment = $request->closing_of_the_first_financial_year_comment;
        $foundation->minimum_quorum = $request->minimum_quorum;
        $foundation->minimum_quorum_comment = $request->minimum_quorum_comment;
        $foundation->majority_required = $request->majority_required;
        $foundation->fiduciary_contract = $request->fiduciary_contract;
        $foundation->shareholder_agreement = $request->shareholder_agreement;
        $foundation->commercial_register = $request->commercial_register;
        $foundation->commercial_register_person = $request->commercial_register_person;
        $foundation->tax_exemption = $request->tax_exemption;
        $foundation->tax_ruling = $request->tax_ruling;
        $foundation->part_of_group = $request->part_of_group;
        $foundation->people_count = $request->people_count;
        $foundation->additional_comments = $request->additional_comments;


        $foundation->save();
        if(!empty($request->election_file))
        {
            $fileId = File::uploadV2($request->file('election_file'),'foundations', 'election_file', Auth::user()->id);
            $foundation->election_file_id = $fileId;
            $foundation->save();

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'foundations',$file->name);
        }

        if(!empty($request->further_document_file_1))
        {
            $fileId = File::uploadV2($request->file('further_document_file_1'),'foundations', 'further_document_file_1', Auth::user()->id);
            $foundation->further_document_file_id_1 = $fileId;
            $foundation->save();

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'foundations',$file->name);
        }

        if(!empty($request->further_document_file_2))
        {
            $fileId = File::uploadV2($request->file('further_document_file_2'),'foundations', 'further_document_file_2', Auth::user()->id);
            $foundation->further_document_file_id_2 = $fileId;
            $foundation->save();

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'foundations',$file->name);
        }

        if(!empty($request->further_document_file_3))
        {
            $fileId = File::uploadV2($request->file('further_document_file_3'),'foundations', 'further_document_file_3', Auth::user()->id);
            $foundation->further_document_file_id_3 = $fileId;
            $foundation->save();

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'foundations',$file->name);
        }

        if(!empty($request->attestation_certificate_file))
        {
            $fileId = File::uploadV2($request->file('attestation_certificate_file'),'foundations', 'attestation_certificate_file', Auth::user()->id);
            $foundation->attestation_certificate_file_id = $fileId;
            $foundation->save();

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'foundations',$file->name);
        }


        if(!empty($request->detailed_inventory_file))
        {
            $fileId = File::uploadV2($request->file('detailed_inventory_file'),'foundations', 'detailed_inventory_file', Auth::user()->id);
            $foundation->detailed_inventory_file_id = $fileId;
            $foundation->save();

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'foundations',$file->name);
        }

        if(!empty($request->balance_sheet_file))
        {
            $fileId = File::uploadV2($request->file('balance_sheet_file'),'foundations', 'balance_sheet_file', Auth::user()->id);
            $foundation->balance_sheet_file_id = $fileId;
            $foundation->save();

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'foundations',$file->name);
        }

        if(!empty($request->another_company_foundation_contract_file))
        {
            $fileId = File::uploadV2($request->file('another_company_foundation_contract_file'),'foundations', 'another_company_foundation_contract_file', Auth::user()->id);
            $foundation->another_company_foundation_contract_file_id = $fileId;
            $foundation->save();

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'foundations',$file->name);
        }

        if(!empty($request->another_company_takeover_contract_file))
        {
            $fileId = File::uploadV2($request->file('another_company_takeover_contract_file'),'foundations', 'another_company_takeover_contract_file', Auth::user()->id);
            $foundation->another_company_takeover_contract_file_id = $fileId;
            $foundation->save();

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'foundations',$file->name);
        }

        if(!empty($request->another_company_auditor_providing_file))
        {
            $fileId = File::uploadV2($request->file('another_company_auditor_providing_file'),'foundations', 'another_company_auditor_providing_file', Auth::user()->id);
            $foundation->another_company_auditor_providing_file_id = $fileId;
            $foundation->save();

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'foundations',$file->name);
        }

        if(!empty($request->mandate_file))
        {
            $fileId = File::uploadV2($request->file('mandate_file'),'foundations', 'mandate_file', Auth::user()->id);
            $foundation->mandate_file_id = $fileId;
            $foundation->save();

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'foundations',$file->name);
        }

        if(!empty($request->excerpt_file))
        {
            $fileId = File::uploadV2($request->file('excerpt_file'),'foundations', 'excerpt_file', Auth::user()->id);
            $foundation->excerpt_file_id = $fileId;
            $foundation->save();

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'foundations',$file->name);
        }

        if(!empty($request->additional_file_1))
        {
            $fileId = File::uploadV2($request->file('additional_file_1'),'foundations', 'additional_file_1', Auth::user()->id);
            $foundation->additional_file_id_1 = $fileId;
            $foundation->save();

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'foundations',$file->name);
        }

        if(!empty($request->additional_file_2))
        {
            $fileId = File::uploadV2($request->file('additional_file_2'),'foundations', 'additional_file_2', Auth::user()->id);
            $foundation->additional_file_id_2 = $fileId;
            $foundation->save();

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'foundations',$file->name);
        }
        if(!empty($request->additional_file_3))
        {
            $fileId = File::uploadV2($request->file('additional_file_3'),'foundations', 'additional_file_3', Auth::user()->id);
            $foundation->additional_file_id_3 = $fileId;
            $foundation->save();

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'foundations',$file->name);
        }

        UploadExcelToNextCloudJob::dispatchSync($dav,Auth::user(),new FoundationPartOneExport(Auth::user()->id),'foundations','foundations-p1.xlsx');
        UploadExcelToNextCloudJob::dispatchSync($dav,Auth::user(),new FoundationPartTwoExport(Auth::user()->id),'foundations','foundations-p2.xlsx');

        return redirect()->back();
    }

    private function updateFounders($founders)
    {


        $user = Auth::user();

        if(!empty($founders))
        {
            FoundationFounder::query()
                ->where('user_id', $user->id)
                ->delete();

            foreach ($founders as $founder)
            {
                FoundationFounder::query()
                    ->create([
                        'user_id' => $user->id,
                        'name' => $founder['name'],
                        'surname' => $founder['surname'],
                    ]);
            }
        }

        $dav = (new DAVHelper());
        UploadExcelToNextCloudJob::dispatchSync($dav,Auth::user(),new FoundationFounderExport(Auth::user()->id),'foundations','foundation-founders.xlsx');
    }

    private function updateDirections($directions)
    {
        $user = Auth::user();

        if(!empty($directions))
        {
            FoundationDirection::query()
                ->where('user_id', $user->id)
                ->delete();

            foreach ($directions as $direction)
            {
                FoundationDirection::query()
                    ->create([
                        'user_id' => $user->id,
                        'name' => $direction['name'],
                        'surname' => $direction['surname'],
                        'role' => $direction['role'],
                        'signature_power' => $direction['signature_power'],
                    ]);
            }
        }
        $dav = (new DAVHelper());
        UploadExcelToNextCloudJob::dispatchSync($dav,Auth::user(),new FoundationDirectionExport(Auth::user()->id),'foundations','foundation-directions.xlsx');

    }

    private function updateShareholders($shareholders)
    {
        $user = Auth::user();

        if(!empty($shareholders))
        {
            FoundationShareholder::query()
                ->where('user_id', $user->id)
                ->delete();

            foreach ($shareholders as $shareholder)
            {
                FoundationShareholder::query()
                    ->create([
                        'user_id' => $user->id,
                        'name' => $shareholder['name'],
                        'surname' => $shareholder['surname'],
                        'number_of_shares' => $shareholder['number_of_shares'],
                    ]);
            }
        }

        $dav = (new DAVHelper());
        UploadExcelToNextCloudJob::dispatchSync($dav,Auth::user(),new FoundationShareholderExport(Auth::user()->id),'foundations','foundation-shareholders.xlsx');


    }
}
