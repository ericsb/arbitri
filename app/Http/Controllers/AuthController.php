<?php

namespace App\Http\Controllers;

use App\Exports\IdentityExport;
use App\Helpers\DAVHelper;
use App\Models\User;
use App\Services\UserServices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;

class AuthController extends Controller
{

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    public function loginForm()
    {
        return view('login');
    }


    public function login(Request $request)
    {
        $this->validate($request,[
            'email' => 'required',
            'password' => 'required|min:6'
        ]);

        $user = User::query()
            ->where('email', $request->email)
            ->firstOrFail();

        if($user->is_active == 0)
        {
            return redirect()->back()
                ->withInput($request->only('email' ))
                ->with('errors', 'Account is not active');
        }

        $isChild = User::query()
            ->where('email', $request->email)
            ->where('parent_id', '!=', null)
            ->first();

        if(Auth::guard('web')->attempt(['email' => $request->email , 'password' => $request->password])){
            if($isChild)
            {
                $parent = User::query()
                    ->where('id', $isChild->parent_id)
                    ->firstOrFail();

                Auth::login($parent);
            }

            return redirect()->route('dashboard');
        }

        return redirect()->back()
            ->withInput($request->only('email' ))
            ->with('errors', 'Invalid email or password.');
    }

    public function logout()
    {
        Auth::guard('web')->logout();
        return redirect()->route('login.form');
    }

    public function registerForm()
    {
        return view('register');
    }


    public function register(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'unique:users,name'],
            'email' => ['required', 'email', 'unique:users,email'],
            'password' => ['required', 'min:6'],
            'admin_secret' => ['required'],
        ]);

        if($request->admin_secret != 'as12AS!@')
        {
            return redirect()->back();
        }

        $u =User::query()
            ->create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
            ]);

        $data = [
            'companyName' =>  $request->name,
            'password' => $request->password,
        ];

        Mail::send('invite-mail', $data, function($message) use($request){
            $message->to($request->email, $request->email)
                ->subject('Invite');
            $message->from('no-reply@arbitri.ch','Arbitri');
        });

        //(new UserServices())->initialNextcloudFolders($request->name);
        //Excel::store(new IdentityExport($u->id), 'public/'.$u->id.'/excels/identities.xlsx');


        if(Auth::guard('web')->attempt(['email' => $request->email , 'password' => $request->password]))
        {
            return redirect()->intended(route('dashboard'));
        }
    }
}
