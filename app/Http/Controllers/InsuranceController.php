<?php

namespace App\Http\Controllers;

use App\Exports\InsuranceExport;
use App\Helpers\DAVHelper;
use App\Jobs\UploadExcelToNextCloudJob;
use App\Jobs\UploadFileToNextCloudJob;
use App\Models\Employment;
use App\Models\File;
use App\Models\Identity;
use App\Models\Insurance;
use App\Models\SpecialRequest;
use App\Services\IdentifyServices;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InsuranceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');
    }


    public function edit()
    {
        $insurance = Insurance::query()
            ->where('user_id', Auth::user()->id)
            ->first();

        if(!$insurance)
        {
            $insurance = Insurance::query()
                ->create([
                    'user_id' => Auth::user()->id
                ]);
        }
        return view('insurances.edit', compact('insurance'));
    }

    public function update(Request $request)
    {

        $dav = (new DAVHelper());


        $insurance = Insurance::query()
            ->where('user_id', Auth::user()->id)
            ->first();

        if(!$insurance)
        {
            $insurance = Insurance::query()
                ->create([
                    'user_id' => Auth::user()->id
                ]);
        }

        $insurance->conclude_insurance_status = $request->conclude_insurance_status;
        $insurance->conclude_insurance_comment = $request->conclude_insurance_comment;
        $insurance->conclude_private_status = $request->conclude_private_status;
        $insurance->conclude_private_comment = $request->conclude_private_comment;
        $insurance->specific_requirements = $request->specific_requirements;
        $insurance->additional_comments = $request->additional_comments;
        $insurance->save();

        if(!empty($request->file1))
        {
            $fileId = File::uploadV2($request->file('file1'),'insurances', 'additional_file_1', Auth::user()->id);
            $insurance->file_id_1 = $fileId;
            $insurance->save();

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'insurances',$file->name);
        }

        if(!empty($request->file2))
        {
            $fileId = File::uploadV2($request->file('file2'),'insurances', 'additional_file_2', Auth::user()->id);
            $insurance->file_id_2 = $fileId;
            $insurance->save();

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'insurances',$file->name);
        }

        if(!empty($request->file3))
        {
            $fileId = File::uploadV2($request->file('file3'),'insurances', 'additional_file_3', Auth::user()->id);
            $insurance->file_id_3 = $fileId;
            $insurance->save();

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'insurances',$file->name);
        }


        UploadExcelToNextCloudJob::dispatchSync($dav,Auth::user(),new InsuranceExport(Auth::user()->id),'insurances','insurances.xlsx');


        return redirect()->back();
    }


}
