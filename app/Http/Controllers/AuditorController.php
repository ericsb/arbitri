<?php

namespace App\Http\Controllers;

use App\Exports\AuditorExport;
use App\Helpers\DAVHelper;
use App\Jobs\UploadExcelToNextCloudJob;
use App\Jobs\UploadFileToNextCloudJob;
use App\Models\Auditor;
use App\Models\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuditorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');
    }

    public function edit()
    {
        $auditor = Auditor::query()
            ->where('user_id', Auth::user()->id)
            ->first();

        if(!$auditor)
        {
            $auditor = Auditor::query()
                ->create([
                    'user_id' => Auth::user()->id
                ]);
        }
        return view('auditors.edit', compact('auditor'));
    }

    public function update(Request $request)
    {

        $dav = (new DAVHelper());

        $auditor = Auditor::query()
            ->where('user_id', Auth::user()->id)
            ->first();

        if(!$auditor)
        {
            $auditor = Auditor::query()
                ->create([
                    'user_id' => Auth::user()->id
                ]);
        }

        $auditor->economic_activities = $request->economic_activities;
        $auditor->avg_volume_transaction = $request->avg_volume_transaction;
        $auditor->book_entries = $request->book_entries;
        $auditor->countries_or_regions = $request->countries_or_regions;
        $auditor->fiat_currencies = $request->fiat_currencies;
        $auditor->crypto_currencies = $request->crypto_currencies;
        $auditor->employ_count = $request->employ_count;
        $auditor->regulated_activity_status = $request->regulated_activity_status;
        $auditor->certification_status = $request->certification_status;
        $auditor->ordinary_status = $request->ordinary_status;
        $auditor->comments = $request->comments;

        if(!empty($request->business_plan_file))
        {
            $fileId = File::uploadV2($request->file('business_plan_file'),'auditors','business_plan', Auth::user()->id);
            $auditor->business_plan_file_id = $fileId;


            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'auditors',$file->name);

        }
        if(!empty($request->internal_chart_file))
        {
            $fileId = File::uploadV2($request->file('internal_chart_file'),'auditors','internal_chart', Auth::user()->id);
            $auditor->internal_chart_file_id = $fileId;

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'auditors',$file->name);
        }
        if(!empty($request->overall_chart_file))
        {
            $fileId = File::uploadV2($request->file('overall_chart_file'),'auditors','overall_chart', Auth::user()->id);
            $auditor->overall_chart_file_id = $fileId;

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'auditors',$file->name);
        }

        if(!empty($request->additional_file_1))
        {
            $fileId = File::uploadV2($request->file('additional_file_1'),'auditors','additional_file_1', Auth::user()->id);
            $auditor->additional_file_id_1 = $fileId;

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'auditors',$file->name);
        }

        if(!empty($request->additional_file_2))
        {
            $fileId = File::uploadV2($request->file('additional_file_2'),'auditors','additional_file_2', Auth::user()->id);
            $auditor->additional_file_id_2 = $fileId;

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'auditors',$file->name);
        }

        if(!empty($request->additional_file_3))
        {
            $fileId = File::uploadV2($request->file('additional_file_3'),'auditors','additional_file_3', Auth::user()->id);
            $auditor->additional_file_id_3 = $fileId;

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'auditors',$file->name);
        }

        $auditor->save();


        UploadExcelToNextCloudJob::dispatchSync($dav,Auth::user(),new AuditorExport(Auth::user()->id),'auditors','auditors.xlsx');

        return redirect()->back();
    }
}
