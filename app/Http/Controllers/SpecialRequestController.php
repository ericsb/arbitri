<?php

namespace App\Http\Controllers;

use App\Exports\SpecialRequestExport;
use App\Helpers\DAVHelper;
use App\Jobs\UploadExcelToNextCloudJob;
use App\Jobs\UploadFileToNextCloudJob;
use App\Models\File;
use App\Models\Identity;
use App\Models\SpecialRequest;
use App\Services\IdentifyServices;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class SpecialRequestController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');
    }


    public function edit()
    {
        $specialRequest = SpecialRequest::query()
            ->where('user_id', Auth::user()->id)
            ->first();

        if(!$specialRequest)
        {
            $specialRequest = SpecialRequest::query()
                ->create([
                    'user_id' => Auth::user()->id,
                ]);
        }
        return view('special_requests.edit', compact('specialRequest'));
    }

    public function update(Request $request)
    {
        $dav = (new DAVHelper());

        $specialRequest = SpecialRequest::query()
            ->where('user_id', Auth::user()->id)
            ->first();

        if(!$specialRequest)
        {
            $specialRequest = SpecialRequest::query()
                ->create([
                    'user_id' => Auth::user()->id,
                ]);
        }

        $specialRequest->description = $request->description;
        $specialRequest->save();

        if(!empty($request->file1))
        {
            $fileId = File::uploadV2($request->file('file1'),'special_requests', 'additional_file_1', Auth::user()->id);
            $specialRequest->additional_file_id_1 = $fileId;
            $specialRequest->save();
            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'special_requests',$file->name);
        }

        if(!empty($request->file2))
        {
            $fileId = File::uploadV2($request->file('file2'),'special_requests', 'additional_file_2', Auth::user()->id);
            $specialRequest->additional_file_id_2 = $fileId;
            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'special_requests',$file->name);
        }

        if(!empty($request->file3))
        {
            $fileId = File::uploadV2($request->file('file3'),'special_requests', 'additional_file_3', Auth::user()->id);
            $specialRequest->additional_file_id_3 = $fileId;
            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'special_requests',$file->name);
        }

        if(!empty($request->file4))
        {
            $fileId = File::uploadV2($request->file('file4'),'special_requests', 'additional_file_4', Auth::user()->id);
            $specialRequest->additional_file_id_4 = $fileId;
            $specialRequest->save();
            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'special_requests',$file->name);
        }


        UploadExcelToNextCloudJob::dispatchSync($dav,Auth::user(),new SpecialRequestExport(Auth::user()->id),'special_requests','special_requests.xlsx');
        return redirect()->back();
    }


    private function uploadNextCloud($dav,$fileName)
    {
        $userId = Auth::user()->id;
        $dav->mkdir(Auth::user()->name);

        $dav->mkdir(Auth::user()->name.'/special_requests');

        $dav->upload(Auth::user()->name.'/special_requests/'.$fileName,file_get_contents("https://form.arbitri.ch/storage/".$userId."/uploads/special_requests/". $fileName));
    }

    private function uploadExcel($dav)
    {
        $userId = Auth::user()->id;
        Excel::store(new SpecialRequestExport($userId), 'public/'.$userId.'/excels/special_requests.xlsx');
        $dav->mkdir(Auth::user()->name);

        $dav->mkdir(Auth::user()->name.'/special_requests');

        $dav->upload(Auth::user()->name.'/special_requests/'.'special_requests.xlsx',file_get_contents("https://form.arbitri.ch/storage/".$userId."/excels/special_requests.xlsx"));

    }


}
