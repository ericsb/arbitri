<?php

namespace App\Http\Controllers;

use App\Exports\DomiciliationExport;
use App\Helpers\DAVHelper;
use App\Jobs\UploadExcelToNextCloudJob;
use App\Jobs\UploadFileToNextCloudJob;
use App\Models\Domiciliation;
use App\Models\Employment;
use App\Models\File;
use App\Models\Identity;
use App\Models\SpecialRequest;
use App\Services\IdentifyServices;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DomiciliationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');
    }


    public function edit()
    {
        $domiciliation = Domiciliation::query()
            ->where('user_id', Auth::user()->id)
            ->first();

        if(!$domiciliation)
        {
            $domiciliation = Domiciliation::query()
                ->create([
                    'user_id' => Auth::user()->id
                ]);
        }
        return view('domiciliations.edit', compact('domiciliation'));
    }

    public function update(Request $request)
    {
        $dav = (new DAVHelper());

        $domiciliation = Domiciliation::query()
            ->where('user_id', Auth::user()->id)
            ->first();

        if(!$domiciliation)
        {
            $domiciliation = Domiciliation::query()
                ->create([
                    'user_id' => Auth::user()->id
                ]);
        }

        $domiciliation->third_party_status = $request->third_party_status;
        $domiciliation->third_party_requirements = $request->third_party_requirements;
        $domiciliation->looking_renting_status = $request->looking_renting_status;
        $domiciliation->budget_per_month = $request->budget_per_month;
        $domiciliation->purchasing_budget = $request->purchasing_budget;
        $domiciliation->need_residence_status = $request->need_residence_status;
        $domiciliation->country = $request->country;
        $domiciliation->canton = $request->canton;
        $domiciliation->city = $request->city;
        $domiciliation->neighborhood = $request->neighborhood;
        $domiciliation->infrastructure = $request->infrastructure;
        $domiciliation->multiple_office_status = $request->multiple_office_status;
        $domiciliation->preferred_style = $request->preferred_style;
        $domiciliation->m2_count = $request->m2_count;
        $domiciliation->people_count = $request->people_count;
        $domiciliation->infrastructure_near_by = $request->infrastructure_near_by;
        $domiciliation->infrastructure_in = $request->infrastructure_in;
        $domiciliation->comments = $request->comments;
        if(!empty($request->specific_requirements) && $request->specific_requirements != 'null')
            $domiciliation->specific_requirements = json_encode($request->specific_requirements);
        $domiciliation->save();

        if(!empty($request->file1))
        {
            $fileId = File::uploadV2($request->file('file1'),'domiciliations', 'additional_file_1', Auth::user()->id);
            $domiciliation->additional_file_id_1 = $fileId;
            $domiciliation->save();

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'domiciliations',$file->name);
        }

        if(!empty($request->file2))
        {
            $fileId = File::uploadV2($request->file('file2'),'domiciliations', 'additional_file_2', Auth::user()->id);
            $domiciliation->additional_file_id_2 = $fileId;
            $domiciliation->save();

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'domiciliations',$file->name);
        }

        if(!empty($request->file3))
        {
            $fileId = File::uploadV2($request->file('file1'),'domiciliations', 'additional_file_3', Auth::user()->id);
            $domiciliation->additional_file_id_3 = $fileId;
            $domiciliation->save();

            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'domiciliations',$file->name);
        }

        UploadExcelToNextCloudJob::dispatchSync($dav,Auth::user(),new DomiciliationExport(Auth::user()->id),'domiciliations','domiciliations.xlsx');


        return redirect()->back();
    }


}
