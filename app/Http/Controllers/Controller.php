<?php

namespace App\Http\Controllers;

use App\Exports\AuditorExport;
use App\Exports\BankingCapitalExport;
use App\Exports\BankingCompanyExport;
use App\Exports\BankingExport;
use App\Exports\BankingPersonalExport;
use App\Exports\DomiciliationExport;
use App\Exports\EmploymentExport;
use App\Exports\FoundationDirectionExport;
use App\Exports\FoundationFounderExport;
use App\Exports\FoundationPartOneExport;
use App\Exports\FoundationPartTwoExport;
use App\Exports\FoundationShareholderExport;
use App\Exports\IdentityExport;
use App\Exports\InsuranceExport;
use App\Exports\SpecialRequestExport;
use App\Exports\TestExport;
use App\Helpers\DAVHelper;
use App\Models\BankingPersonal;
use App\Models\Identity;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Sabre\DAV\Client;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function export()
    {
        //Excel::store(new IdentityExport(1), 'public/1/excels/identities.xlsx');
        //Excel::store(new FoundationPartOneExport(1), 'public/1/excels/foundations-p1.xlsx');
        //Excel::store(new FoundationPartTwoExport(1), 'public/1/excels/foundations-p2.xlsx');
        //Excel::store(new FoundationDirectionExport(1), 'public/1/excels/foundation-directions.xlsx');
        //Excel::store(new FoundationFounderExport(1), 'public/1/excels/foundation-founders.xlsx');
        //Excel::store(new FoundationShareholderExport(1), 'public/1/excels/foundation-shareholders.xlsx');
        //Excel::store(new DomiciliationExport(1), 'public/1/excels/domiciliations.xlsx');
        //Excel::store(new AuditorExport(1), 'public/1/excels/auditors.xlsx');
        //Excel::store(new BankingExport(1), 'public/1/excels/bankings.xlsx');
        //Excel::store(new BankingCapitalExport(1), 'public/1/excels/banking-capitals.xlsx');
        //Excel::store(new BankingCompanyExport(1), 'public/1/excels/banking-companies.xlsx');
        //Excel::store(new BankingPersonalExport(1), 'public/1/excels/banking-personals.xlsx');
        //Excel::store(new InsuranceExport(1), 'public/1/excels/insurances.xlsx');
        //Excel::store(new EmploymentExport(2), 'public/1/excels/employments.xlsx');
        //Excel::store(new SpecialRequestExport(1), 'public/1/excels/special-requests.xlsx');

    }

    public function nextcloud()
    {
        $settings = array(
            'baseUri'    => 'https://cloud.arbitri.ch/remote.php/dav/files/farhang@thirdly.co.uk/',
            'userName'   => 'farhang@thirdly.co.uk',
            'password'   => '!H@6Eg!qibPm',
        );
    }


    public function createUploadFile()
    {
        return view('upload-file');
    }

    public function storeUploadFile(Request $req){
        $req->validate([
            'file' => 'required|max:2048'
        ]);

        $r = (new DAVHelper('https://cloud.arbitri.ch', 'farhang@thirdly.co.uk', '!H@6Eg!qibPm'))
            ->mkdir('company1');
        dd($r);

        if($req->file()) {
            $fileName = time().'_'.$req->file->getClientOriginalName();
            $filePath = $req->file('file')->storeAs('uploads', $fileName, 'public');
            (new DAVHelper('https://cloud.arbitri.ch', 'farhang@thirdly.co.uk', '!H@6Eg!qibPm', 'newTest/subNewTest'))
                ->upload('testEricc/'. $fileName, file_get_contents(storage_path("app\\public\\uploads\\$fileName")));
            return back()
                ->with('success','File has been uploaded.')
                ->with('file', $fileName);
        }
    }

    public function deleteFile()
    {
        (new DAVHelper('https://cloud.arbitri.ch', 'farhang@thirdly.co.uk', '!H@6Eg!qibPm'))
            ->delete('testEric/'. '1644914086_logo.png');

    }
}
