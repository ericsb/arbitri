<?php

function getFile($userId, $postfixPath, $fileName)
{
    return asset('storage/'. $userId .'/uploads/'. $postfixPath . '/' . $fileName);
}

function flagStatus($tableName, $colName, $rowId, $type)
{
    $flag = \App\Models\Flag::query()
        ->where('table_name', $tableName)
        ->where('col_name', $colName)
        ->where('row_id', $rowId)
        ->where('type', $type)
        ->first();

    if(!$flag) return false;
    return true;
}