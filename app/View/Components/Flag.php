<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Flag extends Component
{
    public $tableName;
    public $colName;
    public $rowId;

    public function __construct($tableName, $colName, $rowId)
    {
        $this->tableName  = $tableName;
        $this->colName    = $colName;
        $this->rowId = $rowId;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.flag');
    }
}
