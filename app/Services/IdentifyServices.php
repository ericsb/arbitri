<?php
/**
 * Created by PhpStorm.
 * User: erfan
 * Date: 2/16/2022
 * Time: 3:04 PM
 */

namespace App\Services;


use App\Exports\IdentityExport;
use App\Helpers\DAVHelper;
use App\Jobs\UploadExcelToNextCloudJob;
use App\Jobs\UploadFileToNextCloudJob;
use App\Models\File;
use App\Models\Identity;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class IdentifyServices
{
    public function updateOrCreate($request, $id = null)
    {
        $identity = empty($id) ? new Identity() : Identity::query()->findOrFail($id);

        $identity->user_id = Auth::user()->id;
        $identity->name = $request->name;
        $identity->surname = $request->surname;
        $identity->birth_date = $request->birth_date;
        $identity->address_of_domicile =  $request->address_of_domicile;
        $identity->phone_number = $request->phone_number;
        $identity->email = $request->email;
        $identity->profession = $request->profession;
        $identity->company_name = $request->company_name;
        $identity->role_in_the_company = $request->role_in_the_company;
        $identity->linkedin =  $request->linkedin;
        $identity->previous_year_annual_salary_in_chf = $request->previous_year_annual_salary_in_chf;
        $identity->nationality = $request->nationality;
        $identity->other_nationalities = $request->other_nationalities;
        $identity->city_of_origin = $request->city_of_origin;
        $identity->municipality_of_origin = $request->municipality_of_origin;
        $identity->company_person_represent = $request->company_person_represent;
        $identity->company_legal_form = $request->company_legal_form;
        $identity->company_address = $request->company_address;
        $identity->company_id_number = $request->company_id_number;
        $identity->company_commercial_register_extract = $request->company_commercial_register_extract;
        $identity->additional_comments =  $request->additional_comments;
        $identity->save();

        $this->uploadFiles($request, $identity);

        $dav = (new DAVHelper());
        UploadExcelToNextCloudJob::dispatchSync($dav,Auth::user(),new IdentityExport(Auth::user()->id),'identities','identities.xlsx');
    }

    public function uploadFiles($request, $identity)
    {
        $inputNames = [
          'id_recto_file_id',
          'id_verso_file_id',
          'passport_file_id',
          'utility_bill_file_id',
          'criminal_record_extract_file_id',
          'company_proof_of_existence_file_id',
          'company_signed_procuration_file_id',
          'company_other_proof_of_existence_file_id',
          'company_other_signed_procuration_file_id',
          'additional_1_file_id',
          'additional_2_file_id',
          'additional_3_file_id',
        ];
        foreach ($inputNames as $inputName)
        {
            $this->uploadSingleFile($request, $inputName, $identity);
        }
    }

    private function uploadSingleFile($request, $inputName, $identity)
    {
        if(!empty($request->$inputName))
        {
            $fileId = File::uploadV2($request->file($inputName),'identities', $inputName, Auth::user()->id);
            $identity->$inputName = $fileId;
            $identity->save();

            $dav = (new DAVHelper());
            $file = File::query()->findOrFail($fileId);
            UploadFileToNextCloudJob::dispatchSync($dav,Auth::user(),'identities',$file->name);

        }
    }
}