<?php
/**
 * Created by PhpStorm.
 * User: erfan
 * Date: 2/16/2022
 * Time: 3:04 PM
 */

namespace App\Services;


use App\Helpers\DAVHelper;
use App\Models\File;
use App\Models\Identity;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class UserServices
{
    public function initialNextcloudFolders($name)
    {
        (new DAVHelper())
            ->mkdir('Companies/'.$name);

        (new DAVHelper())
            ->mkdir('Companies/'.$name .'/'. '1.Identities');

        (new DAVHelper())
            ->mkdir('Companies/'.$name.'/'. '2.Create_a_company');

        (new DAVHelper())
            ->mkdir('Companies/'.$name.'/'. '3.Domiciliation_Residence');

        (new DAVHelper())
            ->mkdir('Companies/'.$name.'/'. '4.Auditor_Accountant');

        (new DAVHelper())
            ->mkdir('Companies/'.$name.'/'. '5.Banking');

        (new DAVHelper())
            ->mkdir('Companies/'.$name.'/'. '6.Insurances');

        (new DAVHelper())
            ->mkdir('Companies/'.$name.'/'. '7.Employment');

        (new DAVHelper())
            ->mkdir('Companies/'.$name.'/'. '8.Special_request');
    }
}