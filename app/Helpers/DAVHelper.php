<?php

namespace App\Helpers;


use Sabre\DAV\Client;

class DAVHelper
{
    protected $_baseUri;
    protected $_username;
    protected $_password;
    protected $_client;

    /**
     * DAVHelper constructor.
     * @param $_baseUri
     * @param $_username
     * @param $_password
     */
    public function __construct($_baseUri = null, $_username = null, $_password = null)
    {
        $this->_baseUri = 'https://cloud.arbitri.ch';
        $this->_username = 'farhang@thirdly.co.uk';
        $this->_password = '!H@6Eg!qibPm';
        $settings = array(
            'baseUri'    => $this->_baseUri . "/remote.php/dav/files/" . $this->_username . "/",
            'userName'   => $this->_username,
            'password'   => $this->_password,
        );
        $this->_client = new Client($settings);
    }

    public function upload($path, $content)
    {
        $this->_client->request('PUT', $path, $content);
    }

    public function mkdir($path)
    {
        return $this->_client->request('MKCOL', $path);
    }

    public function indexRepo()
    {
        return $this->_client->propFind('', array(
            '{DAV:}getlastmodified',
            '{DAV:}getcontenttype',
            '{DAV:}displayname',
        ),3);
    }

    public function delete($file)
    {
        // return $this->_client->request('DELETE', $file, null, array('If-Match' => '"12345765"'));
        return $this->_client->request('DELETE', $file, null, array());
    }



}