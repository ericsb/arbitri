<?php

namespace App\Jobs;

use App\Exports\SpecialRequestExport;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class UploadExcelToNextCloudJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $dav;
    protected $user;
    protected $export;
    protected $folderName;
    protected $fileName;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($dav, $user, $export, $folderName, $fileName)
    {
        $this->dav = $dav;
        $this->user = $user;
        $this->export = $export;
        $this->folderName = $folderName;
        $this->fileName = $fileName;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $mainFolder = 'Users';
        $this->dav->mkdir($mainFolder);

        $userId = $this->user->id;
        $companyName = $userId.'_'.str_replace(' ','_', $this->user->name);
        Excel::store($this->export, 'public/'.$userId.'/excels/'. $this->fileName);
        $this->dav->mkdir($mainFolder.'/'.$companyName);

        $this->dav->mkdir($mainFolder.'/'.$companyName.'/'. $this->folderName);
        $this->dav->upload($mainFolder.'/'.$companyName.'/'. $this->folderName. '/' . $this->fileName,file_get_contents("https://form.arbitri.ch/storage/".$userId."/excels/". $this->fileName));

    }
}
