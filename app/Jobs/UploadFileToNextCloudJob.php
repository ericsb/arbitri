<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UploadFileToNextCloudJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $dav;
    protected $user;
    protected $folderName;
    protected $fileName;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($dav, $user, $folderName, $fileName)
    {
        $this->dav = $dav;
        $this->user = $user;
        $this->folderName = $folderName;
        $this->fileName = $fileName;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $mainFolder = 'Users';
        $this->dav->mkdir($mainFolder);

        $userId = $this->user->id;
        $companyName = $userId . '_' .str_replace(' ','_', $this->user->name);
        $this->dav->mkdir($mainFolder.'/'.$companyName);

        $this->dav->mkdir($mainFolder.'/'.$companyName.'/'. $this->folderName);
        $this->dav->upload($mainFolder. '/' .$companyName.'/'.$this->folderName.'/'.$this->fileName,file_get_contents("https://form.arbitri.ch/storage/".$userId."/uploads/". $this->folderName."/". $this->fileName));

    }
}
