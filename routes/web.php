<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* TEST */
Route::get('/test/export/', [\App\Http\Controllers\Controller::class, 'export']);
Route::get('/nextcloud', [\App\Http\Controllers\Controller::class, 'nextcloud']);
Route::get('/upload-file', [\App\Http\Controllers\Controller::class, 'createUploadFile']);
Route::get('/delete-file', [\App\Http\Controllers\Controller::class, 'deleteFile']);
Route::post('/upload-file', [\App\Http\Controllers\Controller::class, 'storeUploadFile'])->name('upload.file');

Route::get('/', function (){
    return redirect()->route('dashboard');
});


/* Admin */
Route::prefix('admin')->group(function(){
    Route::any('logout', [\App\Http\Controllers\Admin\AuthController::class, 'logout'])->name('admin.logout');
    Route::get('login', [\App\Http\Controllers\Admin\AuthController::class, 'loginForm'])->name('admin.login.form');
    Route::post('login', [\App\Http\Controllers\Admin\AuthController::class, 'login'])->name('admin.login');
    Route::get('dashboard', [\App\Http\Controllers\Admin\DashboardController::class, 'index'])->name('admin.dashboard');
    Route::prefix('users')->group(function(){
        Route::get('index', [\App\Http\Controllers\Admin\UserController::class, 'index'])->name('admin.users.index');
        Route::get('login-as/{id}', [\App\Http\Controllers\Admin\UserController::class, 'loginAs'])->name('admin.users.login-as');
        Route::any('toggle-active/{id}', [\App\Http\Controllers\Admin\UserController::class, 'toggleActive'])->name('admin.users.toggle-active');
    });
});

// MAIN APP
Route::get('/login', [\App\Http\Controllers\AuthController::class, 'loginForm'])->name('login.form');
Route::post('/login', [\App\Http\Controllers\AuthController::class, 'login'])->name('login');
Route::get('/register-secret-admin', [\App\Http\Controllers\AuthController::class, 'registerForm'])->name('register.form');
Route::post('/register', [\App\Http\Controllers\AuthController::class, 'register'])->name('register');
Route::any('/logout', [\App\Http\Controllers\AuthController::class, 'logout'])->name('logout');
Route::get('/dashboard', [\App\Http\Controllers\DashboardController::class, 'index'])->name('dashboard');
Route::get('/invite', [\App\Http\Controllers\DashboardController::class, 'createInvite'])->name('create.invite');
Route::post('/invite', [\App\Http\Controllers\DashboardController::class, 'storeInvite'])->name('store.invite');


Route::get('zip', [\App\Http\Controllers\DashboardController::class, 'zip'])->name('zip');
Route::post('toggle-flag', [\App\Http\Controllers\DashboardController::class, 'toggleFlag'])->name('toggle.flag');



Route::prefix('identities')->group(function(){
    Route::get('index', [\App\Http\Controllers\IdentitiesController::class, 'index'])->name('identities.add-name.index');
    Route::get('create', [\App\Http\Controllers\IdentitiesController::class, 'create'])->name('identities.add-name.create');
    Route::post('store', [\App\Http\Controllers\IdentitiesController::class, 'store'])->name('identities.add-name.store');
    Route::get('edit/{id}', [\App\Http\Controllers\IdentitiesController::class, 'edit'])->name('identities.add-name.edit');
    Route::post('update/{id}', [\App\Http\Controllers\IdentitiesController::class, 'update'])->name('identities.add-name.update');
    Route::get('destroy/{id}', [\App\Http\Controllers\IdentitiesController::class, 'destroy'])->name('identities.add-name.destroy');
});




Route::prefix('special-request')->group(function(){
    Route::get('edit', [\App\Http\Controllers\SpecialRequestController::class, 'edit'])->name('special_request.edit');
    Route::post('update', [\App\Http\Controllers\SpecialRequestController::class, 'update'])->name('special_request.update');
});



Route::prefix('employments')->group(function(){
    Route::get('edit', [\App\Http\Controllers\EmploymentController::class, 'edit'])->name('employments.edit');
    Route::post('update', [\App\Http\Controllers\EmploymentController::class, 'update'])->name('employments.update');
});



Route::prefix('insurance')->group(function(){
    Route::get('edit', [\App\Http\Controllers\InsuranceController::class, 'edit'])->name('insurances.edit');
    Route::post('update', [\App\Http\Controllers\InsuranceController::class, 'update'])->name('insurances.update');
});


Route::prefix('bankings')->group(function(){
    Route::get('index', [\App\Http\Controllers\BankingsController::class, 'index'])->name('bankings.index');
    Route::post('update', [\App\Http\Controllers\BankingsController::class, 'update'])->name('bankings.update');

    Route::prefix('personal')->group(function(){
        Route::get('edit/{id}', [\App\Http\Controllers\BankingsController::class, 'editPersonal'])->name('bankings.personal.edit');
        Route::post('update/{id}', [\App\Http\Controllers\BankingsController::class, 'updatePersonal'])->name('bankings.personal.update');
    });

    Route::prefix('capital')->group(function(){
        Route::get('edit/{id}', [\App\Http\Controllers\BankingsController::class, 'editCapital'])->name('bankings.capital.edit');
        Route::post('update/{id}', [\App\Http\Controllers\BankingsController::class, 'updateCapital'])->name('bankings.capital.update');
    });


    Route::prefix('company')->group(function(){
        Route::get('edit/{id}', [\App\Http\Controllers\BankingsController::class, 'editCompany'])->name('bankings.company.edit');
        Route::post('update/{id}', [\App\Http\Controllers\BankingsController::class, 'updateCompany'])->name('bankings.company.update');
    });

});


Route::prefix('auditor')->group(function(){
    Route::get('edit', [\App\Http\Controllers\AuditorController::class, 'edit'])->name('auditor.edit');
    Route::post('update', [\App\Http\Controllers\AuditorController::class, 'update'])->name('auditor.update');
});


Route::prefix('domiciliation')->group(function(){
    Route::get('edit', [\App\Http\Controllers\DomiciliationController::class, 'edit'])->name('domiciliation.edit');
    Route::post('update', [\App\Http\Controllers\DomiciliationController::class, 'update'])->name('domiciliation.update');
});


Route::prefix('foundations')->group(function(){
    Route::get('edit', [\App\Http\Controllers\FoundationController::class, 'edit'])->name('foundation.edit');
    Route::post('update', [\App\Http\Controllers\FoundationController::class, 'update'])->name('foundation.update');
});